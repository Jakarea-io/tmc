<div class="container columnunder_container">
	<div class="row">
		<div class="col-md-12">
			<div class="content-container">
				<ol class="breadcrumb">
				<li class="breadcrumb-item"><a href="<?php url('')?>">TOP  <i class="fas fa-angle-right breadcumb-icon"></i> </a></li>
					<li class="breadcrumb-item"><a href="<?php url('column')?>">コラム <i class="fas fa-angle-right breadcumb-icon"></i></a></li>
					<li class="breadcrumb-item active"> 体外受精を繰り返しても妊娠しない理由</li>
				</ol>
			</div>
		</div>
    </div>
    <div class="row">
		<div class="col-md-12">
			<div class="col-wrapper">
				<div class="col-under-firstone">
					<?= image('img/col-under-top.png','common-img colunder-top1-web','col-under-top')?>
					<?= image('img/colunder-top-mobile1.png','common-img colunder-top-mobile1','colunder-top-mobile1')?>
					<div class="colunder-top1-contentwrapper">
						<div class="col-under-top1-wrapper">
							<?= image('img/col-under-top1-arrow.png','','col-under-top1-arrow')?>
							<h5 class="colunder-arrow-text">目からウロコの</h5>
							
						</div>
						<h3>不妊治療<span class="col-top1-span">なるほどコラム</span></h3>
					</div>
				</div>
				<div class="col-under-secondone">
					<?= image('img/dont-you-get-web.png','common-img colunder-top-mobile2web','dont-you-get-web')?>
                    <?= image('img/dont-you-get-mobile.png','common-img colunder-top-mobile2mobile','dont-you-get-mobile.png')?>
                    <div class="media colunder-common-media inconvenient-media">
						<div class="col-common-pink-circle">Column</div>
						<div class="media-body">
							<div class="common-mid-text-banner">
								<h4>体外受精を繰り返しても妊娠しない理由</h4>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-9 col-sm-12">
			<div class="column-under-top-list-wrapper">
				<div class="line-wrapper">
					<h3 class="column-under-list-title blue-text-common">目次</h3>
				</div>
				<ul class="columnunder-bottom-before-list">
					<li><a href="#y1">体外受精の成功率アップは3回がリミット！？</a></li>
					<li><a href="#y2">体外受精を繰り返しても妊娠に至らない場合は、まず原因究明を</a></li>
                    <li><a href="#y3">無駄打ちしない。効果的な体外受精成功の秘訣は体質改善だった。</a></li>
                    <li><a href="#y4">［健康診断上、正常な数値］≠［妊娠しやすい数値］ではない。</a></li>
                    <li><a href="#y5">健康診断結果＝じゃない。妊娠に最適な体質改善のススメ</a></li>
				</ul>
			</div>
			
            <div class="row column-under-text-1">
				<div class="col-md-12">
                    <div class="column-bottom-before-title">
                        <a href="#y6">体外受精の成功率アップは3回がリミット!?
                        </a>
                    </div>
                    <div class="media arrange-order">
						<div class="col-under-media-text-container one">
							<p>一般に、体外受精は回数を重ねるほどに成功率が高まるように思われがちですが、ミシガン州立大学の研究主任Barbara Luke氏は、次のように述べています。</p>
                            <p>「3回体外受精して妊娠しなければ、それ以上続けても見込みはほとんどないですよ」</p>
                            <p>これは、2004年から2008年までに、アメリカ全土で実施された体外受精治療が対象となった研究結果による見解です。</p>
                        </div>
						<div class="media-body">
							<?= image('img/cost-media3.png','common-responsive-img','cost-media3')?>
						</div>
                    </div>
                    <p class="common-bold-black">「3回の体外受精のあと、収穫遁源（あるレベルに達した後に継続するとその能率度で低下が起きること）が起きることが分かりました。成功率は上がり続けないのです」</p>
                    <p>体外受精によって出産した171,327分娩（全員初産）が何回目の体外受精で出産に至ったかを調べたところ、7回以上の体外受精を受けた女性たちの成功率は、3回の治療を受けたグループと比べ、わずか3%しか上がらない事が明らかになっています。<br>
                    <a class="common-anchor" href="http://healthland.time.com/2010/10/28/researchers-question-when-to-stop-trying-with-ivf"> (http://healthland.time.com/2010/10/28/researchers-question-when-to-stop-trying-with-ivf/) </a></p>
                    <p class="common-bg-yellowred">コラムでも何度もお伝えしてきた通り、そもそも日本の体外受精で出産に至れる確率は、世界で1番低いのですが、その上、回数を重ねたところで、出産率はほぼ上がらないのです。</p>
                    <p></p>
                    <p>にもかかわらず、<span class="common-bold-black">1回数十万円かかる体外受精を何度も繰り返し患者に行う国内の不妊クリニックには、疑問を抱かざるを得ません。</span></p>
                    <p>ただし、体外受精には、厳密には複数の方法が存在しています(どこまで選択肢を用意しているかは、クリニックによっても異なるものの)。</p>
                    <div class="media">
						<?= image('img/cost-media4.png','common-responsive-img','cost-media4')?>
						<div class="media-body common-media-body">
							<p>体外受精」では主に、受精方法として、卵子に精液をふりかけて受精を試みる<span class="common-bold-black">「体外受精」</span>と、人為的に選別した精子を針で直接卵子に注入する<span class="common-bold-black">「顕微授精」</span>の2種類の方法がありますが、<br>
                            その先の工程においても、受精卵を胚盤胞まで育ててから移植するか、その前に移植するか？胚移植の方法にも、AHA、SEET法、スクラッチング、G-CSF子宮内注入法など、治療の進め方に複数の選択肢があるのです。</p>
						</div>
					</div>
                    <p><u>その為、たとえば胚移植法を変えて再チャレンジしてみるとか、何かしら変化を付けるならまだ不妊原因を特定する手掛かりになり得ますが</u> <span class="common-bold-black">全く同じ方法で体外受精を繰り返し、<span class="yellow-bg">全く同じ方法で何度も失敗し続けるようなら、やはり冒頭の研究結果通り、いつまでも妊娠できない可能性が高いでしょう。</span></p>
                    <p>逆に、体外受精が成功しない原因を突き止められれば、それを解消する事で、挙児を得る事ができるようになるかもしれません</p>
                </div>
            </div>
            <div class="row column-under-text-1">
				<div class="col-md-12">
				    <div class="column-bottom-before-title"><a href="#y8">体外受精を繰り返しても妊娠に至らない場合は、まず原因究明を</a></div>
					<p class=""><span class="common-bg-yellowred">体外受精を繰り返しても妊娠に至らない場合、「着床不全」「不育症」が原因になっている可能性があります。</span>特に、</p>
                    <div class="bol-under-isto dont-you-knw-ul-wrap">
                        <ul class="dont-you-unorder-list">
                            <li>採卵はできている</li>
                            <li>体外で、受精卵までは培養できている</li>
                            <li>受精卵が正常に胚分割し、胚盤胞まで育っている</li>
                            <li>でも、胚盤胞を移植しても着床しない</li>
                            <li>着床までは到達するが、妊娠を継続できない</li>
                        </ul>
                    </div>    
                    <p>というケースでは、<span class="common-bold-black">まず着床不全・不育症を疑います。<br>
                        (着床不全→</span>受精卵がそもそも着床しない　<span class="common-bold-black">不育症→</span>受精卵の着床後に、流産を繰り返す)</p>
                    <p>ところが、不育症の検査・治療を提案せず、ひたすら体外受精を繰り返す治療を提案するクリニックが少なくないのです。</p>            
                
                </div>
            </div>
            <div class="row column-under-text-1">
				<div class="col-md-12">
                    <div class="column-bottom-before-title">
                        <a href="#y14">無駄打ちしない。効果的な体外受精成功の秘訣は体質改善だった。</a>
                    </div>
                    <div class="media arrange-order">
						<div class="col-under-media-text-container one">
							<p>でも少し触れたように、<span class="common-bg-yellowred">もし着床不全・不育症の検査を行って体外受精の阻害要因を取り除けるのであれば、体外受精を始める前に、着床不全・不育症の検査・治療を完了しておけば良いと思う</span>のですが。</p>
                            <p>実際には、着床不全・不育症の治療は、たいてい体外受精失敗後にスタートするか、いつまでも始まらないまま終わるかのどちらかです。</p>
                        </div>
                        <div class="media-body">
							<?= image('img/dont-you-get-media3.png','common-responsive-img','dont-you-get-media3')?>
						</div>
                    </div>
                    <p>(不妊治療は女性の年齢との戦いなので、時間的制約から先に体外受精を行うのだと主張するクリニックも多いですが、私からすれば、<span class="common-bold-black">準備不足で体外受精に臨む方が、患者の時間と体力とお金の無駄遣いだと思います)</span></p>
                    <p>そもそも、不妊専門クリニックの中でも、着床不全・不育症の検査や治療を行っているクリニック自体が少ないです。</p>
                    <p>では、何度体外受精を行っても妊娠に至らない原因が、着床不全・不育症にあるとすれば、<span class="common-bold-black">着床不全と不育症になる原因は何なのでしょうか？</span></p>
                    <p>これはそれぞれ、</p>
                    <div class="column-under-middle1 burden-content-box-wrap dont-you-ge-middle">
						<ul class="column-under-middle1-list colunder-unorder-wrapper">
                            <li>
                                <h4 class="blue-bg blue-bg-wrapper">着床不全になり得る原因</h4>
								<li><div class="contents-list-order colunder-order">1</div>
                                <p class="common-bold-black">受精卵の質</p></li>
                                <li><div class="contents-list-order colunder-order">2</div>
                                <p class="common-bold-black">子宮内膜着床能(スペインのigenomix社は子宮内膜着床能を測定する検査を提供)</p></li>
                                <li><div class="contents-list-order colunder-order">3</div>
                                <p class="common-bold-black">子宮内膜ポリープ、子宮奇形</p></li>
                                <li><div class="contents-list-order colunder-order">4</div>
                                <p class="common-bold-black">慢性子宮内膜炎</p></li>
                                <li><div class="contents-list-order colunder-order">5</div>
                                <p class="common-bold-black">子宮内の細菌叢バランス</p></li>
                                <li><div class="contents-list-order colunder-order">6</div>
                                <p class="common-bold-black">(受精卵を受け入れる)免疫</p></li>
                            </li>
							<br>
                            <li class="common-bold-black">
                                <h4 class="blue-bg blue-bg-wrapper">不育症になり得る原因</h4>
                                <p class="common-bold-black"><u>着床不全になりえる原因1～6に加え、</u></p>
								<li><div class="contents-list-order colunder-order">7</div>
                                <p class="common-bold-black">甲状腺機能異常</p></li>
                                <li><div class="contents-list-order colunder-order">8</div>
                                <p class="common-bold-black">抗リン脂質抗体症候群</p></li>
                                <li><div class="contents-list-order colunder-order">9</div>
                                <p class="common-bold-black">膠原病</p></li>
                                <li><div class="contents-list-order colunder-order">10</div>
                                <p class="common-bold-black">血液凝固異常</p></li>
                            </li>
						</ul>
                    </div>
					<br>
                    などが挙げられます。<br>
                    非掲載
					<p><a class="common-anchor" href="http://www.kanagawa-art.jp/topics/547.html">(http://www.kanagawa-art.jp/topics/547.html)</a></p>
                    <p><u>体外受精という物理的アプローチこそが、最も効果を実感しやすい気がするものですが、実は体外受精が上手く行かない場合、<span class="red-text">結局は体質改善に立ち戻って行く</span>のです。</u></p>
                </div>
            </div>
            <div class="row column-under-text-1">
				<div class="col-md-12">
				    <div class="column-bottom-before-title"><a href="#y10">［健康診断上、正常な数値］≠［妊娠しやすい数値］ではない。</a></div>
                    <div class="media">
						<?= image('img/you-dont-know-media4.png','common-responsive-img','you-dont-know-media4')?>
						<div class="media-body common-media-body">
							<p>もっとも、この「体質改善」というのは、<span class="common-bold-black">必ずしも「不健康な状態から、健康な状態へ戻す」という意味ではありません。</span></p>
                            <p>ハッキリ言って、日常生活では健康そのものなのに、妊娠できない不妊患者はたくさん居ます。</p>
                            <p>そういう患者にとっては、特に健康状態に問題が無いだけに、体質改善などでは、到底妊娠できる気がしないでしょうが、実は、</p>
						</div>
                    </div>
                    <div class="bol-under-isto dont-you-knw-ul-wrap ul-second-inside-box">
                        <p class="common-bold-black">［健康診断上、正常な数値］≠［妊娠しやすい数値］</p>
                    </div>
                    <p>である事を、知っておく必要があります(もちろん、一致する項目もありますが、全項目完全にイコールではない)。</p>
                    <p>たとえば、先ほど不育症になる一因として、甲状腺機能異常を挙げましたが、ここでいう甲状腺機能の数値と言うのも、健康診断上、正常な数値と、妊娠を希望する期間に望ましい数値は、完全には一致していません。</p>
				</div>
            </div>
            <div class="row column-under-text-1">
				<div class="col-md-12">
                    <div class="column-bottom-before-title">
                        <a href="#y11">健康診断結果＝じゃない。妊娠に最適な体質改善のススメ</a>
                    </div>
                    <div class="media arrange-order">
						<div class="col-under-media-text-container one">
							<p>あなたが健康なのに妊娠に至れていないとすれば、<span class="yellow-bg">健康診断上は正常でも、妊娠に適した体内環境にはなっていない可能性が高い</span>です。</p>
                            <p>その為、<span class="common-bold-black">何度体外受精を行っても妊娠に至らない夫婦には特に、ただ単に健康診断上正常な数値を目指すのではなく、あくまで妊娠を目的とした体質改善を指導しています。</span></p>
                        </div>
                        <div class="media-body">
							<?= image('img/you-dont-know-media5.png','common-responsive-img','dont-you-get-media5')?>
						</div>
                    </div>
                </div>
            </div>
		    <div class="row column-under-text-1">
				<div class="col-md-12">
					
					<p><span class="common-bold-black">不妊治療や巷の「妊活」には、様々なアプローチ法が存在しますが、私は、不妊治療は<span class="common-bg-yellowred">「妊娠率(分娩率)」</span>と<span class="common-bg-yellowred">「安全性」</span>で選ぶべきだと考えております。</span></p>
					<p class="common-bg-yellowred">だからこそ、外科的な処置が一切伴わない、妊娠率89%を達成した英国式療法をお勧めしております。</p>
					<p class="column-after-red-text">※この治療法は、患者の症状に合わせて最適な処方が異なります。<br>もしご自身に合った治療法をお知りになりたい場合は、下記の無料相談(問診)を通して<br>お問い合せ下さい。</p>
	　			</div>
			</div>
		    <div class="dark-red-button red-button-common">
				<a href="http://projects.cloudproduction.co.bd/tmclinic/consultation" type="button" class="btn btn-lg btn-red-custom waves-effect waves-light">不妊治療無料相談はこちら</a>
			</div>
			<div class="ceo-bottom-container">
				<div class="line-wrapper">
					<h3 class="column-under-list-title blue-text-common">監修</h3>
				</div>
				<div class="colunder-bottommedia-container">
					<div class="media">
						<div class="col-under-media-text-container">
							<?= image('img/ceo1.png','','ceo')?>
						</div>
						<div class="media-body">
							<p class="blue-text-common ceo-right-text">外来部長 <span class="pt-up-font">山下明子先生</span><span>医学博士、内科医、神経内科専門医、抗加齢医学専門医</span></p>
						</div>
					</div>
					<div class="media">
						<div class="col-under-media-text-container">
							<?= image('img/ceo2.png','','ceo2')?>
						</div>
						<div class="media-body">
							<p class="blue-text-common ceo-right-text">顧問医師 <span class="pt-up-font">身原正哉先生</span><span>産婦人科専門医・日本抗加齢医学会専門医</span></p>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-3 colunder-right-section">
			<?php view('common/ceo') ?>
		</div>
	</div>
</div>