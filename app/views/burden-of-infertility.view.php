<div class="container columnunder_container">
	<div class="row">
		<div class="col-md-12">
			<div class="content-container">
				<ol class="breadcrumb">
					<li class="breadcrumb-item"><a href="#b13">TOP  <i class="fas fa-angle-right breadcumb-icon"></i> </a></li>
					<li class="breadcrumb-item">コラム <i class="fas fa-angle-right breadcumb-icon"></i></li>
					<li class="breadcrumb-item active"> 不妊治療ステップアップの先の負担</li>
				</ol>
			</div>
		</div>
    </div>
    <div class="row">
		<div class="col-md-12">
			<div class="col-wrapper">
				<div class="col-under-firstone">
					<?= image('img/col-under-top.png','common-img colunder-top1-web','col-under-top')?>
					<?= image('img/colunder-top-mobile1.png','common-img colunder-top-mobile1','colunder-top-mobile1')?>
					<div class="colunder-top1-contentwrapper">
						<div class="col-under-top1-wrapper">
							<?= image('img/col-under-top1-arrow.png','','col-under-top1-arrow')?>
							<h5 class="colunder-arrow-text">目からウロコの</h5>
							
						</div>
						<h3>不妊治療<span class="col-top1-span">なるほどコラム</span></h3>
					</div>
				</div>
				<div class="col-under-secondone">
					<?= image('img/burden-of-infertility-web.png','common-img colunder-top-mobile2web','burden-of-infertility-web')?>
                    <?= image('img/burden-of-infertility-mobile.png','common-img colunder-top-mobile2mobile','burden-of-infertility-mobile')?>
                    <div class="media colunder-common-media inconvenient-media">
						<div class="col-common-pink-circle">Column</div>
						<div class="media-body">
							<div class="common-mid-text-banner">
								<h4>不妊治療ステップアップの先の負担</h4>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-9 col-sm-12">
			<div class="column-under-top-list-wrapper">
				<div class="line-wrapper">
					<h3 class="column-under-list-title blue-text-common">目次</h3>
				</div>
				<ul class="columnunder-bottom-before-list">
					<li><a href="#b1">当事者でなければ分からない。ステップアップごとに上がる費用と身体的負担</a></li>
					<li><a href="#b2">タイミング法では男性は通院も採精も不要</a></li>
					<li><a href="#b3">男女共に受診が必須、身体的負担も増える人口受精</a></li>
					<li><a href="#b4">人授精でも妊娠に至らない場合は、ヒューナーテスト～卵管造影へ</a></li>
					<li><a href="#b5">人授精から体外受精へ。外科的処置不要の男性と身体的負担が増す女性</a></li>
					<li><a href="#b6">つらい治療と高額な費用をかけても体外受精の出産率は20％以下</a></li>
				</ul>
			</div>
			<div class="column-bottom-before-title">
				<a href="#b7">当事者でなければ分からない。<br>ステップアップごとに上がる費用と身体的負担</a>
			</div>
            
			<div class="row column-under-text-1 incon-truth-second-wrapper cost-of-middle-container">
                <div class="col-md-12">
					<div class="media">
						<div class="col-under-media-text-container">
							<p>避妊せず夫婦生活を営むと、健康なカップルは最初の1年で80%が妊娠します。
								そして、次の1年では残りの20%のうちの半分が妊娠すると言われています。</p>

							<p>不妊治療をせずに、です。</p>

							<p>その為、クリニックへ行き、タイミング法による医師からの指導を受けて、妊娠に至ったカップルは、
							「みんな何もしなくても簡単に妊娠できているのに…」
							自分たちは妊娠しづらいのだと考えます。</p>
						</div>
						<div class="media-body">
							<?= image('img/burden-media1.png','common-responsive-img','burden-media1')?>
						</div>
					</div>
					<p>しかし、人工授精によって妊娠した人たちは、タイミング法だけで妊娠できたなんて、不妊治療を受けたうちに入 らない、と言います。</p>
					<p>そして、体外受精によって妊娠した人たちは、人工授精だけで妊娠できたなんて、不妊治療を受けたうちに入らない、などと言うのです。</p>

					<p>これは、「ステップアップするごとに、患者にどんな負担が増すのか？」<br>
						当事者でなければ具体的にはわかりづらい事から生まれる意識の差です。<br>
						(たとえタイミング法であっても通院は必要になるわけで、どの治療法にせよ、自然妊娠より労力を伴う事は間違いないのですが…)</p>
					<p>ですから、<span class="common-bold-black">「体外受精」「顕微授精」までステップアップすると、発生する負担がどう変わるのかも、実際に治
						療を受ける状況と直面するまで、大半の患者がよく分かって居ません。</span>
					</p>
                </div>
			</div>
			<div class="row column-under-text-1">
				<div class="col-md-12">
                    <div class="column-bottom-before-title"><a href="#b8">タイミング法では男性は通院も採精も不要</a></div>
                    <div class="media cost-of-media-wrap">
						<?= image('img/cost-media1.png','common-responsive-img','cost-media1.png')?>
						<div class="media-body common-media-body">
							<p>まず、タイミング法では、<span class="common-bold-black">男性の通院は必要ありません。<br>
								いわゆる「採精」も不要</span>です。</p>
							<p>排卵が近づくと、女性はクリニックの超音波で卵胞の大きさを測り、もっとも妊娠しやすい日を予測して貰います。<br>
								あとは、予測日に性交するだけですが、クリニックで受けるタイミング法の場合、超音波による卵胞チェックの為に、膣内に器具を入れられます。</p>
						</div>
					</div>
					<p class="common-bold-black">激痛を伴うような処置はありませんが、女性は、そんな事をしなくても自然妊娠できるなら、それに越した事は無いと思うはずです。</p>
                 </div>
			</div>

			<div class="row column-under-text-1">
				<div class="col-md-12">
					<div class="column-bottom-before-title"><a href="#b9">男女共に受診が必須、身体的負担も増える人工受精</a></div>
					<div class="media">
						<div class="col-under-media-text-container">
							<p>人工授精にステップアップすると、ここからは<span class="common-bold-black">男性の受診も必要</span>になっていきます。</p>
							<p>極端な話、タイミング法は産婦人科・婦人科でも指導して貰えたりするのですが、人工授精以降は、不妊専門クリニックの門扉を叩かねばなりません。</p>
							<p>初めて不妊の専門クリニックを訪れると、これから本格的に不妊治療を受ける為に必要な検査が一通り行われます。</p>
						</div>
						<div class="media-body">
						<?= image('img/cost-media2.png','common-responsive-img','cost-media2')?>
						</div>
					</div>
					<p>人工授精の場合もタイミング法と同じく、超音波で卵胞チェックを行い、妊娠しやすい日を予測するのですが、タイミング法とは異なり、予測日に<span class="common-bold-black">精液を子宮へ直接注入します。</span></p>
					<p>その為、<span class="common-bold-black">基本的に男性は、指定日にクリニックへ赴き、射精するよう言われます。</span><br>
						容器に精液を入れて、クリニック側へ渡すのですが、これを<span class="common-bold-black">「採精」</span>と言います。</p>
					<p>どうしてもクリニックで射精できない場合は、自宅で採精を行うケースもありますが、この場合は、採取した精液を人肌にあたためながら、1時間程度以内にクリニックへ持ち込むことになります。<br>
						※ただし、「鮮度が落ちる」という理由で、精液の自宅からの持ち込みを推奨しない(または禁止する)クリニックも多いです。</p>
					<p>この「採精」を「男のプライドが傷つく」と言って嫌がったり、なかなか仕事の調整が付かず、治療に協力できない男性も珍しくありません。<br>
						体外受精の場合は、男性が精液を凍結保存したり、女性の排卵日を薬でコントロールしたりするケースが増えるのですが、人工授精の場合、精液を直接子宮へ注入する事以外は、自然に近い状態で実施されるケースが大半です。</p>
					<p>女性はカテーテルを子宮まで入れられる為、治療経由の感染症の可能性もゼロではなくなり、やはりタイミング法より体への負担や物理的な不快感は増します。</p>
					<p class="common-bold-black">タイミング法までは、受精に人の手が介入する事はありませんが、人工授精は自宅で実践できる治療法でもなく、夫婦ともに通院が必要となり、不妊治療の為に仕事を調整する機会も増えます。</p>
				</div>
			</div>
			<div class="row column-under-text-1">
				<div class="col-md-12">
                    <div class="column-bottom-before-title"><a href="#b10">人口授精でも妊娠に至らない場合は、ヒューナーテスト～卵管造影へ</a></div>
					<p>さて、めでたく人工授精で妊娠が成立すれば良いのですが、人工授精を行っても妊娠に至らない場合、様々な原因が考えられます。</p>
					<div class="column-under-middle1 burden-content-box-wrap">
						<ul class="column-under-middle1-list colunder-unorder-wrapper">
							<li>
								<div class="contents-list-order colunder-order">1</div>
								<p><span class="common-bold-black">排卵期の女性の頸管粘液が、抗精子抗体を持っている</span><br>
								<span class="order-sublist-span">→精子が子宮内で生きられない(受精できない)</span><br>
								<span class="order-sublist-span">→ヒューナーテストを行って、抗精子抗体の有無を確認する</span></p>
							</li>
							<li>
								<div class="contents-list-order colunder-order">2</div>
								<p><span class="common-bold-black">卵管が閉塞している</span><br>
								<span class="order-sublist-span">→排卵しても卵子が子宮まで辿り着かず、精子と出会えない(受精できない)</span><br>
								<span class="order-sublist-span">→卵管造影を行って、卵管閉塞の有無を確認する</span></p>
							</li>
							
						</ul>
					</div>
					<br>
					<p><span class="common-bg-yellowred"> ヒューナーテスト</span><span class="yellow-bg">は、性交後、子宮内に何匹の精子が生きているかを観察するもの</span>ですので、夫婦はこの検査にタイミングを合わせて性交するよう言われます。</p>
					<p>こけだけでも十分、夫婦にとって快適な話ではないと思うのですが、<span class="common-bg-yellowred">卵管造影</span><span class="common-bold-black">に至っては、ほとんどの女性が激痛に苦悶します。</span><br>
					※「卵管造影」と検索すれば、「卵管造影　痛い」「卵管造影　痛み」などの検索候補が自動的に出てきます。
					中には、痛みの余り、失神する女性も居るほどです。</p>
					<p>そして、ヒューナーテストを行っても、卵管造影を行っても、どちらも異常なしにもかかわらず、タイミング法や人工授精で妊娠に至らない夫婦が、次にチャレンジするのが<span class="common-bg-yellowred">「体外受精」</span>です。</p>
					<div class="media cost-of-media-wrap">
						<?= image('img/cost-media3.png','common-responsive-img','cost-media3')?>
						<div class="media-body common-media-body">
							<p>体外受精には、大きな分類では<span class="common-bg-yellowred">「自然周期法」</span>と<span class="common-bg-yellowred">「刺激法」</span>の2種類の方法があります。<br>
								人工授精までは、女性のお腹の中にある卵子を取り出す事はありません。</p>
							<p>しかし、体外受精では、<span class="common-bg-yellowred">「採卵」</span>といって、女性の卵子を外に取りだしてから精子と出会わせて受精卵を作り、それをまた女性の体内に戻す処置(移植)が発生します。</p>
						</div>
					</div>
					<p>この流れは、<span class="common-bold-black">「自然周期法」</span>でも<span class="common-bold-black">「刺激法」</span>でも同じです。</p>
					<p>採卵には、麻酔を使うクリニックと、無麻酔のクリニックがあります。</p>
					<p>体内のものを体外へ取り出すのですから、当然麻酔が無いと怖い気がすると思いますが、麻酔ありの採卵も、それはそれで体に大きな負担が掛かるのです。</p>
					<p>その為、クリニックによって方針が異なります。</p>

					<h4 class="blue-bg blue-bg-wrapper">自然周期法</h4>
					<p><span class="common-bg-yellowred">出来る限り薬を使わず、自然な排卵を待って採卵し、受精卵の移植後も、なるべく薬を使わない。</span><br>
						(月経8日目から少量の自己注射が必要になる可能性はある)</p>
					<p>薬を使わないという点では体への負担は比較的軽くなるが、1回の採卵で取れる卵子は、1周期につき多くても2個まで。<br>
						その為、移植可能な受精卵の数が少なく、採卵の回数が増える可能性がある<br>
						また、排卵日を薬でコントロールしない分、採卵に最適なタイミングを計る為の通院回数が多くなる。</p>

					<h4 class="blue-bg blue-bg-wrapper">刺激法</h4>
					<p class="common-bg-yellowred">排卵誘発剤で卵巣を刺激しながら、排卵を抑える薬で排卵日をコントロールし、1度の採卵でなるべく多くの卵子が取れる事を優先する方針。</p>

					<p><br>月経当日または3日目以降、妊娠判定まで毎日自己注射と服薬が必要になる<br>
						受精卵がたくさんできるほど、妊娠率も高くなると言われており、効率の良さから海外ではこちらが主流。</p>
					<p>ただし、女性は薬漬けになってホルモンバランスを崩し、精神的に不安定になったり、OHSS(卵巣過剰刺激症候群)を発症してしまう事もある<br>
						※OHSS…薬の副作用で卵巣腫大、腹水や胸水の貯留などが起こり、重症時には血栓症、呼吸困難などの症状が出て、入院が必要になったり、命にかかわる場合もある他、月経周期を乱し、卵子の質を悪化させる一因になる</p>
					<p>体外受精では、処方される薬や服用期間など、クリニックと患者の状態によって異なりますが、「自然周期法」にも「刺激法」にも、どちらにも一長一短あります。</p>
					<p>そして、<span class="common-bold-black">どちらもリスクを伴う治療法です。</span></p>
					<p>また、頻繁な通院や毎日の自己注射が必要になる事などから、仕事に支障をきたし、体外受精へのステップアップで退職する女性も少なくありません。</p>
				</div>
			</div>
			<div class="row column-under-text-1">
				<div class="col-md-12">
				<div class="column-bottom-before-title"><a href="#b11">人口授精から体外受精へ。<br>外科的処置不要の男性と身体的負担が増す女性</a></div>
					<div class="media cost-of-media-wrap">
						<?= image('img/column-under-right.png','common-responsive-img','column-under-right')?>
						<div class="media-body common-media-body">
							<p>なお、体外受精の場合、男性は精子を凍結保存しておけば、なんと通院は(凍結保存した精子が尽きるまでは)1回で済みます。</p>
							<p>また、不妊治療をステップアップしていく過程の検査結果から、男性不妊が明らかになる事もありますが、たとえ男性が無精子症であっても、顕微授精であれば、ほとんどの場合、妊娠が成立しますので、男性に外科的な処置が必要になるケースは珍しいです</p>
						</div>
					</div>
					<p class="common-bold-black">さらに、何度体外受精を行っても妊娠が成立(または継続)しない場合は、女性は<span class="common-bg-yellowred">「不育症」</span>の検査を受ける事もあります。<br>
						※何度も流産を繰り返してしまう場合も、「不育症」の検査を受けたりします。</p>
					<p>不育症の検査の中で、もっとも激痛で有名なのが<span class="common-bg-yellowred">「BCE検査」</span>です。<br>
						卵管造影と同じく、「BCE検査」と検索すると、「BCE検査　痛い」という検索候補が出てくると思います。
					</p>
					<p class="common-bold-black">不妊治療というのは、とにかくステップアップするほど、(痛みも含めて)あらゆる負担が増していくものなのです。</p>
				</div>
			</div>
			<div class="row column-under-text-1">
				<div class="col-md-12">
					<div class="column-bottom-before-title"><a href="#b12">つらい治療と高額な費用をかけても体外受精の出産率は20％以下</a></div>
					<div class="media cost-of-media-wrap">
						<div class="col-under-media-text-container">
							<p>ここまで、不妊治療ステップアップの先に待ち受ける負担をお伝えしてきましたが、<span class="common-bold-black">それぞれの治療を1回だけ試して終わる患者は少数派です。</span></p>
							<p class="common-bold-black">体外受精までステップアップし、これだけの負担やリスクを負っても、体外受精の平均出産率（分母は総治療周期数）は11.４％しかない（2010年のデータ、日本産科婦人科学会資料より）と言われています。</p>
							<p>それでも、不妊に悩む夫婦の多くが、このわずかな確率に賭けて、何度も何度も、諦めずに高額な不妊治療を続けているのです。</p>
							
						</div>
						<div class="media-body">
							<?= image('img/burden-media2png.png','common-responsive-img','burden-media2png')?>
						</div>
					</div>
					<p class="common-bold-black">不妊治療や巷の「妊活」には、様々なアプローチ法が存在しますが、私は、不妊治療は<span class="common-bg-yellowred">「妊娠率(分娩率)」</span>と<span class="common-bg-yellowred">「安全性」</span>で選ぶべきだと考えております。</p>
					<p class="common-bg-yellowred">だからこそ、外科的な処置が一切伴わない、妊娠率89%を達成した英国式療法をお勧めしております。</p>
					<p class="column-after-red-text">※この治療法は、患者様の症状に合わせて最適な処方が異なります。<br>もしご自身に合った治療法をお知りになりたい場合は、下記の無料相談(問診)を通して<br>お問い合せ下さい。</p>
	　			</div>
			</div>
			
            <div class="dark-red-button red-button-common">
				<a href="http://projects.cloudproduction.co.bd/tmclinic/consultation" type="button" class="btn btn-lg btn-red-custom waves-effect waves-light">不妊治療無料相談はこちら</a>
			</div>
			<div class="ceo-bottom-container">
				<div class="line-wrapper">
					<h3 class="column-under-list-title blue-text-common">監修</h3>
				</div>
				<div class="colunder-bottommedia-container">
					<div class="media">
						<div class="col-under-media-text-container">
							<?= image('img/ceo1.png','','ceo')?>
						</div>
						<div class="media-body">
							<p class="blue-text-common ceo-right-text">外来部長 <span class="pt-up-font">山下明子先生</span><span>医学博士、内科医、神経内科専門医、抗加齢医学専門医</span></p>
						</div>
					</div>
					<div class="media">
						<div class="col-under-media-text-container">
							<?= image('img/ceo2.png','','ceo2')?>
						</div>
						<div class="media-body">
							<p class="blue-text-common ceo-right-text">顧問医師 <span class="pt-up-font">身原正哉先生</span><span>産婦人科専門医・日本抗加齢医学会専門医</span></p>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-3 colunder-right-section">
			<?php view('common/ceo') ?>
		</div>
	</div>
</div>