<div class="container columnview-container">
	<div class="row">
		<div class="col-md-12">
			<div class="col-wrapper">
				<?= image('img/column-top-bg.png','common-img col-topbg-web','column-top-bg')?>
				<?= image('img/column-top-bg-mobile.png','common-img col-topbg-mobile','column-top-bg-mobile')?>
				<div class="column-top-bg-content">
					<h4 align="" class="col-sbtitle">TMクリニック 不妊外来研究員の</h4>
					<div class="top-arrow-bg-wrap">
						<?= image('img/top-bg-arrow.png','common-img top-arrow-img','column-top-bg')?>
						<h5 class="arrow-inner-text">目からウロコの</h5>
						
					</div>
					<h1 align="" class="col-title">不妊治療<span class="col-title-red">なるほどコラム</span></h1>
					<div class="col-top-bottom-text">
						<p class="col-text-p1 left">外来部長 <br/>山下先生</p>
						<p class="col-text-p2 right">顧問医師 <br>身原先生</p>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="row column-mid-img-wrapper">
		<div class="col-md-9 column-mid-left-bg padding_0">
			<div class="col-middle-left-wrap ptoff">
				<div class="common-col-inside">
					<?= image('img/col-1.png','common-img web-common-img','col-1-bg')?>
					<?= image('img/col1-mobile.png','common-img mobile-common-img','col1-mobile bg')?>
					<div class="media col-common-media">
						<div class="col-common-pink-circle">Column</div>
						<div class="media-body">
							<div class="common-mid-text">
								<h4><a href="<?= url('column-under')?>">日本初上陸の妊娠率89%の<br>英国式療法</a></h4>
							</div>
						</div>
					</div>
				</div>
				<div class="common-col-inside">
					<?= image('img/col-2.png','common-img web-common-img','col-2-bg')?>
					<?= image('img/col2-mobile.png','common-img mobile-common-img','col2-mobile bg')?>
					<div class="media col-common-media">
						<div class="col-common-pink-circle">Column</div>
						<div class="media-body">
							<div class="common-mid-text">
								<h4><a href="<?= url('inconvenient-truth')?>">日本の不妊治療の不都合な真実</a></h4>
							</div>
						</div>
					</div>
				</div>
				<div class="common-col-inside">
					<?= image('img/col-3.png','common-img web-common-img','col-3-bg')?>
					<?= image('img/col3-mobile.png','common-img mobile-common-img','col3-mobile bg')?>
					<div class="media col-common-media">
						<div class="col-common-pink-circle">Column</div>
						<div class="media-body">
							<div class="common-mid-text">
								<h4><a href="<?= url('cost-of-infertility')?>">不妊治療にかかる費用</a></h4>
							</div>
						</div>
					</div>
				</div>
				<div class="common-col-inside">
					<?= image('img/col-4.png','common-img web-common-img','col-4-bg')?>
					<?= image('img/col4-mobile.png','common-img mobile-common-img','col4-mobile bg')?>
					<div class="media col-common-media">
						<div class="col-common-pink-circle">Column</div>
						<div class="media-body">
							<div class="common-mid-text">
								<h4><a href="<?= url('burden-of-infertility')?>">不妊治療ステップアップの先の負担</a></h4>
							</div>
						</div>
					</div>
				</div>
				<div class="common-col-inside">
					<?= image('img/col-5.png','common-img web-common-img','col-5-bg')?>
					<?= image('img/col5-mobile.png','common-img mobile-common-img','col5-mobile bg')?>
					<div class="media col-common-media">
						<div class="col-common-pink-circle">Column</div>
						<div class="media-body">
							<div class="common-mid-text">
								<h4><a href="<?= url('absence-of-infertility')?>">不妊治療の不条理</a></h4>
							</div>
						</div>
					</div>
				</div>
				<div class="common-col-inside">
					<?= image('img/col-6.png','common-img web-common-img','col-6-bg')?>
					<?= image('img/col6-mobile.png','common-img mobile-common-img','col6-mobile bg')?>
					<div class="media col-common-media">
						<div class="col-common-pink-circle">Column</div>
						<div class="media-body">
							<div class="common-mid-text">
								<h4><a href="<?= url('combine-infertility')?>">男性不妊と女性不妊</a></h4>
							</div>
						</div>
					</div>
				</div>
				<div class="common-col-inside">
					<?= image('img/col-7.png','common-img web-common-img','col-7-bg')?>
					<?= image('img/col7-mobile.png','common-img mobile-common-img','col7-mobile bg')?>
					<div class="media col-common-media">
						<div class="col-common-pink-circle">Column</div>
						<div class="media-body">
							<div class="common-mid-text">
								<h4><a href="<?= url('one-out-of-every-hundread')?>">100人に1人が45歳以上の母だった<br>大正時代</a></h4>
							</div>
						</div>
					</div>
				</div>
				<div class="common-col-inside">
					<?= image('img/col-8.png','common-img web-common-img','col-8-bg')?>
					<?= image('img/col8-mobile.png','common-img mobile-common-img','col8-mobile bg')?>
					<div class="media col-common-media">
						<div class="col-common-pink-circle">Column</div>
						<div class="media-body">
							<div class="common-mid-text">
								<h4><a href="<?= url('dont-you-get')?>">体外受精を繰り返しても<br>妊娠しない理由</a></h4>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-3 ceo-container">
			<?php view('common/ceo') ?>
		</div>
	<div class="row column-bottom-before">
		<div class="col-md-12">
			<div class="column-bottom-before-title"><a href="">
			コラム一覧</a>
			</div>	
			<ul class="column-bottom-before-list">
				<li><a href="<?= url('column-under')?>">日本初上陸の妊娠率89％の英国式療法</a></li>
				<li><a href="<?= url('inconvenient-truth')?>">日本の不妊治療の不都合な真実</a></li>
				<li><a href="<?= url('cost-of-infertility')?>">不妊治療にかかる費用</a></li>
				<li><a href="<?= url('burden-of-infertility')?>">不妊治療ステップアップの先の負担</a></li>
				<li><a href="<?= url('absence-of-infertility')?>">不妊治療の不条理</a></li>
				<li><a href="<?= url('combine-infertility')?>">男性不妊と女性不妊</a></li>
				<li><a href="<?= url('one-out-of-every-hundread')?>">100人に1人が45歳以上の母だった大正時代</a></li>
				<li><a href="<?= url('dont-you-get')?>">体外受精を繰り返しても妊娠しない理由</a></li>
			</ul>
			<div class="column-after-red-text">
				<div class="left_icon_right_text">
					<span class="left_icon"> ※ </span>	
					<span class="right_text"> この治療法は、患者様の症状に合わせて 最適な処方が異なります。<br>もしご自身に合った治療法をお知りになりたい場合は、下記の無料相談(問診)を通して<br>お問い合せ下さい。 </span>
				</div>
			</div>
		</div>
	</div>

		<div class="dark-red-button red-button-common">
    	<a href="<?= url('consultation') ?>" type="button" class="btn btn-lg btn-red-custom waves-effect waves-light">不妊治療無料相談はこちら</a>
		</div>
		
	</div>