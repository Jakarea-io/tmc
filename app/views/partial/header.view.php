<!DOCTYPE html>
<html lang="en">
	<head>
		<title><?= isset($title)?$title:'TM Clinic' ?></title>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<?php style('css/bootstrap.min.css'); ?>
		<link href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" rel="stylesheet" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
		<link href="https://cdnjs.cloudflare.com/ajax/libs/mdbootstrap/4.5.15/css/mdb.min.css" rel="stylesheet">
		<link href="https://fonts.googleapis.com/css?family=Montserrat:400,500,800" rel="stylesheet">
		<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/css/datepicker.css" rel="stylesheet" type="text/css" />
		
		<?php style('css/style.css'); ?>
		<?php style('css/watashino.css'); ?>
	</head>
	<body>
		<nav class="navbar navbar-dark mb-nav navbar-width-screen">
			<div>
				<a class="navbar-brand" href="<?=url('')?>"> 
					<?= image('img/logo.png','logo-img','Logo')?>
				</a>
				<!-- account toggle -->
				<button class="navbar-toggler pull-right top-button-toggler-mb-screen" type="button" data-toggle="collapse" data-target="#account" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>
				</button>
				<div class="collapse navbar-collapse mb-screen-collapse" id="account">
					<ul class="navbar-nav ml-auto md-screen-navbar-nav">
						<li class="nav-item">
							<a class="nav-link " href="<?=url('')?>">トップページ</a>
						</li>
						<li class="nav-item">
							<a class="nav-link" href="<?=url('about')?>">クリニック案内</a>										
						</li>
						<li class="nav-item">
							<a class="nav-link" href="<?=url('contents')?>" >診療内容と費用</a>										
						</li>
						<li class="nav-item">
							<a class="nav-link" href="<?=url('sample')?>">症例集</a>										
						</li>
						<li class="nav-item">
							<a class="nav-link" href="<?=url('column')?>" >コラム (不妊治療研究室)</a>					
						</li>
						<li class="nav-item">
							<a class="nav-link" href="<?=url('faq')?>">Q & A</a>										
						</li>
						<li class="nav-item">
							<a class="nav-link" href="<?=url('contact')?>">お問い合わせはこちら</a>										
						</li>
					</ul>
				</div>
				<a type="button" href="<?= url('contact')?>" class="btn contact-button contact-mb-screen"><i class="fas fa-envelope"></i>お問い合わせはこちら</a>
			</div>
		</nav>

		<div class="container">
			<div class="row second-menu-navbar">
				<div class="col-md-12">
					<nav class="navbar navbar-toggleable-md container-fluid main-navbar mb-nav-menu-wrapper">
						<div class="main-navbar-wrapper opaque-navbar">
							<div class="container">
								<div class="fixed-button-toggler-wrapper pull-right">
									<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
									<i class="fas fa-bars navbar-toggler-icon"></i>
									</button>
								</div>
								<div class="collapse navbar-collapse second-menu-mb-screen" id="navbarSupportedContent">
									<ul class="navbar-nav bottom-menu-item">
									<li class="nav-item">
										<a class="nav-link" href="<?=url('about')?>">クリニック案内</a>										
									</li>
									
									<li class="nav-item">
										<a class="nav-link"href="<?=url('contents')?>">診療内容と費用</a>										
									</li>

									<li class="nav-item">
										<a class="nav-link" href="<?=url('sample')?>">症例集</a>										
									</li>	
									<li class="nav-item">
										<a class="nav-link" href="<?=url('column')?>">コラム (不妊治療研究室)</a>										
									</li>
								
									<li class="nav-item">
            							<a class="nav-link" href="<?=url('faq')?>" >Q & A</a>										
            						</li>
									</ul>
								</div>
							</div>
						</div>
					</nav>
				</div>
			</div>
		</div>