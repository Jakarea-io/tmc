
<div class="container">	
	<div class="row">
		<div class="col-md-12 footer-wrapper">
			<div class="footer-wrapper-inner">
				<div class="title-subtitle">
					<h2>東京TMクリニック不妊外来</h2> 
					
				</div>
				<div class="footer-nav-list">
					<ul>
						<li><a class="#" href="<?=url('about')?>">クリニック案内</a></li>
						<li class=""><a class="" href="<?=url('contents')?>">診療内容と費用</a></li>
						<li class="">
							<a class="" href="<?=url('sample')?>">症例集</a>										
						</li>	
						<li class="">
							<a class="" href="<?=url('column')?>">コラム(不妊治療研究室)</a>										
						</li>
						<li class="">
							<a class="" href="<?php url('consultation') ?>">Q&A</a>										
						</li>
					</ul>
				</div>
				<p class="copyright">Copyright (C) 東京TMクリニック不妊外来 All Rights Reserved.</p>
			</div>	
		</div>
	</div>
</div>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<!-- Bootstrap tooltips -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.4/umd/popper.min.js"></script>
<!-- Bootstrap core JavaScript -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.2.1/js/bootstrap.min.js"></script>
<!-- MDB core JavaScript -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/mdbootstrap/4.7.3/js/mdb.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.js"></script>


<script type="text/javascript" >
$("#radio-wrapper").click(function(){
	if ($("#female").is(":checked")) {
		    $("#hide-male").hide();
		    $("#hide-female").show();
		} 
		
	if ($("#male").is(":checked")) {
		    $("#hide-male").show();
		    $("#hide-female").hide();
		} 
	})


	function toggleIcon(e) {
    $(e.target)
        .prev('.panel-heading')
        .find(".more-less")
        .toggleClass('fa-angle-down fa-angle-up');
	}
	$('.panel-group').on('hidden.bs.collapse', toggleIcon);
	$('.panel-group').on('shown.bs.collapse', toggleIcon);


	



	$('#question1Dropdown').change(function(e) {
		console.log('show',e.target.value);
		var showDiv = e.target.value;
		var showDiv4 = e.target.value;
		if(showDiv == 'いる'){
			$('#showQuestion1Dropdown').css("display", "block")
		}
		
		else if(showDiv4 == '異常'){
			$('#showQuestion4Dropdown').css("display", "block")
		}
		else {
			$('#showQuestion1Dropdown').hide();
		}
	});

	// $('#question4Dropdown').change(function(e) {
	// 	var showDiv4 = e.target.value;
	// 	if(showDiv4 == '異常'){
	// 		$('#showQuestion4Dropdown').css("display", "block")
	// 	}
	// 	else {
	// 		$('#showQuestion4Dropdown').hide();
	// 	}
	// });
	$('#question12fire').change(function(e) {
		var showDiv12 = e.target.value;
		console.log('showDiv12 ', showDiv12);
	
		if(showDiv12 == '整'){
			$('#adjustmentPulldown').css("display", "block")
			$('#iregularityPulldown').hide();
		}

		if(showDiv12 == '不整') {
			$('#iregularityPulldown').css("display", "block")
			$('#adjustmentPulldown').hide();
		}
		

	});

$('#maleQuestionWrapper').change(function(e) {
		var maleQuestion1 = e.target.value;
		console.log('statieeec', maleQuestion1);

		if(maleQuestion1 == 'はい') {
			$('#maleQuestionPulldown').css("display", "flex");
		}
		if(maleQuestion1 == 'いいえ')  {
			$('#maleQuestionPulldown').hide();
		}
	});


	$('#maleqInputPulldown').change(function(e) {
		var maleqInptPuldown = e.target.value;
		if(maleqInptPuldown == 'はい') {
			$('.input-wrapper').css("display", "block");
		}
		else {
			$('.input-wrapper').hide();
		}
	});



	$('#q3smokeCigarrateWrapper').change(function(e) {
		var q3smokeCigarrate = e.target.value;
		

		if(q3smokeCigarrate == 'はい') {
			$('#q3smokeCigarrate').css("display", "flex");
		}
		else {
			$('#q3smokeCigarrate').hide();
		}
	});


	$('#q3alcoholWrapper').change(function(e) {
		var q3alcohol= e.target.value;
		

		if(q3alcohol == 'はい') {
			$('#q3Alcohol').css("display", "block");
		}
		else {
			$('#q3Alcohol').hide();
		}
	});
	$('#allergyCHeckboxask').change(function(e) {
		var allergyCHeckbox= e.target.value;
		

		if(allergyCHeckbox == 'はい') {
			$('#doubleCheckboxPulldown').css("display", "block");
		}
		else {
			$('#doubleCheckboxPulldown').hide();
		}
	});
	$('#maleq10').change(function(e) {
		var mq10= e.target.value;
		

		if(mq10 == 'はい') {
			$('#selectmaleQuestion10').css("display", "block");
		}
		else {
			$('#selectmaleQuestion10').hide();
		}
	});
	$('#maleq19doubleCheckWrapper').change(function(e) {
		var maleq19= e.target.value;
		

		if(maleq19 == 'はい') {
			$('#maleq19doubleCheck').css("display", "block");
		}
		else {
			$('#maleq19doubleCheck').hide();
		}
	});

	$('#male20Wrapper').change(function(e) {
		var male20= e.target.value;
		if(male20 == 'はい') {
			$('#male20Pulldown').css("display", "block");
		}
		else {
			$('#male20Pulldown').hide();
		}
	});

	$('#male21Wrapper').change(function(e) {
		var male21 = e.target.value;
		if(male21 == 'はい') {
			$('#male21Pulldown').css("display", "block");
		}
		else {
			$('#male21Pulldown').hide();
		}
	});
	$('#male26Wrapper').change(function(e) {
		var male26 = e.target.value;
		if(male26 == 'はい') {
			$('.inputWrapperq26').css("display", "block");
		}
		else {
			$('.inputWrapperq26').hide();
		}
	});
	$('#maleq27Wrapper').change(function(e) {
		var male27 = e.target.value;
		if(male27 == 'はい') {
			$('.inputWrapperq27').css("display", "block");
		}
		else {
			$('.inputWrapperq27').hide();
		}
	});






	$('#dateRangePullDown').change(function(e) {
		var dateRange = e.target.value;
		console.log('maleQuestion1 ', dateRange);

		if(dateRange == 'はい') {
			$('#dateRangeWrapper').css("display", "flex");
		}
		else {
			$('#dateRangeWrapper').hide();
		}
	});

	$('#question3SelectPulldown').change(function(e) {
		var q3SelectPulldown = e.target.value;
		console.log('q3SelectPulldown ', q3SelectPulldown);

		if(q3SelectPulldown == 'はい') {
			$('#q2selectHide').css("display", "block");
		}
		else {
			$('#q2selectHide').hide();
		}
	});

	$('#q10Wrapper').change(function(e) {

		var q10display = e.target.value;

		if(q10display == 'はい') {
			$('.q10blockContainer').css("display", "block");
		}
		if(q10display == 'いいえ')  {
			$('.q10blockContainer').hide();
		}
	
	});

	

		function showProfileFrom() {
		
		document.getElementById("basicInfoBlock").style.display = "block";
	}






$(function() {
$("#datepicker").datepicker({
		language: "es",
		todayHighlight: true
});
});

$(function() {
$("#datepicker1").datepicker({
		language: "es",
		todayHighlight: true
});
$("#datepicker2").datepicker({
		language: "es",
		todayHighlight: true
});
$("#datepicker3").datepicker({
		language: "es",
		todayHighlight: true
});

});




$(document).ready(function(){
var quantitiy=0;
$('.quantity-right-plus').click(function(e){
// Stop acting like a button
e.preventDefault();
// Get the field name
var quantity = parseInt($('#quantity').val());
// If is not undefined
$('#quantity').val(quantity + 1);
// Increment
});

$('.quantity-left-minus').click(function(e){
	// Stop acting like a button
	e.preventDefault();
	// Get the field name
	var quantity = parseInt($('#quantity').val());
	
	// If is not undefined
	
	// Increment
	if(quantity>0){
	$('#quantity').val(quantity - 1);
	}
});

});


$('.input-daterange').datepicker({
todayBtn: true,
autoclose: true,
language: "es",
todayHighlight: true
});

$('.input-daterange input').each(function() {
$(this).datepicker('clearDates');
});

var height = $(".fixit").offset().top
	window.onscroll = function() {
		if (window.innerWidth > 900 && (document.body.scrollTop > height || document.documentElement.scrollTop > height)) {
			if(document.querySelector(".fixit") !== null)
		    	document.querySelector(".fixit").classList.add("fixed")
		} else {
			if(document.querySelector(".fixit") !== null)
		    	document.querySelector(".fixit").classList.remove("fixed")
		}
}

</script>
</body>
</html>

