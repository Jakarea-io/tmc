<div class="container contact-form-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <div class="contact-form-inner">
                <div class="contact-title heading-top">
                    <h2 class="h1-responsive text-center my-4"> お問い合わせフォーム</h2>
                    <p class="text-center w-responsive mx-auto"></p>
                </div>
            </div>
        </div>
        <hr>
    </div>
</div>
<section class="contact-form common-section">
    <div class="container">
        <div class="row">
            <?php if(isset($_SESSION["message"])){ ?>
            <div class="col-md-8 offset-md-2 first-step-wrapper ">
                <div class="alert alert-success" role="alert">
                    <?= $_SESSION["message"]?>
                </div>
            </div>
            <?php unset($_SESSION["message"]); } ?>
            <div class="col-lg-12">
                <form name="contact-form" role="form" id="inputForm" action="<?= url('contact') ?>" method="POST">
                    <div class="row setup-content justify-content-md-center" id="step-1">
                        <div class="col-12 col-lg-7 form-content-cover">
                            <div class="col-md-12 card first-step-wrapper">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="name" class="<?= hasError('name')? "text-danger":''?>">お名前（ニックネーム可能）</label>
                                            <input type="text" class="form-control" id="name" name="name" value="<?= old('name') ?>" placeholder="お名前（ニックネーム可能）">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="email" class="<?= hasError('email')? "text-danger":''?>">返信先メールアドレス</label>
                                            <input type="text" class="form-control" id="email" name="email" value="<?= old('email') ?>" placeholder="返信先メールアドレス">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="md-form">
                                            <div class="left-icon-title">
                                                <div class="genter-title <?= hasError('sex')? "text-danger":''?>">性別</div>
                                            </div>
                                            <div class="custom-radio-wrapper">
                                                <div class="custom-control custom-radio custom-control-inline">
                                                    <input type="radio"  value= '女性' class="custom-control-input" id="sex1" name="sex">
                                                    <label class="custom-control-label" for="sex1">男性</label>
                                                </div>
                                                <div class="custom-control custom-radio custom-control-inline">
                                                    <input type="radio"  value= '男性' class="custom-control-input" id="sex2" name="sex" >
                                                    <label class="custom-control-label" for="sex2">女性</label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="md-form">
                                            <div class="left-icon-title">
                                                <div class="genter-title <?= hasError('age')? "text-danger":''?>">年齢</div>
                                            </div>
                                            <div class="custom-radio-wrapper">
                                                <div class="custom-control custom-radio custom-control-inline">
                                                    <input type="radio"  value= '20代 ' class="custom-control-input" id="age1" name="age">
                                                    <label class="custom-control-label" for="age1">20代 </label>
                                                </div>
                                                <div class="custom-control custom-radio custom-control-inline">
                                                    <input type="radio"  value= '30代  ' class="custom-control-input" id="age2" name="age">
                                                    <label class="custom-control-label" for="age2">30代  </label>
                                                </div>
                                                <div class="custom-control custom-radio custom-control-inline">
                                                    <input type="radio"  value= '40代 ' class="custom-control-input" id="age3" name="age">
                                                    <label class="custom-control-label" for="age3">40代 </label>
                                                </div>
                                                <div class="custom-control custom-radio custom-control-inline">
                                                    <input type="radio"  value= '50代 ' class="custom-control-input" id="age4" name="age">
                                                    <label class="custom-control-label" for="age4">50代 </label>
                                                </div>
                                                <div class="custom-control custom-radio custom-control-inline">
                                                    <input type="radio"  value= '60代 ' class="custom-control-input" id="age5" name="age">
                                                    <label class="custom-control-label" for="age5">60代 </label>
                                                </div>
                                                <div class="custom-control custom-radio custom-control-inline">
                                                    <input type="radio"  value= '70代 ' class="custom-control-input" id="age6" name="age">
                                                    <label class="custom-control-label" for="age6">70代 </label>
                                                </div>
                                                <div class="custom-control custom-radio custom-control-inline">
                                                    <input type="radio"  value= '80代以上 ' class="custom-control-input" id="age7" name="age">
                                                    <label class="custom-control-label" for="age7">80代以上 </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="name" class="<?= hasError('question')? "text-danger":''?>">お問い合わせ内容</label>
                                            <textarea type="text" id="question" name="question" rows="4" placeholder="お問い合わせ内容" class="form-control md-textarea"><?= old('question') ?></textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="dark-red-button">
                                    <button type="submit" class="btn btn-lg btn-red-custom waves-effect waves-light"><i class="fas fa-check-square"></i> 送信する </button>
                                </div>
                            </div>
                        </div>
                    </div>
            </div>
            </form>
        </div>
        <div class="dark-red-button red-button-common">
            <a href="<?= url('consultation') ?>" class="btn btn-lg btn-red-custom waves-effect waves-light"> 不妊治療無料相談はこちら </a>
        </div>
    </div>
</section>