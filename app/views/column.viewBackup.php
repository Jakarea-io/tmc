<div class="container">
	<div class="row top-about-bg-container">
		<div class="col-md-12 bg-pink">
			<div class="column-bg-title-wrapper">
				<h2 align="center">COLUMN</h2>
				<h4 align="center">コラム</h4>
			</div>
		</div>
		<div class="col-md-12 column-bg-wrapper">
			<div class="row">
			<div class="column-container col-md-12">
				<ol class="breadcrumb">
					<li class="breadcrumb-item"><a href="#">TOP  <i class="fas fa-angle-right breadcumb-icon"></i> </a></li>
					<li class="breadcrumb-item active"> コラム</li>
				</ol>
			</div>
		</div>
			<div class="column-bg-content">
				<h2 align="center">TMクリニック不妊外来研究員の<br>
					目からウロコの<br>
					不妊治療なるほどコラム</h2>
			</div>
		</div>
	</div>
	

	<div class="row column-images-wrapper">
		<div class="col-md-10 offset-md-1 column-common-img-container">
			<div class="col-md-6">
				<a href="<?= url('column-under')?>">
					<?= image('img/column-img-1.png','common-img column-single-img-common','column-img-1')?>
					<div class="comlumn-text">
						<span class="text-column-inner">日本初上陸にして、<br>
						日本唯一の妊娠率<br>
						89%英国式療法とは</span>	
					</div>
				</a>
			</div>
			<div class="col-md-6">
				<div class="column-single">
					<a href="<?= url('column-under')?>">
						<?= image('img/column-img-2.png','common-img column-single-img-common','column-img-1')?>
						<div class="comlumn-text">
							<span class="text-column-inner">体外受精でも<br>
								妊娠率20〜35%日本の<br>
								不妊治療の不都合な真実</span>	
						</div>
					</a>
				</div>
			</div>
		</div>
		<div class="col-md-10 offset-md-1 column-common-img-container">
			<div class="col-md-6">
				<div class="column-single">
					<a href="<?= url('column-under')?>">
						<?= image('img/column-img-3.png','common-img column-single-img-common','column-img-1')?>
						<div class="comlumn-text">
							<span class="text-column-inner">不妊治療にかかる費用</span>	
						</div>
					</a>
				</div>
			</div>
			<div class="col-md-6">
				<div class="column-single">
					<a href="<?= url('column-under')?>">
						<?= image('img/column-img-4.png','common-img column-single-img-common','column-img-1')?>
						<div class="comlumn-text">
							<span class="text-column-inner">不妊治療ステップアップの<br>
							先に待ち受ける負担</span>	
						</div>
					</a>
				</div>
			</div>
		</div>
		<div class="col-md-10 offset-md-1 column-common-img-container">
			<div class="col-md-6">
				<div class="column-single">
					<a href="<?= url('column-under')?>">
						<?= image('img/column-img-5.png','common-img column-single-img-common','column-img-1')?>
						<div class="comlumn-text">
							<span class="text-column-inner">不妊治療の不条理</span>	
						</div>
					</a>
				</div>
			</div>
			<div class="col-md-6">
				<div class="column-single">
					<a href="<?= url('column-under')?>">
						<?= image('img/column-img-6.png','common-img column-single-img-common','column-img-1')?>
						<div class="comlumn-text">
							<span class="text-column-inner">男性不妊と女性不妊</span>	
						</div>
					</a>
				</div>
			</div>
		</div>
		<div class="col-md-10 offset-md-1 column-common-img-container">
			<div class="col-md-6">
				<div class="column-single">
					<a href="<?= url('column-under')?>">
						<?= image('img/column-img-7.png','common-img column-single-img-common','column-img-1')?>
						<div class="comlumn-text">
							<span class="text-column-inner">大正時代は「45歳以上の母」<br>
							が現在の【21倍】存在</span>	
						</div>
					</a>
				</div>
			</div>

			<div class="col-md-6">
				<div class="column-single">
					<a href="<?= url('column-under')?>">
						<?= image('img/column-img-8.png','common-img column-single-img-common','column-img-1')?>
						<div class="comlumn-text">
							<span class="text-column-inner">何度体外受精を行なっても<br>
							妊娠に至らない原因</span>	
						</div>
					</a>
				</div>
			</div>
		</div>
	</div>


		<div class="row">
			<div class="col-md-10 offset-md-1">
				<div class="column-bottom-before-title">
				コラム一覧
				</div>	
				<ul class="column-bottom-before-list">
					<li >日本初上陸にして、日本唯一の妊娠率89％英国式療法とは</li>
					<li >体外受精でも妊娠率20〜35%日本の不妊治療の不都合な真実</li>
					<li >不妊治療にかかる費用</li>
					<li >不妊治療ステップアップの先に待ち受ける負担</li>
					<li >不妊治療の不条理</li>
					<li >男性不妊と女性不妊</li>
					<li>大正時代は「45際以上の母」が現在の【21倍】存在</li>
					<li>何度体外受精を行なっても妊娠に至らない原因</li>
				</ul>
				<h3 class="red-text column-after-red-text">※この治療法は、患者の症状に合わせて最適な処方が異なります。
				もしご自身に合った治療法をお知りになりたい場合は、下記の無料相談(問診)を通してお問い合せ下さい。</h3>
			</div>
		</div>

		<div class="dark-red-button red-button-common">
    	<a href="http://projects.cloudproduction.co.bd/tmclinic/consultation" type="button" class="btn btn-lg btn-red-custom waves-effect waves-light">不妊治療無料相談はこちら</a>
		</div>
		
	</div>