<!-- Code started -->
<div class="container">
	<div class="row">
		<div class="col-md-12">
			<br>
			<br>
			<br>
		</div>
	</div>
</div>
<div class="container">
	<div class="row">
		<?php view('partial/message') ?>
		<div class="col-md-10">
			<div class="panel-body">
				<form name="contact-form" role="form" id="inputForm" action="<?= url('login') ?>" method="POST">
					<div class="row setup-content justify-content-md-center" id="step-1">
						<div class="col-7 col-lg-7 form-content-cover">
							<div class="col-md-12 card first-step-wrapper">
								<div class="panel-heading">
									<h3 class="panel-title">Login Form</h3>
								</div>
								<div class="row">
									<div class="col-md-12">
										<div class="md-form <?= hasError('email')? "has-danger":''?>">
											<i class="fas fa-envelope-square <?= hasError('email')? "danger-icon":'text-default'?> prefix"></i>
											<input type="text" id="email" name="email" value="<?= old('email') ?>" class="form-control">
											<label for="email">Email Address</label>
										</div>
										<div class="form-control-feedback feedback" id="fed_email"><?= showError('email') ?></div>
									</div>
								</div>
								<div class="row">
									<div class="col-md-12">
										<div class="md-form <?= hasError('password')? "has-danger":''?>">
											<i class="fas fa-blender-password <?= hasError('password')? "danger-icon":'text-default'?> prefix"></i>
											<input type="text" id="password" name="password" value="<?= old('password') ?>" class="form-control">
											<label for="password">Password</label>
										</div>
										<div class="form-control-feedback feedback" id="fed_password"><?= showError('password') ?></div>
									</div>
								</div>
								<div class="text-center text-md-center button-wrapper">
									<button class="btn btn-default pull-right" type="submit">  <i class="fas fa-share-square"></i>Login </button>
								</div>
							</div>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>