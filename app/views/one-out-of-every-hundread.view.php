<div class="container columnunder_container">
	<div class="row">
		<div class="col-md-12">
			<div class="content-container">
				<ol class="breadcrumb">
					<li class="breadcrumb-item"><a href="#d10">TOP  <i class="fas fa-angle-right breadcumb-icon"></i> </a></li>
					<li class="breadcrumb-item">コラム <i class="fas fa-angle-right breadcumb-icon"></i></li>
					<li class="breadcrumb-item active"> 100人に1人が45歳以上の母だった大正時代</li>
				</ol>
			</div>
		</div>
    </div>
    <div class="row">
		<div class="col-md-12">
			<div class="col-wrapper">
				<div class="col-under-firstone">
					<?= image('img/col-under-top.png','common-img colunder-top1-web','col-under-top')?>
					<?= image('img/colunder-top-mobile1.png','common-img colunder-top-mobile1','colunder-top-mobile1')?>
					<div class="colunder-top1-contentwrapper">
						<div class="col-under-top1-wrapper">
							<?= image('img/col-under-top1-arrow.png','','col-under-top1-arrow')?>
							<h5 class="colunder-arrow-text">目からウロコの</h5>
							
						</div>
						<h3>不妊治療<span class="col-top1-span">なるほどコラム</span></h3>
					</div>
				</div>
				<div class="col-under-secondone">
					<?= image('img/one-out-of-top-web.png','common-img colunder-top-mobile2web','one-out-of-top-web')?>
                    <?= image('img/combine-top-mobile.png','common-img colunder-top-mobile2mobile','combine-infertility-mobile')?>
                    <div class="media colunder-common-media inconvenient-media">
						<div class="col-common-pink-circle">Column</div>
						<div class="media-body">
							<div class="common-mid-text-banner">
								<h4>100人に1人が45歳以上の母だった大正時代</h4>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-9 col-sm-12">
			<div class="column-under-top-list-wrapper">
				<div class="line-wrapper">
					<h3 class="column-under-list-title blue-text-common">目次</h3>
				</div>
				<ul class="columnunder-bottom-before-list">
					<li><a href="#dl">大正時代。100人に１人は45歳以上の母親から生まれていたという事実</a></li>
					<li><a href="#d2">不妊治療がなくても45歳以上で自然妊娠、出産していた大正時代</a></li>
                    <li><a href="#d3">30歳で生殖能力が落ちるというウソ</a></li>
                    <li><a href="#d4">外科的療法一切なし。妊娠率89%を誇る英国式療法</a></li>
				</ul>
			</div>
			
            <div class="row column-under-text-1">
				<div class="col-md-12">
                    <div class="column-bottom-before-title">
                        <a href="#d11">大正時代。<br>
                            100人に１人は45歳以上の母親から生まれていたという事実
                        </a>
                    </div>
                    <div class="media arrange-order">
						<div class="col-under-media-text-container one">
							<p>コラムで、体外受精で出産まで至る確率は約11%と述べてきましたが、それはあくまでも、平均的な話です。</p>

                            <p>厳密には、年齢によって出産率は変動します。</p>

                            <p>右図は、1回の治療で出産に至る確率を示しています。</p>

                            <p>厚生労働省で作成された日本産婦人科学会のデータによれば、30～45歳までの女性が1回の不妊治療で出産に至る確率は、平均7.4%という事になります。</p>

                            <p>45歳だと、1回の不妊治療で出産に至る確率は、0.6%しかありません。<br>
                            その為、40歳を境に、次第に妊娠を諦める女性が増えていきます。</p>
						</div>
						<div class="media-body">
							<?= image('img/one-out-of-media1.png','common-responsive-img','one-out-of-media1')?>
						</div>
                    </div>
                    <p>しかし、『卵子老化の真実』（文春新書）の著者で、日本で唯一の出産専門ジャーナリストの河合蘭氏によれば、実は<span class="yellow-bg">1925年の日本では、40～45歳の女性による出生数が約12万人、45歳～50歳で約２万人。<br>
                    45歳以上母親から生まれた赤ちゃんの数は、現在の21倍だったそうです。<br>
                    総出生件数からみると、100人に1人の赤ちゃんの母親が45歳以上となります。</span></p>
                    <p>また、出生数の割合からみると、</p>
                    <p class="common-bold-black">1925年：30歳以上の方の出生数の全出生数に占める割合…40％　（うち35歳以上…20％）<br>
                    2014年：30歳以上の方の出生数の全出生数に占める割合…60％以上　（うち35歳以上…25％以上）</p>
                    <div class="media">
						<div class="col-under-media-text-container">
							<p>データの残っている1925年の35歳以上の女性による出産数は42万8299人。<br>
                                2014年が27万6767人なので、その約1.5倍にあたります。</p>
                            <p><span class="yellow-bg">大正時代には、母親が50歳以上でも、3648人の赤ちゃんが生まれています。</span></p>
						</div>
						<div class="media-body">
							<?= image('img/out-of-media2.png','common-responsive-img','out-of-media2')?>
						</div>
                    </div>
                    <p><span class="common-bold-black">なお、現在の総人口は、1925年当時の約2倍になっていますから、1925年のデータは、今より少ない分母での数値になります。</span><br>
                        ※当時、今と同じくらいの総人口があったなら、母親が45歳以上で生まれてくる赤ちゃんは、さらに多かったことでしょう。</p>
                </div>
            </div>
             <div class="row column-under-text-1">
				<div class="col-md-12">
				    <div class="column-bottom-before-title"><a href="#d5">不妊治療がなくても45歳以上で自然妊娠、出産していた大正時代</a></div>
					<div class="media cost-of-media-wrap">
						<?= image('img/out-of-one-media3.png','common-responsive-img','out-of-one-media3')?>
						<div class="media-body common-media-body">
							<p>ここで見逃せない事実として、<span class="red-yellow-bg">そもそも1952年の日本には、不妊治療などなかった</span>事が挙げられます。</p>
                            <p>つまり、<span class="red-yellow-bg">上記は全て「自然妊娠」による数値</span>なのです。</p>
                            <p>大正時代より現代の方が高度生殖医療が発達しているというなら、40歳以上の妊娠・出産率も、当時より増えていなければ(結果が伴って居なければ)おかしいと思いませんか？</p>
						</div>
					</div>
					<p class="">しかし、かつてより不妊治療が進歩したはずの日本で、40歳を超えると、自然妊娠なんてまず望めず、体外受精でも妊娠率1%未満である現状を、私はある意味、当然と考えています。</p>
                    <p>ただし、私はそれを「40代だから当然」と言っているのではありません。</p>
                    <p>本来、女性には40代でも妊娠する力が備わっているのですが、体外受精に挑む40代女性は、自然妊娠できずに居るから、高度生殖医療に踏み切っているわけですよね。</p>
                    <p>要するに、<span class="common-bold-black">「40代だから」ではなく、自然妊娠できない体内環境のまま、体外受精に臨んでいるわけですから、成功率は、低くて当然なのです。</span></p>
				</div>
            </div>
            <div class="row column-under-text-1">
				<div class="col-md-12">
                    <div class="column-bottom-before-title">
                        <a href="#d6">30歳で生殖能力が落ちるというウソ</a>
                    </div>
                    <div class="media arrange-order">
						<div class="col-under-media-text-container one">
							<p>婦人科顧問医であるイギリスのオックスフォード大学ティム・チャイルド教授は、女性が40歳のカップルが12カ月間にわたって妊活をした場合、<span class="common-bold-black">「妊娠する可能性は約50％」</span>だと述べています。</p>

                            <p>これは、<span class="common-bold-black">日本の体外受精による40代女性の妊娠率より、はるかに高い数値</span>です。</p>
                            <p>インペリアル・カレッジ・ロンドンで生殖研究を行うロバート・ウィンストン名誉教授もそれに同意し、</p>
						</div>
						<div class="media-body">
							<?= image('img/one-out-of-media4.png','common-responsive-img','one-out-of-media4')?>
						</div>
                    </div>
                    <div class="bol-under-isto cost-of-isto one-of-one-box-wrap">
                        <p class="common-bold-black one-of-one-box-text"><i>「女性は本人たちが思うよりも長く、生殖能力を維持します。
                        第一子の出産平均年齢は現在、30歳を超えていますが、イギリスでは出生率の低下は認められません。<br>
                        30歳で生殖能力が落ちるという考え方はまったく無意味です」</i></p>
                    </div>
                    <p>と提唱しています。</p>
                    <p>性と生殖に関するリプロダクティブヘルスのコンサルタント、ヘレン・マンロー医師も、次のように話しています。</p>
                    <p>「意外なほど高齢でも妊娠は可能なので、『35歳を過ぎたら避妊は不要』などと考えてはいけません。<br>
                    40歳を超えても妊娠は確実に可能です。<br>
                    『35歳の壁』の話を散々聞かされてきたせいで、妊娠できることをつい忘れてしまう女性がいますが。
                    元イギリス首相トニー・ブレアの妻シェリー・ブレアも、45歳で妊娠しました。<br>
                    生理が完全に止まってしまうまで、妊娠は可能です」<br>
                    <a class="common-anchor" href="#d7">(https://www.buzzfeed.com/jp/tomchivers/this-is-what-you-need-to-know-about-fertility-1)</a></p>
                    <p>上記は現代のイギリスでの話ですが、<span class="common-bold-black">日本でも、確かに大正時代には40歳以上で出産している事例が数多くあった</span>のです。</p>
                    <p><a class="common-anchor" href="#d8">コラム２章　体外受精でも妊娠率20～35%日本の不妊治療の不都合な真実</a>でもお伝えしましたが、<span class="common-bold-black">日本は体外受精の治療件数が世界一多く、それに対する出産率が世界一低い国です。</span></p>
                </div>
            </div>

            <div class="row column-under-text-1">
				<div class="col-md-12">
				    <div class="column-bottom-before-title"><a href="#d9">外科的療法一切なし。妊娠率89%を誇る英国式療法</a></div>
                    <div class="media arrange-order">
						<div class="col-under-media-text-container one">
							<p>国内では、体外受精が最後の砦と認識されがちで、体外受精を何度繰り返しても妊娠できず、失望する女性が少なくありませんが、<span class="common-bold-black">国内の治療法が全てではありません。</span></p>
						</div>
						<div class="media-body">
							<?= image('img/col-under-left4.png','common-responsive-img','col-under-left4')?>
						</div>
                    </div>
				</div>
            </div>
            
			
			<div class="row column-under-text-1">
				<div class="col-md-12">
					
					<p><span class="common-bold-black">不妊治療や巷の「妊活」には、様々なアプローチ法が存在しますが、私は、不妊治療は<span class="common-bg-yellowred">「妊娠率(分娩率)」</span>と<span class="common-bg-yellowred">「安全性」</span>で選ぶべきだと考えております。</span></p>
					<p class="common-bg-yellowred">だからこそ、外科的な処置が一切伴わない、妊娠率89%を達成した英国式療法をお勧めしております。</p>
					<p class="column-after-red-text">※この治療法は、患者様の症状に合わせて最適な処方が異なります。<br>もしご自身に合った治療法をお知りになりたい場合は、下記の無料相談(問診)を通して<br>お問い合せ下さい。</p>
	　			</div>
			</div>

            <div class="dark-red-button red-button-common">
				<a href="http://projects.cloudproduction.co.bd/tmclinic/consultation" type="button" class="btn btn-lg btn-red-custom waves-effect waves-light">不妊治療無料相談はこちら</a>
			</div>
			<div class="ceo-bottom-container">
				<div class="line-wrapper">
					<h3 class="column-under-list-title blue-text-common">監修</h3>
				</div>
				<div class="colunder-bottommedia-container">
					<div class="media">
						<div class="col-under-media-text-container">
							<?= image('img/ceo1.png','','ceo')?>
						</div>
						<div class="media-body">
							<p class="blue-text-common ceo-right-text">外来部長 <span class="pt-up-font">山下明子先生</span><span>医学博士、内科医、神経内科専門医、抗加齢医学専門医</span></p>
						</div>
					</div>
					<div class="media">
						<div class="col-under-media-text-container">
							<?= image('img/ceo2.png','','ceo2')?>
						</div>
						<div class="media-body">
							<p class="blue-text-common ceo-right-text">顧問医師 <span class="pt-up-font">身原正哉先生</span><span>産婦人科専門医・日本抗加齢医学会専門医</span></p>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-3 colunder-right-section">
			<?php view('common/ceo') ?>
		</div>
	</div>
</div>
