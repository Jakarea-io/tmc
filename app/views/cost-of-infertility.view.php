<div class="container columnunder_container">
	<div class="row">
		<div class="col-md-12">
			<div class="content-container">
				<ol class="breadcrumb">
					<li class="breadcrumb-item"><a href="#c13">TOP  <i class="fas fa-angle-right breadcumb-icon"></i> </a></li>
					<li class="breadcrumb-item">コラム <i class="fas fa-angle-right breadcumb-icon"></i></li>
					<li class="breadcrumb-item active"> 日本初上陸の妊娠率89％の英国式療法</li>
				</ol>
			</div>
		</div>
    </div>
    <div class="row">
		<div class="col-md-12">
			<div class="col-wrapper">
				<div class="col-under-firstone">
					<?= image('img/col-under-top.png','common-img colunder-top1-web','col-under-top')?>
					<?= image('img/colunder-top-mobile1.png','common-img colunder-top-mobile1','colunder-top-mobile1')?>
					<div class="colunder-top1-contentwrapper">
						<div class="col-under-top1-wrapper">
							<?= image('img/col-under-top1-arrow.png','','col-under-top1-arrow')?>
							<h5 class="colunder-arrow-text">目からウロコの</h5>
							
						</div>
						<h3>不妊治療<span class="col-top1-span">なるほどコラム</span></h3>
					</div>
				</div>
				<div class="col-under-secondone">
					<?= image('img/infertility-cost-top-web.png','common-img colunder-top-mobile2web','infertility-cost-top-mobile')?>
                    <?= image('img/infertility-cost-top-mobile.png','common-img colunder-top-mobile2mobile','infertility-cost-top-web')?>
                    <div class="media colunder-common-media inconvenient-media">
						<div class="col-common-pink-circle">Column</div>
						<div class="media-body">
							<div class="common-mid-text-banner">
								<h4>不妊治療にかかる費用</h4>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-9 col-sm-12">
			<div class="column-under-top-list-wrapper">
				<div class="line-wrapper">
					<h3 class="column-under-list-title blue-text-common">目次</h3>
				</div>
				<ul class="columnunder-bottom-before-list">
					<li><a href="#c1">ステップアップするにつれて膨れ上がる費用負担</a></li>
					<li><a href="#c2">日本で行われている標準的な不妊治療</a></li>
					<li><a href="#c3">不妊治療は１度で終わらない～治療費平均193万円</a></li>
					<li><a href="#c4">88.6%は出産まで至れない！体外受精の低い出産率</a></li>
					<li><a href="#c5">不妊治療の選択に必要なのは「妊娠率(分娩率)」と「安全性」</a></li>
				</ul>
			</div>
			<div class="column-bottom-before-title"><a href="#c6">ステップアップするにつれて膨れ上がる費用負担
            </a></div>
            
			<div class="row column-under-text-1 incon-truth-second-wrapper cost-of-middle-container">
                <div class="col-md-12">
                <p>これから不妊治療を始めようと考えている方にとって、具体的な治療費は気になるポイントではないでしょうか。</p>
                <?= image('img/infertility-cost-middle.png','infertility-cost-middle common-responsive-img','infertility-cost-middle')?>
                <p>不妊治療は、</p>
                
				<div class="bol-under-isto cost-of-isto">
					<ul>
						<li> <a href="#"> タイミング法 </a> </li>
						<li> <i class="fas fa-long-arrow-alt-right"></i> </li>
						<li> <a href="#"> 人工授精 </a> </li>
						<li> <i class="fas fa-long-arrow-alt-right"></i> </li>
						<li> <a href="#"> 体外受精  </a> </li>
						<li> <i class="fas fa-long-arrow-alt-right"></i> </li>
						<li> <a href="#"> 顕微授精 </a> </li>
					</ul>
				</div>
                    <p>とステップアップしていく事をお伝えしましたが、<span class="red-text">治療の進行に伴い、時間的・肉体的・精神的負担だけでなく、</span><span class="common-bg-yellowred">金銭的負担も大きくなっていきます。</span></p>
                </div>
                
			</div>
			<div class="row column-under-text-1">
				<div class="col-md-12">
                    <div class="column-bottom-before-title"><a href="#c7">日本で行われている標準的な不妊治療</a></div>
                    <h4 class="blue-bg blue-bg-wrapper">タイミング法</h4>
					<div class="media arrangeorder">
						<div class="col-under-media-text-container two">
							<p>女性が最も妊娠しやすい排卵日を、排卵検査薬やエコー<br><span class="yellow-bg">などの検査結果をもとに、医師が推測。</span><br>
                                医師指定のタイミングで性交する事を「タイミング法」と言います。<br>
                                保健が適応されるので、1回数千円程度の費用で済みます。</p>
							<p>ただし、タイミング法は不妊治療の第一段階ではありますが、これは自<span class="common-bold-black">然妊娠で妊娠できる(これまで、ただタイミングが合っていないから妊娠できなかっただけの)夫婦にしか有効ではありません。</span></p>
						</div>
						<div class="media-body one">
							<?= image('img/cost-media1.png','common-responsive-img','cost-media1.png')?>
						</div>
                    </div>
                 </div>
            </div>
           	<div class="row column-under-text-1">
				<div class="col-md-12">
                <h4 class="blue-bg blue-bg-wrapper">人工受精</h4>
					<div class="media arrangeorder">
						<div class="col-under-media-text-container two">
                            <p>不妊治療で保険が適応されるのは、ほとんどタイミング法だけと言えるでしょう。<br><span class="common-bold-black">人工授精から先は、ふつう自費診療と</span>なります。</p>
                            <p>人工授精は、予め採精した男性の精液を、子宮にカテーテルで直接注入する方法です。<br>
                            注入前に、採精した精液の中から元気な精子を選別し、排卵のタイミングに合わせて、元気な精子だけを子宮に送りこみます。保険適用外で1回1～2万円です。</p>
                        </div>
						<div class="media-body one">
						<?= image('img/cost-media2.png','common-responsive-img','cost-media2')?>
						</div>
					</div>
                    <p>不妊の原因が、精子が子宮まで辿り着いていない場合に有効と言えるでしょう。<br>
                    しばしば体外受精と混同されますが、全く違う方法です。</p>
                    <p>ただし、<span class="common-bold-black">若年女性であっても、人工授精を5周期以上続けてわずか3〜5％しか妊娠を期待できないと言われており、<span class="yellow-bg">人工授精による妊娠率は低い</span></span>です。</p>
                    <p>とはいえ、体外受精と比べると、心身・時間・経済的負担が軽いことから、何度もチャレンジしやすい不妊治療です。</p>


                    <h4 class="blue-bg blue-bg-wrapper"> 顕微授精 </h4>
                    <div class="media">
						<div class="col-under-media-text-container">
                            <p>女性側に「採卵」「移植」などの処置が必要になってくる不妊治療です。<br>
                                女性の体内から卵子を取り出し、シャーレーの中で精子と受精させる方法です。</p>
                            <p>受精卵が順調に分割して受精後2日目くらいの「初期胚」または5～6日後の「胚盤胞」の段階まで成長すれば、カテーテルで子宮へ送り込みます。</p>
                            <p>保険が適用されず、体外受精1回あたりにかかる費用は20～60万円です。</p>
                        </div>
						<div class="media-body">
						    <?= image('img/cost-media3.png','common-responsive-img','cost-media3')?>
						</div>
					</div>
                    <p class="common-bold-black">しかし、「体外受精」「顕微授精」には、<span class="yellow-bg">「自然周期法」</span>と<span class="yellow-bg">「刺激周期法」</span>があり(正確には、「刺激周期法」の中にも色々な方法がある)、刺激周期法では、基本的に女性側は自己注射が必要になります。</p>
                    <p>その注射が1アンプル数千円～で、医師の指示に沿って連日注射を打たなければならない為、1ヶ月に必要な薬代も、高額になる場合が殆どです。</p>
 
                    
                    <h4 class="blue-bg blue-bg-wrapper">顕微授精</h4>
                    <div class="media arrangeorder">
						<div class="col-under-media-text-container two">
                            <p>「体外受精」と「顕微授精」の違いは、卵子に精液をふりかけて受精を試みる(体外受精)か、<span class="common-bold-black">卵子に針を差し、精子を直接注入して受精を試みる(顕微授精) </span> かの違いです。</p>
                            
                            <p>精子の数が極端に少なかったり、卵子の壁を破る元気のない精子しか居ない場合などは、体外受精ではなく、顕微授精になります。</p>
                            <p>大半のクリニックでは、体外受精の費用プラス数万円で、顕微授精が行われています。</p>
                        </div>
						<div class="media-body one">
						    <?= image('img/cost-media4.png','common-responsive-img','cost-media3')?>
						</div>
                    </div>
                    
                    <h4 class="blue-bg blue-bg-wrapper">不妊漢方</h4>
                    <div class="media arrangeorder">
						<div class="col-under-media-text-container two">
                            <p>不妊治療の中には、不妊専門の漢方薬が処方される事もあります。</p>
                            
                            <p>漢方による不妊治療の効果に関しては、あまり認めていない医師が多数派だと思いますが、中には、他の治療法と組み合わせて提案する医師も居ますし、不妊漢方を専門とする医師も存在しています。</p>
                            <p>漢方は、保険が適応される顆粒タイプのものと、保健が適応されない、煮だすタイプ(生薬)のものがあります。</p>
                        </div>
						<div class="media-body one">
						    <?= image('img/cost-media5.png','common-responsive-img','cost-media5')?>
                        </div>
                        
                    </div>
                    <p>漢方医の見解では、後者の方が圧倒的に効果があるというのが定説です。</p>
                    <p>漢方にも様々な薬がありますので、一概には言えませんが、保険適応の漢方なら1ヶ月数千円程度、保険適応外の漢方なら1ヶ月3～4万円程度のケースが多いです。</p>
                    <p>ただし、クリニックで体外受精を受ける場合は、かえって漢方が治療の妨げになるという考えから、クリニックの医師に服用を禁止される事もあります。<br>
                    ※クリニック側で処方する薬との相性もあるでしょう。</p>
                 </div>
            </div>

            <div class="row column-under-text-1">
				<div class="col-md-12">
                    <div class="column-bottom-before-title"><a href="#c8">不妊治療は１度で終わらない<br>～治療費平均193万円</a></div>
                    <p>このように、不妊治療にかかる費用は、治療内容によっても様々なのですが、不妊治療費が患者の大きな負担となる要因のひとつとして、<br>
                    <span class="common-bg-yellowred"> 不妊治療は、一度で終わらない事がほとんど</span><br>
                    である事が挙げられます。</p>
					
                    <p>上記でご説明した不妊治療にかかる費用は、いずれも<span class="common-bold-black">「1回あたり(または1ヶ月あたり)」の費用</span>です。</p>
                    <p><span class="common-bold-black">人工授精よりは妊娠の確率が高いと言われている体外受精でも、1回の体外受精で妊娠する確率が約20%程度だとしますと、80%の人は、1回で妊娠に至れません。</span></p>
                    <?= image('img/cost-graph1.png','common-responsive-img cost-graph1','cost-graph1')?>
					<p class="cost-graph1-text">※国立社会保障・人口問題研究所「第15回出生動向基本調査」（2015年6月）</p>
                    <div class="cost-graph-list">
						【調査概要】
						<p>調査対象：10年以内に妊活経験のある20～49歳までの女性<br>
						有効回答数：300名（妊活時の平均世帯年収約500万円）<br>
						調査期間：2017年10月11日（水）～10月30日（月）<br>
						調査方法：インターネット調査
						<br/>
						引用元：<a href="https://www.ninkatsu-voice.jp/">妊活ボイス（https://www.ninkatsu-voice.jp/）</a></p>
					</div>

					<p>不妊治療にはどのくらいの費用が掛かるのか、病院・クリニックの利用状況などを通じて行われたある調査では、
						高度不妊治療（体外受精・顕微授精）にかかった費用は<span class="common-bold-black">平均で約193万円 </span>という結果が出ています。</p>
					<p>どれほど費用を掛けても、必ず妊娠できるとは限らない点が、不妊治療の辛いところです。</p>
                </div>
            </div>
			<div class="row column-under-text-1">
				<div class="col-md-12">
				<div class="column-bottom-before-title"><a href="#c10">88.6%は出産まで至れない！体外受精の低い出産率</a></div>
					<div class="media cost-of-media-wrap">
						<?= image('img/cost-media6.png','common-responsive-img','cost-media6')?>
						<div class="media-body common-media-body">
							<p>体外受精の平均出産率（分母は総治療周期数）は11.４％しかない（2010年のデータ、日本産科婦人科学会資料より）と言われています。</p>
							<p>しかし、<span class="common-bg-yellowred">多くの不妊に悩むカップルが、88.6%は出産まで至れないその確率に、多額の費用を投じているのが現在の日本の実状</span>
							なのです。</p>
						</div>
					</div>
					<p class="common-bold-black">インペリアル・カレッジ・ロンドンで生殖研究を行うロバート・ウィンストン名誉教授は、「クリニックは体外受精(IVF)を実施してお金を儲けている。イギリスで行われる体外受精（IVF）の半分は不要な処置だ」との見解を示しています。</p>
					<p>出典  <a href="https://www.buzzfeed.com/jp/tomchivers/this-is-what-you-need-to-know-about-fertility-1" class="hyperlink-cost">https://www.buzzfeed.com/jp/tomchivers/this-is-what-you-need-to-know-about-fertility-1</a></p>
				</div>
			</div>
			<div class="row column-under-text-1">
				<div class="col-md-12">
					<div class="column-bottom-before-title mobile-br"><a href="#c12">不妊治療の選択に必要なのは<br/>「妊娠率(分娩率)」と「安全性」</a></div>
					<div class="media cost-of-media-wrap arrangeorder">
						<div class="col-under-media-text-container two">
							<p>日本では体外受精こそが最後の砦のように思われがちですが、体外受精は決して万能ではありませんし、不要でも勧められる場合があるのです。</p>
							
							<p class="common-bold-black">>不妊治療や巷の「妊活」には、様々なアプローチ法が存在していますが、私は、不妊治療は<span class="common-bg-yellowred">「妊娠率(分娩率)」</span>と<span class="common-bg-yellowred">「安全性」</span>で選ぶべきだと考えております。</p>
							
						</div>
						<div class="media-body">
							<?= image('img/col-under-left-2.png','common-responsive-img','col-under-left-2')?>
						</div>
					</div>
					<p class="common-bg-yellowred">だからこそ、外科的な処置が一切伴わない、妊娠率89%を達成した英国式療法をお勧めしております。</p>
					<p class="column-after-red-text">※この治療法は、患者様の症状に合わせて最適な処方が異なります。<br>もしご自身に合った治療法をお知りになりたい場合は、下記の無料相談(問診)を通して<br>お問い合せ下さい。</p>
	　			</div>
            </div>
            <div class="dark-red-button red-button-common">
				<a href="http://projects.cloudproduction.co.bd/tmclinic/consultation" type="button" class="btn btn-lg btn-red-custom waves-effect waves-light">不妊治療無料相談はこちら</a>
			</div>
			<div class="ceo-bottom-container">
				<div class="line-wrapper">
					<h3 class="column-under-list-title blue-text-common">監修</h3>
				</div>
				<div class="colunder-bottommedia-container">
					<div class="media">
						<div class="col-under-media-text-container">
							<?= image('img/ceo1.png','','ceo')?>
						</div>
						<div class="media-body">
							<p class="blue-text-common ceo-right-text">外来部長 <span class="pt-up-font">山下明子先生</span><span>医学博士、内科医、神経内科専門医、抗加齢医学専門医</span></p>
						</div>
					</div>
					<div class="media">
						<div class="col-under-media-text-container">
							<?= image('img/ceo2.png','','ceo2')?>
						</div>
						<div class="media-body">
							<p class="blue-text-common ceo-right-text">顧問医師 <span class="pt-up-font">身原正哉先生</span><span>産婦人科専門医・日本抗加齢医学会専門医</span></p>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-3 colunder-right-section">
			<?php view('common/ceo') ?>
		</div>
	</div>
</div>