<div class="container columnunder_container">
	<div class="row">
		<div class="col-md-12">
			<div class="content-container">
				<ol class="breadcrumb">
					<li class="breadcrumb-item"><a href="#i11">TOP  <i class="fas fa-angle-right breadcumb-icon"></i> </a></li>
					<li class="breadcrumb-item">コラム <i class="fas fa-angle-right breadcumb-icon"></i></li>
					<li class="breadcrumb-item active"> 日本初上陸の妊娠率89％の英国式療法</li>
				</ol>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<div class="col-wrapper">
				<div class="col-under-firstone">
					<?= image('img/col-under-top.png','common-img colunder-top1-web','col-under-top')?>
					<?= image('img/colunder-top-mobile1.png','common-img colunder-top-mobile1','colunder-top-mobile1')?>
					<div class="colunder-top1-contentwrapper">
						<div class="col-under-top1-wrapper">
							<?= image('img/col-under-top1-arrow.png','','col-under-top1-arrow')?>
							<h5 class="colunder-arrow-text">目からウロコの</h5>
							
						</div>
						<h3>不妊治療<span class="col-top1-span">なるほどコラム</span></h3>
					</div>
				</div>
				<div class="col-under-secondone">
					<?= image('img/inconvenient-top-img.png','common-img colunder-top-mobile2web','inconvenient-top-img')?>
                    <?= image('img/inconvenient-top-for-mobile.png','common-img colunder-top-mobile2mobile','inconvenient-top-for-mobile2')?>
                    <div class="media colunder-common-media inconvenient-media">
						<div class="col-common-pink-circle">Column</div>
						<div class="media-body">
							<div class="common-mid-text-banner">
								<h4>日本の不妊治療の不都合な真実</h4>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-9 col-sm-12">
			<div class="column-under-top-list-wrapper">
				<div class="line-wrapper">
					<h3 class="column-under-list-title blue-text-common">目次</h3>
				</div>
				<ul class="columnunder-bottom-before-list">
					<li><a href="#i1">1年自然妊娠しなければ不妊治療</a></li>
					<li><a href="#i2">体外受精大国ニッポン。それでも75%は妊娠できない事実</a></li>
					<li><a href="#i3">実は不妊治療後進国だった日本</a></li>
					<li><a href="#i4">顕微授精で高まる自閉症スペクトラム障害リスク</a></li>
					<li><a href="#i5">不妊治療リスクと安全性</a></li>
				</ul>
			</div>
			<div class="column-bottom-before-title"><a href="#i6">1年自然妊娠しなければ不妊治療</a>
			</div>
			<div class="row column-under-text-1 incon-truth-second-wrapper">
                <div class="col-md-12">
                    <div class="media inconvenient-media-wrap arrangeorder">
						<div class="col-under-media-text-container two">
							<p><span class="yellow-bg">「不妊」とは、妊娠を望む健康な男女が避妊をしないで性交をしているにもかかわらず、一定期間妊娠しない事</span>を指し、 日本産科婦人科学会では、この <span class="yellow-bg">「一定期間」について「1年というのが一般的である」と定義</span>しています。</p>
							</p>
							<p>ですから(開始時期は人それぞれですが)、1年以上妊娠しなければ、不妊治療を始めようと考えるカップルも多いでしょう。</p>
							<p>現在の日本では、不妊治療は主に、</p>
						</div>
						<div class="media-body one">
						    <?= image('img/inconvenient-media1.png','inconvenient-media-img common-responsive-img','inconvenient-media1')?>
						</div>
					</div>
					<div class="bol-under-isto inconvenient-mobile">
                    	<span class="sp-block">タイミング法</span><i class="fas fa-long-arrow-alt-right"></i><i class="fas fa-long-arrow-alt-down"></i><span class="sp-block">人工授精</span> <i class="fas fa-long-arrow-alt-right"></i><i class="fas fa-long-arrow-alt-down"></i><span class="sp-block">体外受精</span><i class="fas fa-long-arrow-alt-right"></i><i class="fas fa-long-arrow-alt-down"></i><span class="sp-block">顕微授精</span>
					</div>
                </div>
                <h6 class="outside-text-block margin-leftcol">と「ステップアップ」する方針が採られています。</h6>
                <h6 class="outside-text-block margin-leftcol">タイミング法で妊娠しなければ人工授精、人工授精で妊娠しなければ体外受精、それでも妊娠しなければ顕微授精<br>…という具合に治療を進めて行きますので、「顕微授精」は、不妊治療の最終手段たる位置づけにあります。</h6>
			</div>

			<div class="row column-under-text-1">
				<div class="col-md-12">
					<div class="column-bottom-before-title"><a href="#i7">体外受精大国ニッポン。それでも75%は妊娠できない事実</a></div>
					<p>その為、「体外受精は、他の不妊治療より妊娠する確率は高いけれど、金銭・心身・時間の負担が最も大きいから、それ以外の方法で妊娠できるなら、それに越した事は無い」そんなイメージを持たれがちなのですが、実際には、<span class="common-bg-yellowred">体外受精の妊娠率は20～35%</span>と言われています。</p>
                    <p>妊娠率は年齢によっても異なりますが、下記のグラフをご覧下さい。</p>
                    <?= image('img/graph1.png','center common-responsive-img graph1','graph1')?>

					<div class="media margin-bottom-25">
						<div class="col-under-media-text-container">
							<p>体外受精から分娩(出産)へ至っている確率は、たとえ女性が20代であっても、25%未満なのです。<br>言い換えれば、<span class="common-bg-yellowred">体外受精までステップアップしても、妊娠できない確率が75%です。
                                妊娠できない確率の方が高いのです。</span></p>
							</p>
							<p>また、日本は体外受精の治療件数が、世界一多い国です(右図参照)。</p>
						</div>
						<div class="media-body">
							<?= image('img/graph2.png','common-responsive-img graph1','graph2')?>
						</div>
                    </div>

                    <div class="media">
						<div class="col-under-media-text-container">
							<p>にもかかわらず、<span class="common-bold-black"> 採卵1回あたりの出産数は、世界最下</span>位(右図参照)。</p>
						</div>
						<div class="media-body">
							<?= image('img/graph3.png','common-responsive-img','graph3')?>
						</div>
					</div>
                </div>
            </div>
           	<div class="row column-under-text-1">
				<div class="col-md-12">
					<div class="column-bottom-before-title"><a href="#i8">実は不妊治療後進国だった日本</a></div>
					<div class="media">
						<div class="col-under-media-text-container">
							<p>日本は、不妊治療においては「後進国」と言えるでしょう。私は、不妊治療に悩む方には、これらの事を全て知った上で、治療を選択する権利があると考えています。
                            しかし、現実には、日本の不妊治療の実状をご存知無いまま、不妊治療を受けておられる方が大勢いらっしゃいます。</p>
                        </div>
						<div class="media-body">
						<?= image('img/inconvenient-media2.png','common-responsive-img','inconvenient-media2')?>
						</div>
					</div>
					<p class="common-bold-black">高額な治療費にもかかわらず、このように成功率が低い不妊治療や、治験や科学的根拠のないあらゆる「妊活」しか選択肢が無いとすれば、日本の不妊治療は、たくさんの選択肢があるように見えて、その実、選択の余地が無いに等しいと言えるのではないでしょうか。</p>
				</div>
            </div>
            <div class="row column-under-text-1">
				<div class="col-md-12">
					<div class="column-bottom-before-title"><a href="#i9">顕微授精で高まる自閉症スペクトラム障害リスク</a>
					</div>
					<div class="media">
						<?= image('img/inconvenient-media3.png','common-responsive-img','col-under-left-2')?>
						<div class="media-body common-media-body">
							<p>さらに、2015年3月「American Journal of Public Health」に掲載された論文では、<span class="common-bold-black">顕微授精に代表される生殖補助医療<span class="yellow-bg">によって生まれた子は、そうでない子に比べ、</span>自閉症スペクトラム障害であるリスクが2倍であるという研究結果が発表されています。</span></p>	
                            <p>これは、アメリカ疾病対策予防センターの統計（カリフォルニア州で1997～2007年に生まれた590万例の小児の調査）を元に分析されたものです。</p>
                        </div>
                    </div>
                    <p class="outside-text-block margin-leftcol">また、米医学誌『JAM』（2013年7月3日）に掲載された論文でも、スウェーデンで出生した250万人を10年間追跡調査した結果、<span class="common-bold-black">顕微授精は体外受精に比べて、自閉症や知的障害リスクが高まることが報告されています。</span></p>
                </div>
            </div>
            <div class="row column-under-text-1">
				<div class="col-md-12">
					<div class="column-bottom-before-title"><a href="#i10">不妊治療リスクと安全性</a></div>
					<div class="media arrangeorder">
						<div class="col-under-media-text-container two">
							<p>しかし、<span class="common-bg-yellowred">日本では、これから不妊治療を受ける患者に、こうしたリスクが説明される事は、ほとんどありません。</span></p>
							
							<p>そもそも日本では、説明を義務付けるような不妊治療のガイドラインが、まだ整備されていないのです(2019年2月現在)。</p>
							
						</div>
						<div class="media-body one">
							<?= image('img/inconvenient-media4.png','common-responsive-img','col-under-right3')?>
						</div>
					</div>
					<p><br>不妊治療や巷の「妊活」には、様々なアプローチ法が存在していますが、私は、不妊治療は<span class="common-bg-yellowred">「妊娠率(分娩率)」</span>と<span class="common-bg-yellowred">「安全性」</span>で選ぶべきだと考えております。</p>
					<p class="common-bg-yellowred">だからこそ、外科的な処置が一切伴わない、妊娠率89%を達成した英国式療法をお勧めしております。</p>

					<p class="janina column-after-red-text">※
					<span class="column-after-red-text">
						
						この治療法は、患者様の症状に合わせて最適な処方が異なります。<br>もしご自身に合った治療法をお知りになりたい場合は、下記の無料相談(問診)を通して<br>お問い合せ下さい。</span></p>
	　			</div>
            </div>
            <div class="dark-red-button red-button-common">
				<a href="http://projects.cloudproduction.co.bd/tmclinic/consultation" type="button" class="btn btn-lg btn-red-custom waves-effect waves-light">不妊治療無料相談はこちら</a>
			</div>
			<div class="ceo-bottom-container">
				<div class="line-wrapper">
					<h3 class="column-under-list-title blue-text-common">監修</h3>
				</div>
				<div class="colunder-bottommedia-container">
					<div class="media">
						<div class="col-under-media-text-container">
							<?= image('img/ceo1.png','','ceo')?>
						</div>
						<div class="media-body">
							<p class="blue-text-common ceo-right-text">外来部長 <span class="pt-up-font">山下明子先生</span><span>医学博士、内科医、神経内科専門医、抗加齢医学専門医</span></p>
						</div>
					</div>
					<div class="media">
						<div class="col-under-media-text-container">
							<?= image('img/ceo2.png','','ceo2')?>
						</div>
						<div class="media-body">
							<p class="blue-text-common ceo-right-text">顧問医師 <span class="pt-up-font">身原正哉先生</span><span>産婦人科専門医・日本抗加齢医学会専門医</span></p>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-3 colunder-right-section">
			<?php view('common/ceo') ?>
		</div>
	</div>
</div>