<div class="container common-pages-container">
	<div class="row top-about-bg-container">
		<div class="col-md-12 common-bg-wrapper">
			<div class="about-bg-content">
				<h1 align="center">Q & A</h1>
				<h4 align="center">Q & A</h4>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="content-container col-md-10 offset-md-1">
			<ol class="breadcrumb">
				<li class="breadcrumb-item"><a href="#">TOP  <i class="fas fa-angle-right breadcumb-icon"></i> </a></li>
				<li class="breadcrumb-item active">Q&A</li>
			</ol>
		</div>
	</div>
	<div class="row">
		<div class="col-md-10 offset-md-1">
			<h4 class="faq-title" align="center">よくある質問をまとめました。お問い合わせの前にご確認ください。</h4>
			<div class="panel-group panel-faq" id="accordion" role="tablist" aria-multiselectable="true">
				<div class="panel panel-default">
					<div class="panel-heading" role="tab" id="headingOne">
						<h4 class="panel-title">
							<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
								<!-- <i class="more-less glyphicon glyphicon-plus"></i> -->
								<i class="more-less fas fa-angle-down"></i>
								<span class="question-color">Q 1.</span>「Ω-folic（オメガ-フォリック）」とは何ですか？

							</a>
						</h4>
					</div>
					
					<div id="collapseOne" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
						<div class="panel-body">
							A.　英国の不妊治療で使用されている不妊治療に特化したサプリメントです。<br>
							かセイヨウニンジンボク（女性用のみ）、アルギニン、カルチニン（男性用のみ）が妊娠しやすい身体への体質改善をサポートします。
						</div>
					</div>
				</div>
				<div class="panel panel-default">
					<div class="panel-heading" role="tab" id="headingTwo">
						<h4 class="panel-title">
							<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
							<i class="more-less fas fa-angle-down"></i>
							<span class="question-color">Q 2．</span>どんなタイミングで飲めばいいですか？
							</a>
						</h4>
					</div>
					<div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
						<div class="panel-body">
							A.　サプリメントなのでお好きなタイミングでOKです。
						</div>
					</div>
				</div>
				<div class="panel panel-default">
					<div class="panel-heading" role="tab" id="headingThree">
						<h4 class="panel-title">
							<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
							<i class="more-less fas fa-angle-down"></i>
							<span class="question-color">Q 3.</span> 毎日飲み続けないと効果はなくなっていきますか？
							</a>
						</h4>
					</div>
					<div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
						<div class="panel-body">
							A.　体質改善を目的としたサプリメントですので、毎日一定量をお飲みいただくことで、効果を発揮します。
						</div>
					</div>
				</div>
				<div class="panel panel-default">
					<div class="panel-heading" role="tab" id="headingFour">
						<h4 class="panel-title">
							<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
							<i class="more-less fas fa-angle-down"></i>
							<span class="question-color">Q 4.</span> 食物アレルギーに反応する成分や原料は入っていますか？
							</a>
						</h4>
					</div>
					<div id="collapseFour" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFour">
						<div class="panel-body">
							A.　回答するにあたって下記の成分含んでなければOKです
							<div class="red-text-common">大豆を含みます</div>
						</div>
					</div>
				</div>
				<div class="panel panel-default">
					<div class="panel-heading" role="tab" id="headingFive">
						<h4 class="panel-title">
							<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
							<i class="more-less fas fa-angle-down"></i>
							<span class="question-color">Q 5.</span> 他のサプリや薬との併用は可能でしょうか？
							</a>
						</h4>
					</div>
					<div id="collapseFive" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFive">
						<div class="panel-body">
							A. 「Ωフォリック」はサプリメントです。<br>
							食品成分となりますので、お薬を飲まれている方にも安心してご飲用いただけます。
						</div>
					</div>
				</div>
				<div class="panel panel-default">
					<div class="panel-heading" role="tab" id="headingSix">
						<h4 class="panel-title">
							<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseSix" aria-expanded="false" aria-controls="collapseSix">
							<i class="more-less fas fa-angle-down"></i>
							<span class="question-color">Q 6.</span> 夫婦共に飲んだほうが良いですか？
							</a>
						</h4>
					</div>
					<div id="collapseSix" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingSix">
						<div class="panel-body">
							A.　体質改善をして、身体の状態を自然妊娠しやすい状態にもっていくことを目的としていま<br>
							すのでご夫婦一緒にお召し上がりいただくことをおすすめしております。<br>
							<a class="underline-faq" href="consultation">まずはこちらの「無料相談」からご相談ください</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>