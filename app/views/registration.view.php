<div class="container">
	<div class="row">
		<div class="col-md-12">
			<br>
			<br>
			<br>
		</div>
	</div>
</div>
<div class="container">
	<div class="row">
		<?php view('partial/message') ?>
		<div class="col-md-10">
			<div class="panel-body">
				<form name="contact-form" role="form" id="inputForm" action="<?= url('registration') ?>" method="POST">
					<div class="row setup-content justify-content-md-center" id="step-1">
						<div class="col-7 col-lg-7 form-content-cover">
							<div class="panel-heading">
								<h3 class="panel-title">Registration Form</h3>
							</div>
							<div class="row">
								<div class="col-md-12">
									<div class="md-form <?= hasError('username')? "has-danger":''?>">
										<i class="fas fa-envelope-square <?= hasError('username')? "danger-icon":'text-default'?> prefix"></i>
										<input type="text" id="username" name="username" value="<?= old('username') ?>" class="form-control">
										<label for="username">username Address</label>
									</div>
									<div class="form-control-feedback feedback" id="fed_username"><?= showError('username') ?></div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-12">
									<div class="md-form <?= hasError('email')? "has-danger":''?>">
										<i class="fas fa-blender-email <?= hasError('email')? "danger-icon":'text-default'?> prefix"></i>
										<input type="text" id="email" name="email" value="<?= old('email') ?>" class="form-control">
										<label for="email">email</label>
									</div>
									<div class="form-control-feedback feedback" id="fed_email"><?= showError('email') ?></div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-12">
									<div class="md-form <?= hasError('password')? "has-danger":''?>">
										<i class="fas fa-blender-password <?= hasError('password')? "danger-icon":'text-default'?> prefix"></i>
										<input type="text" id="password" name="password" value="<?= old('password') ?>" class="form-control">
										<label for="password">password</label>
									</div>
									<div class="form-control-feedback feedback" id="fed_password"><?= showError('password') ?></div>
								</div>
							</div>
							<div class="text-center text-md-center button-wrapper">
								<button class="btn btn-default pull-right" type="submit">  <i class="fas fa-share-square"></i>register </button>
							</div>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>