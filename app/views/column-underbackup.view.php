<div class="container">
	<div class="row top-about-bg-container">
		<div class="col-md-12 bg-pink">
			<div class="column-bg-title-wrapper">
				<h2 align="center">COLUMN</h2>
				<h4 align="center">と料金</h4>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-10 offset-md-1">
			<div class="content-container">
				<ol class="breadcrumb">
					<li class="breadcrumb-item"><a href="#">TOP  <i class="fas fa-angle-right breadcumb-icon"></i> </a></li>
					<li class="breadcrumb-item">コラム <i class="fas fa-angle-right breadcumb-icon"></i></li>
					<li class="breadcrumb-item active">英国式栄養療法とは</li>
				</ol>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-10 offset-md-1">
			<?= image('img/column-under-bg-2.png','common-img column-single-img-common','column-img-1')?>
			<h3 class="column-under-bg2-text">日本初上陸にして、<br>
				日本唯一の妊娠率89%英国式療法とは
			</h3>
		</div>
	</div>
	<div class="row">
		<div class="col-md-10 offset-md-1">
			<div class="column-under-top-list-wrapper">
				<h3 class="column-under-list-title">目次</h3>
				<ul class="column-bottom-before-list">
					<li >日本のクリニックで行われている不妊治療</li>
					<li >世界で1番妊娠成功率の低い日本の不妊治療</li>
					<li >妊娠率は、健康な赤ちゃんの誕生率ではありません。</li>
					<li >89％という驚異の妊娠率を達成したイギリスの不妊治療</li>
					<li >外科的手術なし。妊娠率89％の秘密はサプリメントを使用した治療法</li>
				</ul>
			</div>
		</div>
	</div>
	<div class="row column-under-middle1-wrapper">
		<div class="col-md-10 offset-md-1">
			<div class="column-bottom-before-title">日本のクリニックで行われている不妊治療と世界で最も低い日本の妊娠率
			</div>
			<div class="row">
				<div class="col-md-8">
					<div class="column-under-middle1">
						<h5>日本のクリニックでは、主に</h5>
						<ul class="column-under-middle1-list">
							<li>
								<div class="contents-list-order">1</div>
								タイミング法
							</li>
							<li>
								<div class="contents-list-order">2</div>
								人工授精
							</li>
							<li>
								<div class="contents-list-order">3</div>
								体外受精
							</li>
							<li>
								<div class="contents-list-order">4</div>
								顕微授精
							</li>
						</ul>
						<h5>上記4種の不妊治療が行われています。</h5>
					</div>
					<h6 class="column-middle-bottom-text">また、クリニック以外では、鍼灸や整体、漢方、食事療法など、あらゆる不妊治療が存在しています。</h6>
				</div>
				<div class="col-md-4">
					<?= image('img/column-under-right.png','common-img','column-under-right')?>
				</div>
			</div>
			<h6>しかし、クリニック以外で行われている治療法は、大抵科学的根拠や臨床試験・論文などの裏付けが無いものばかりです。</h6>
		</div>
	</div>
	<div class="row column-under-text-1">
		<div class="col-md-10 offset-md-1">
			<div class="column-bottom-before-title">世界で1番妊娠成功率の低い日本の不妊治療
			</div>
			<p>クリニックで行われている主な治療法に関しては、まだ一定の根拠・実績があると言えますが、<span class="red-text-common">
				実は日本は、世界で1番体外受精が行われているにもかかわらず、世界で1番妊娠成功率が低い</span>のです<br>
				つまり、現在日本で「この不妊治療が良い」と提唱されている治療法も、それはあくまでも国内の話であり、
				海外も視野に入れると、決して成績が良いとは言えない手法ばかりなのです。
			</p>
			<p>一説には、日本の不妊治療成績が世界一悪いのは、ただ「冷え」を改めるだけの不妊療法に固執したり、イギリスでは既に禁止されている、効率の悪い方法<br>
				で体外受精を行っている為とも言われております。<br>
				さらに、クリニックが提示している「妊娠率」の多くも、「健康な出生率」とは一致しません。
			</p>
			<p>たとえその後、<span class="red-text-common">流産してしまっても、子供が健康な状態で生まれなくても、
				妊娠判定で「陽性」が出れば、すなわち「妊娠」とされています。</span>
			</p>
			<p>ただでさえ、日本の不妊治療で妊娠する確率は低いのですが、妊娠するだけでなく、健康な子供が誕生するまでの数値に直すと、さらに低い成功率が浮かび上がってきます。</p>
		</div>
	</div>
	<div class="row column-under-text-1">
		<div class="col-md-10 offset-md-1">
			<div class="column-bottom-before-title">妊娠率は、健康な赤ちゃんの誕生率ではありません。
			</div>
			<p>みなさんが目にしている「妊娠率」は、決して健康な赤ちゃんの誕生率を示しているわけではありません。<br>
				子供を授かれない事に悩んでいると、どうしても、妊娠する事にばかり意識をとらわれがちです
			</p>
			<p>しかし、妊娠さえできれば、分娩に至らなくて良いなんて、子供を望むカップルの、誰が思うでしょうか？</p>
			<p>妊娠判定で「陽性」が出れば、臨月まで妊娠を継続し、母子ともに健康な出産を望んで当然です<br>
				ところが、採用される不妊療法によっては、障害のある子供の出生率が高まる事が研究の結果、明らかにされていても、それを事前に患者へ伝える不妊クリ<br>
				ニックすら少ないのが実状です。<br>
				※詳しくは　コラム２章「体外受精でも妊娠率20～35%　日本の不妊治療の不都合な真実」をご覧下さい)
			</p>
		</div>
	</div>
	<div class="row column-under-text-1">
		<div class="col-md-10 offset-md-1">
			<div class="column-bottom-before-title">89%という驚異の妊娠率を達成したイギリスの不妊治療とは
			</div>
			<p>一方、イギリスには、妊娠継続期間や子供の出産時体重、集中治療室移送への有無などまで、しっかり追いかけた上で、妊娠率89%を達成した不妊療法が<br>
				存在しています。<br>※これは、SURREY大学の観察研究による論文が発表されている治療法で、不妊クリニックが自己申告している「妊娠率」とは異なります
			</p>
			<p>この不妊療法の臨床試験は、367カップルを対象に、1990～1992年にかけて行われました。<br>(出典：PRECONCEPTUALCARE AND PREGNANCY OUTCOME，)</p>
			<div class="column-under-middle2-list-wrapper">
				<h5>カップルの平均年齢は、女性34歳・男性36歳で、対象となった367カップルは、下記のような経験を持つ方々でした。</h5>
				<ul class="column-under-middle1-list">
					<li>
						<div class="contents-list-order">1</div>
						217（59％）が妊娠トラブル経験
					</li>
					<li>
						<div class="contents-list-order">2</div>
						154(42%)の男性が精子検査を経験
					</li>
					<li>
						<div class="contents-list-order">3</div>
						136（37％）が1年－10年の不妊経験
					</li>
					<li>
						<div class="contents-list-order">4</div>
						139（38％）が1回－5回の流産経験
					</li>
					<li>
						<div class="contents-list-order">5</div>
						11（3％）が死産経験
					</li>
					<li>
						<div class="contents-list-order">6</div>
						40（11％）が早産経験
					</li>
					<li>
						<div class="contents-list-order">7</div>
						15（4％）が軽体重児出産（2500g未満
					</li>
					<li>
						<div class="contents-list-order">8</div>
						7が奇形児
					</li>
					<li>
						<div class="contents-list-order">9</div>
						3が突然死
					</li>
				</ul>
				<h5>(飲酒者→男性：90%、女性：60%、喫煙者→男性：45％、女性：51％)</h5>
			</div>
			<h6>こうした数値を見ても、流産・早産・死産・奇形児・突然死など、妊娠後にも様々なリスクが待ち受けている事が分かります。</h6>
			<h6 class="column-under-middle2-text"><span class="red-text-common">しかし、この英国式療法では、上記対象者367カップル中327人(89%)が妊娠し、そのうち、なんと100%が奇形児無し・集中治療室移送なしで、無事出産を完了した事が報告されています。</span><br>
				※平均38.5wで出産、出産時体重は平均3265g
			</h6>
		</div>
	</div>
	<div class="row column-under-text-1">
		<div class="col-md-10 offset-md-1">
			<div class="column-bottom-before-title">外科的手術なし。妊娠率89％の秘密はサプリメントを使用した治療法
			</div>
			<ul class="column-under-bottom-list">
			<li>多胎妊娠は1例もなく、生まれてきた子供327人は、</li>
			<li>男の子：137人</li>
			<li>女の子：190人</li>
			<li>と、出生時の男女比にも大きな偏りがありませんでした</li>
		</div>
	</div>
</div>