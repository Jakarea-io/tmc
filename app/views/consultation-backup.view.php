<section class="top-section">
    <div class="container">
       <div class="row top-about-bg-container">
    		<div class="col-md-12 common-bg-wrapper">
    			<div class="about-bg-content">
    				<h2 align="center">CONSULTATION</h2>
    				<h4 align="center">不妊治療無料相談問</h4>
    			</div>
    		</div>
	   </div>
	
	
                
        
        <div class="row">
            <div class="content-container col-md-10 offset-md-1">
                <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="#">TOP <i class="fas fa-angle-right breadcumb-icon"></i></a></li>
                <li class="breadcrumb-item active">不妊治療無料相談</li>
                </ol>
                <p class="text-center">山下先生監修 不妊治療無料診断。<br> 下記の質問に答えていただくと、山下先生があなたの不妊治療に関するアドバイスをメールでお送りします。 <br>
                    不妊治療サプリ「Ωフォリック」をご購入ご希望の方もこちらの問診票への回答が必要です。 </p>
                    
            </div>
        </div>
        <div class="row question-ans-container">
            <div class="col-md-10 offset-md-1 question-ans-section">
                <form method='post' action="<?= url('consultation/post') ?>">
                    <div class="question1">
                        <h2 class="d-inline-block">Q1.</h2>
                        <p class="d-inline">現在不妊治療をされていますか？</p>
                        <span>※必須</span>
                        <div class="question1-radio">
                            <div class="form-check-inline">
                                <label class="form-check-label">
                                <input type="radio" class="form-check-input" value='いいえ' name="question_1" <?= (old('question_1') == 'いいえ') ? 'checked':''?>>いいえ
                                </label>
                            </div>
                           
                            <div class="form-check-inline">
                                <label class="form-check-label">
                                <input type="radio" class="form-check-input" value='はい' name="question_1" <?= (old('question_1') == 'はい') ? 'checked':''?>>はい
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="question2">
                        <h2 class="d-inline-block">Q2.</h2>
                        <p class="d-inline">他施設で不妊治療を受けたことがありますか？</p>
                        <span>※必須</span>
                        <div class="question1-radio">
                            <div class="form-check-inline">
                                <label class="form-check-label">
                                <input type="radio" class="form-check-input" value='いいえ'  name="question_2">いいえ
                                </label>
                            </div>
                            <div class="form-check-inline">
                                <label class="form-check-label">
                                <input type="radio" class="form-check-input" value='はい' name="question_2">はい
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="question3">
                        <h2 class="d-inline-block">Q.3</h2>
                        <p class="d-inline">現在何か治療をされていますか？</p>
                        <div class="text-instr">過去に治療したもの含めてすべてチェックしてください。<span>※必須</span></div>
                        <div class="checkbox-wrapper">
                            <div class="form-check-inline">
                                <label class="form-check-label">
                                <input type="checkbox" class="form-check-input" value='ない' name="question_3[]" >ない
                                </label>
                            </div>
                            <div class="form-check-inline">
                                <label class="form-check-label">
                                <input type="checkbox" class="form-check-input" value='タイミング法' name="question_3[]" >タイミング法
                                </label>
                            </div>
                            <div class="form-check-inline">
                                <label class="form-check-label">
                                <input type="checkbox" class="form-check-input" value='は排卵誘発法い' name="question_3[]" >排卵誘発法
                                </label>
                            </div>
                            <div class="form-check-inline">
                                <label class="form-check-label">
                                <input type="checkbox" class="form-check-input" value='人口受精' name="question_3[]" >人口受精
                                </label>
                            </div>
                            <div class="form-check-inline">
                                <label class="form-check-label">
                                <input type="checkbox" class="form-check-input" value='体外受精' name="question_3[]" >体外受精
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="question4">
                        <h2 class="d-inline-block">Q.4</h2>
                        <p class="d-inline">現在お使いになっている薬またはサプリはありますか？</p>
                        <span>※必須</span>
                        <div class="question1-radio">
                            <div class="form-check-inline">
                                <label class="form-check-label">
                                <input type="radio" class="form-check-input" value='いいえ' name="question_4">いいえ
                                </label>
                            </div>
                            <div class="form-check-inline">
                                <label class="form-check-label">
                                <input type="radio" class="form-check-input" value='はい' checked name="question_4">はい
                                </label>
                            </div>
                        </div>
                        <div class="form-group">
                            <textarea class="form-control"  rows="3" name="question_4_1" placeholder="「はい」の方はこちらにこサプリメントの名称を記入ください"><?= old('question_4_1') ?></textarea>
                        </div>
                    </div>
                    <div class="question5">
                        <h2 class="d-inline-block">Q.5</h2>
                        <p class="d-inline">性別をお答えください</p>
                        <span>※必須</span>
                        <div class="question1-radio" id="radio-wrapper">
                            <div class="form-check-inline">
                                <label class="form-check-label">
                                <input type="radio" class="form-check-input" id="male" value='男性' name="question_5">男性
                                </label>
                            </div>
                            <div class="form-check-inline">
                                <label class="form-check-label">
                                <input type="radio" class="form-check-input" id="female" value='女性' name="question_5">女性
                                </label>
                            </div>
                        </div>
                        
                            <div class="question5-inner" id="hide-female">
                                <p class="d-inline">子宮内膜症の検査をしたことがありますか？</p>
                                <span>※必須</span>
                                <div class="question2-radio">
                                    <div class="form-check-inline">
                                        <label class="form-check-label">
                                        <input type="radio" class="form-check-input" value='いいえ' name="question_5_1">いいえ
                                        </label>
                                    </div>
                                    <div class="form-check-inline">
                                        <label class="form-check-label">
                                        <input type="radio" class="form-check-input" value='はい' name="question_5_1">はい
                                        </label>
                                    </div>
                                </div>
                            </div>
                            
                            <div id="hide-male">
                                <div class="question5-sub-inner">
                                    <p class="d-inline">精液検査をしたことがありますか？</p>
                                    <span>※必須</span>
                                    <div class="question3-radio">
                                        <div class="form-check-inline">
                                            <label class="form-check-label">
                                            <input type="radio" class="form-check-input" value='問題なし' name="question_5_2">問題なし
                                            </label>
                                        </div>
                                        <div class="form-check-inline">
                                            <label class="form-check-label">
                                            <input type="radio" class="form-check-input" value='精子数が少ない' name="question_5_2">精子数が少ない
                                            </label>
                                        </div>
                                        <div class="form-check-inline">
                                            <label class="form-check-label">
                                            <input type="radio" class="form-check-input" value='精子の動きが弱い' name="question_5_2">精子の動きが弱い
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <!--<div class="form-group">-->
                                <!--    <textarea class="form-control" id="exampleFormControlTextarea2" rows="3"  name="question_5_3" placeholder=""></textarea>-->
                                <!--    /*「はい」の方はこちらにこサプリメントの名称を記入ください */-->
                                <!--</div>-->
                        </div>
                    </div>
                    <div class="question6">
                        <h2 class="d-inline-block">Q.6</h2>
                        <p class="d-inline">ご質問などあればご自由にお書きください</p>
                        <div class="form-group">
                            <textarea class="form-control"  name="question_6" rows="3" placeholder=""></textarea>
                            <!--「はい」の方はこちらにこサプリメントの名称を記入ください-->
                        </div>
                    </div>
                    <button type="submit" class="btn btn-custom-cons text-center">次へ</button>
                </form>
            </div>
        </div>
    </div>
</section>