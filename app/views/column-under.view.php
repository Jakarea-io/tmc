<div class="container columnunder_container">
	<div class="row">
		<div class="col-md-12">
			<div class="content-container">
				<ol class="breadcrumb">
					<li class="breadcrumb-item"><a href="#">TOP  <i class="fas fa-angle-right breadcumb-icon"></i> </a></li>
					<li class="breadcrumb-item">コラム <i class="fas fa-angle-right breadcumb-icon"></i></li>
					<li class="breadcrumb-item active"> 日本初上陸の妊娠率89％の英国式療法</li>
				</ol>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<div class="col-wrapper">
				<div class="col-under-firstone">
					<?= image('img/col-under-top.png','common-img colunder-top1-web','col-under-top')?>
					<?= image('img/colunder-top-mobile1.png','common-img colunder-top-mobile1','colunder-top-mobile1')?>
					<div class="colunder-top1-contentwrapper">
						<div class="col-under-top1-wrapper">
							<?= image('img/col-under-top1-arrow.png','','col-under-top1-arrow')?>
							<h5 class="colunder-arrow-text">目からウロコの</h5>
							
						</div>
						<h3>不妊治療<span class="col-top1-span">なるほどコラム</span></h3>
					</div>
				</div>
				<div class="col-under-secondone">
					<?= image('img/col-under-top2.png','common-img colunder-top-mobile2web','col-under-top2')?>
					<?= image('img/colunder-top-mobile2.png','common-img colunder-top-mobile2mobile','colunder-top-mobile2')?>
					
					<div class="media colunder-common-media">
						<div class="col-common-pink-circle">Column</div>
						<div class="media-body">
							<div class="common-mid-text-banner">
								<h4>日本初上陸の妊娠率89%の英国式療法</h4>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-9 col-sm-12">
			<div class="column-under-top-list-wrapper">
				<div class="line-wrapper">
					<h3 class="column-under-list-title blue-text-common">目次</h3>
				</div>
				<ul class="columnunder-bottom-before-list">
					<li><a href="#nfertility-treatment">日本のクリニックで行われている不妊治療</a></li>
					<li><a href="#japan-infertility-treatment">世界で1番妊娠成功率の低い日本の不妊治療</a></li>
					<li><a href="#pregnancy-rate-is-not">妊娠率は、健康な赤ちゃんの誕生率ではありません</a></li>
					<li><a href="#infertility-treatment-in-the-united-kingdom">89％という驚異の妊娠率を達成したイギリスの不妊治療</a></li>
					<li><a href="#no-surgical-operation">外科的手術なし。妊娠率89％の秘密はサプリメントを使用した治療法</a></li>
				</ul>
			</div>
			<div class="column-bottom-before-title" id="nfertility-treatment">
				<a href="#">日本のクリニックで行われている不妊治療と世界で最も低い<br>日本の妊娠率</a>
			</div>
			<div class="row">
				<div class="col-md-6 col-under-left-box-ct col-md-order-2">
					<h5 class="common-heading5">日本のクリニックでは、主に</h5>
					<div class="column-under-middle1 col-under-middle-ct">
						<ul class="column-under-middle1-list colunder-unorder-wrapper">
							<li>
								<div class="contents-list-order colunder-order">1</div>
								タイミング法
							</li>
							<li>
								<div class="contents-list-order colunder-order">2</div>
								人工授精
							</li>
							<li>
								<div class="contents-list-order colunder-order">3</div>
								体外受精
							</li>
							<li>
								<div class="contents-list-order colunder-order">4</div>
								顕微授精
							</li>
						</ul>
					</div>
					<div class="common-content-wrapper">
						<h5>上記4種の不妊治療が行われています。</h5>
						<h6 class="column-middle-bottom-text">また、クリニック以外では、鍼灸や整体、漢方、食事療法など、あらゆる不妊治療が存在しています。</h6>
					</div>
				</div>
				<div class="col-md-6 top-first-right-img col-md-order-1">
					<?= image('img/column-under-right.png','common-img col-under-top-mid-mg','column-under-right')?>
				</div>
				<h6 class="outside-text-block margin-leftcol col-md-order-3">しかし、<span class="common-bg-yellowred">クリニック以外で行われている治療法は、大抵科学的根拠や臨床試験・論文などの裏付けが無いものばかりです。</span></h6>
			</div>
			<div class="row column-under-text-1" id="japan-infertility-treatment">
				<div class="col-md-12">
					<div class="column-bottom-before-title"><a href="">世界で1番妊娠成功率の低い日本の不妊治療</a></div>
					<p>クリニックで行われている主な治療法に関しては、まだ一定の根拠・実績があると言えますが、<span class="common-bg-yellowred">
					実は日本は、世界で1番体外受精が行われているにもかかわらず、世界で1番妊娠成功率が低い</span>のです。</p>
					<p>つまり、現在日本で「この不妊治療が良い」と提唱されている治療法も、それはあくまでも国内の話であり、 海外 も視野に入れると、決して成績が良いとは言えない手法ばかりなのです。</p>
					<div class="media">
						<?= image('img/col-under-left1.png','','col-under-left1')?>
						<div class="media-body common-media-body">
							<p>一説には、日本の不妊治療成績が世界一悪いのは、ただ「冷え」を改めるだけの不妊療法に固執したり、イギリスでは既に禁止されている、効率の悪い方法で体外受精を行っている為とも言われております。</p>
							<p>さらに、クリニックが提示している「妊娠率」の多くも、「健康な出生率」とは一致しません。</p>
							<p>たとえその後、<span class="common-bg-yellowred">流産してしまっても、子供が健康な状態で生まれなくても、 妊娠判定で「陽性」が出れば、すなわち「妊娠」とされています。</span>.</p>
						</div>
					</div>
					<h6 class="outside-text-block">ただでさえ、日本の不妊治療で妊娠する確率は低いのですが、妊娠するだけでなく、健康な子供が誕生するまでの数値に直すと、さらに低い成功率が浮かび上がってきます。</h6>
				</div>
			</div>
			<div class="row column-under-text-1" id="pregnancy-rate-is-not">
				<div class="col-md-12">
					<div class="column-bottom-before-title"><a href="">妊娠率は、健康な赤ちゃんの誕生率ではありません。</a>
					</div>
					<p>みなさんが目にしている「妊娠率」は、決して健康な赤ちゃんの誕生率を示しているわけではありません。
						子供を授かれない事に悩んでいると、どうしても、妊娠する事にばかり意識をとらわれがちです。
					</p>
					<div class="media">
						<div class="col-under-media-text-container one">
							<p>しかし、<span class="common-bold-black">妊娠さえできれば、分娩に至らなくて良いなんて、子供を望むカップルの、誰が思うでしょうか？</span>
							</p>
							<p>妊娠判定で「陽性」が出れば、臨月まで妊娠を継続し、母子ともに健康な出産を望んで当然です。</p>
							<p>ところが、<span class="common-bg-yellowred">採用される不妊療法によっては、障害のある子供の出生率が高まる事が研究の結果、明らかにされていても、それを事前に患者へ伝える不妊クリニックすら少ないのが実状</span>です。</p>
						</div>
						<div class="media-body">
							<?= image('img/col-under-right2.png','','col-under-right2')?>
						</div>
					</div>
					<p>※詳しくは　<a href="inconvenient-truth" class="col-under-right2-anchor">コラム２章「体外受精でも妊娠率20～35%　日本の不妊治療の不都合な真実」</a>をご覧下さい</p>
				</div>
			</div>
			<div class="row column-under-text-1" id="infertility-treatment-in-the-united-kingdom">
				<div class="col-md-12">
					<div class="column-bottom-before-title"><a href="">89%という驚異の妊娠率を達成したイギリスの不妊治療とは</a>
					</div>
					<div class="media">
						<?= image('img/col-under-left-2.png','','col-under-left-2')?>
						<div class="media-body common-media-body">
							<p>一方、<span class="common-bg-yellowred">イギリスには、妊娠継続期間や子供の出産時体重、集中治療室移送への有無などまで、しっかり追いかけた上で、妊娠率89%を達成した不妊療法が存在しています。</span></p>
							<p>※これは、SURREY大学の観察研究による論文が発表されている治療法で、不妊クリニックが自己申告している「妊娠率」とは異なります</p>	
						</div>
					</div>
					<div class="common-content-wrapper">
						<p>この不妊療法の臨床試験は、367カップルを対象に、1990～1992年にかけて行われました。
						(出典：PRECONCEPTUALCARE AND PREGNANCY OUTCOME，)</p>
						<p>カップルの平均年齢は、女性34歳・男性36歳で、対象となった367カップルは、下記のような経験を持つ方々でした。</p>
					</div>
						<div class="column-under-middle2-list-wrapper">
						<ul class="column-under-middle1-list unordercol-list">
							<li>
								<div class="contents-list-order1">[ 1 ]</div>
								217（59％）が妊娠トラブル経験
							</li>
							<li>
								<div class="contents-list-order1">[ 2 ]</div>
								154(42%)の男性が精子検査を経験
							</li>
							<li>
								<div class="contents-list-order1">[ 3 ]</div>
								136（37％）が1年－10年の不妊経験
							</li>
							<li>
								<div class="contents-list-order1">[ 4 ]</div>
								139（38％）が1回－5回の流産経験
							</li>
							<li>
								<div class="contents-list-order1">[ 5 ]</div>
								11（3％）が死産経験
							</li>
							<li>
								<div class="contents-list-order1">[ 6 ]</div>
								40（11％）が早産経験
							</li>
							<li>
								<div class="contents-list-order1">[ 7 ]</div>
								15（4％）が軽体重児出産（2,500g未満)
							</li>
							<li>
								<div class="contents-list-order1">[ 8 ]</div>
								7が奇形児
							</li>
							<li>
								<div class="contents-list-order1">[ 9 ]</div>
								3が突然死
							</li>
							<h6 class="unordercol-list-heading5">(飲酒者→男性：90%、女性：60%、喫煙者→男性：45％、女性：51％)</h6>
						</ul>
					</div>
					<p>こうした数値を見ても、流産・早産・死産・奇形児・突然死など、妊娠後にも様々なリスクが待ち受けている事が分かります。</p>
					<p class="column-under-middle2-text">しかし、<span class="common-bg-yellowred">この英国式療法では、上記対象者367カップル中327人(89%)が妊娠し、そのうち、なんと100%が奇形児無し・集中治療室移送なしで、無事出産を完了した事が報告されています。
					</span><br>※平均38.5wで出産、出産時体重は平均3265g</p>
					<h6></h6>
					</h6>
				</div>
			</div>
			<div class="row column-under-text-1" id="no-surgical-operation">
				<div class="col-md-12">
					<div class="column-bottom-before-title"><a href="">外科的手術なし。<br>妊娠率89％の秘密はサプリメントを使用した治療法。</a>
					</div>
					<div class="media mobile-order">
						<div class="media-body">
							<?= image('img/col-under-right3.png','','col-under-right3')?>
						</div>
						<div class="col-under-media-text-container">
							<p>多胎妊娠は1例もなく、生まれてきた子供327人は、</p>
							<div class="bol-under-isto">
							男の子 : 137人<span>女の子 : 190人</span>
							</div>
							<p>と、出生時の男女比にも大きな偏りがありませんでした。</p>
							<p>日本の不妊治療と比較しますと、驚異的な妊娠率(分娩率)ですが、この英国式療法は、患者の症状に合わせた医師の指示に沿って、夫婦共にサプリメントを服用する治療法がメインで、外科的な処置は伴いません。</p>	
						</div>
						
					</div>
					<p>ただ、<span class="common-bg-yellowred">特徴として、医師指定のサプリ服用開始から3ヶ月は、避妊するよう指導されています。
						理由は、ゴールを「妊娠判定陽性」ではなく、あくまでも「母子ともに健康な出産」に設定している事から、流産・早産・死産などを回避する準備期間を導入している為です。</span></p>
					<p>まずは<span class="common-bg-yellowred">3ヶ月、避妊しながらサプリを服用し、それから6ヶ月、サプリの服用を継続しながら性交を再開するプログラムになっています。</span></p>
					<div class="column-under-middle2-list-wrapper colunder-middle2-container">
						<h5 class="dark-blue-text">英国式療法に取り組んだ、30代カップルのケース</h5>
						<h6 class="dark-blue-text">[スーザン30才・パートナー31才]</h6>
						<p>4年間妊活に取り組むも、原因不明の不妊症と診断され、4回IUI(人工授精)にもトライするが、一度も妊娠せず</p>
						<div class="box arrow-bottom">スーザンの生理は周期的だったが、過出血、不正出血、または生理前に頭痛あり<br>生理時は腹部膨満、吐き気</div>
						<div class="box arrow-bottom">英国式療法に取り組み、3ヶ月<br>医師指定のサプリを服用し、体内が妊娠に適した環境に至るのを待つ</div>
						<div class="box arrow-bottom">初回来院から9ヶ月後に妊娠、9ヶ月後に出産</div>
					</div>
					<div class="media">
						<div class="imgs one">
							<?= image('img/col-under-left4.png','','col-under-left4')?>
						</div>
						<div class="media-body common-media-body">
							<p class="common-bold-black">長年不妊治療に取り組んできたカップルにとって、従来のサプリメントによる治療法は「気休め」程度のイメージしか無いかもしれませんが、このような臨床試験や論文で裏付けられた不妊治療サプリは、これまで日本にはありませんでした。</p>
							<p class="common-bold-black">ここで用いられているサプリメントは、2019年4月にはじめて日本に上陸し、国内ではまだ1社しか取り扱いがないのです。</p>
						</div>
					</div>
					<p>不妊治療や巷の「妊活」には、様々なアプローチ法が存在していますが、私は、不妊治療は<span class="common-bg-yellowred">「妊娠率(分娩率)」</span>と<span class="common-bg-yellowred">「安全性」</span>で選ぶべきだと考えております。</p>
					<p class="common-bg-yellowred">だからこそ、外科的な処置が一切伴わない、妊娠率89%を達成した英国式療法をお勧めしております。</p>
					<div class="column-after-red-text displayFlex_f">
						<div class="displayFlex_f_left">
						※
						</div>	
						<div class="displayFlex_f_Right ml-3">
						この治療法は、患者様の症状に合わせて最適な処方が異なります。<br>もしご自身に合った治療法をお知りになりたい場合は、下記の無料相談(問診)を通して<br>お問い合せ下さい。
						</div>		
					</div>
	　			</div>
			</div>
			<div class="dark-red-button red-button-common">
				<a href="http://projects.cloudproduction.co.bd/tmclinic/consultation" type="button" class="btn btn-lg btn-red-custom waves-effect waves-light">不妊治療無料相談はこちら</a>
			</div>
			<div class="ceo-bottom-container col-md-9">
				<div class="line-wrapper">
					<h3 class="column-under-list-title blue-text-common">監修</h3>
				</div>
				<div class="colunder-bottommedia-container">
					<div class="media">
						<div class="col-under-media-text-container">
							<?= image('img/ceo1.png','','ceo')?>
						</div>
						<div class="media-body">
							<p class="blue-text-common ceo-right-text">外来部長 <span class="pt-up-font">山下明子先生</span><span>医学博士、内科医、神経内科専門医、抗加齢医学専門医</span></p>
						</div>
					</div>
					<div class="media">
						<div class="col-under-media-text-container">
							<?= image('img/ceo2.png','','ceo2')?>
						</div>
						<div class="media-body">
							<p class="blue-text-common ceo-right-text">顧問医師 <span class="pt-up-font">身原正哉先生</span><span>産婦人科専門医・日本抗加齢医学会専門医</span></p>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-3 colunder-right-section">
			<?php view('common/ceo') ?>
		</div>
	</div>
</div>