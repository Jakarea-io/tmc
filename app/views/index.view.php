<div class="container">
	<div class="row top-bg-container">
		<div class="col-md-12">
			<?= image('img/top-bg.png','common-img not-mb-sreen-top-bg','banner')?>
		</div>
	</div>
	<div class="row doctor-message">
		<div class="col-md-10 offset-md-1">
			<div class="media media-top-page-wrapper">
				<?= image('img/doctor.png','doctor-md','doctor')?>
				<div class="media-body message-wrapper-container">
					<div class="message-wrapper">
						<h1>Message</h1>
						<h5>山下外来部長からのメッセージ</h5>
						<p>英国では不妊治療にサプリメントが使われており、自然妊娠率89％という驚異的な結果を出しています。<br>日本でも不妊で悩む方々に喜びをお届けしたいと思い、このサプリメントを扱うことになりました。
						</p>
					</div>
				</div>
			</div>
			<a type="button" href="<?=url('consultation')?>" class="btn btn-lg common-button">不妊治療無料相談はこちら <i class="fas fa-chevron-right"></i></a>
		</div>
	</div>
</div>		