<div class="container common-pages-container">
	<div class="row top-about-bg-container">
		<div class="col-md-12 common-bg-wrapper">
			<div class="about-bg-content">
				<h2>CLINIC GUIDE</h2>
				<h4>クリニック案内</h4>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="content-container col-md-10 offset-md-1">
			<ol class="breadcrumb">
				<li class="breadcrumb-item"><a href="#">TOP  <i class="fas fa-angle-right breadcumb-icon"></i> </a></li>
				<li class="breadcrumb-item active">クリニック案内</li>
			</ol>
		</div>
	</div>
	<div class="row">
		<div class="col-md-10 offset-md-1 col-12">
			<div class="about-image-with-title" align="center">
				<?= image('img/about-ribbon.png','ribbon-image','ribbon-image')?>
				<h3>不妊外来について</h3>
			</div>
			<div class="row about-bg-wrapper">
				<div class="col-md-6 about-bg-left">
					<?= image('img/about-desc.png','common-img','about-desc-image')?>
				</div>
				<div class="col-md-6 about-bg-right">
					<p>TMクリニック不妊外来では、医学的見地にもとづいた栄養療法を
						取り入れています。<br>
						妊娠するには、まず妊娠しやすい身体を作りが大切だと考えます。<br><br>
						英国では20年以上前から使われてている89%の任率を誇る不妊治療に特化したサプリを使って、卵子のエイジングケアを行いながら、自然な受精、着床、妊娠へ導きます。<br>
						同時に男性も一緒に服用することで、精子の状態を改善します。
					</p>
				</div>
			</div>
		</div>
	</div>
	<div class="row about-middle-bg">
		<div class="border-button col-md-12">
			<a type="button" class="btn btn-lg btn-border-custom">医師紹介</a>
		</div>
		<div class="col-md-4 about-middle-left-images">
			<?= image('img/about-intro-1.png','common-img','about-intro-image')?>
		</div>
		<div class="col-md-8">
			<div class="about-right-block-1">
				<div class="common-about-title">
					<h3>外来部長</h3>
					<h1>山下明子先生</h1>
				</div>
				<h4>医学博士、内科医、神経内科専門医、抗加齢医学専門医</h4>
				<ul>
					<li>平成15年　福岡大学病院勤務</li>
					<li>平成17年　米国メイヨー・クリニック留学</li>
					<li>平成19年　医療法人社団　如水会　今村病院理事</li>
					<li>平成28年　株式会社マインドフルヘルスを設立</li>
				</ul>
			</div>
		</div>
		<div class="col-md-4 about-middle-right-images">
			<?= image('img/about-intro-2.png','common-img','about-intro-image')?>
		</div>
		<div class="col-md-8">
			<div class="about-right-block-2">
				<div class="common-about-title">
					<h3>顧問医師</h3>
					<h1>身原正哉先生</h1>
				</div>
				<h4>産婦人科専門医・日本抗加齢医学会専門医</h4>
				<ul>
					<li>平成21年　分娩件数1021件以上(平成28年度)</li>
					<li>京都で50年以上続く産婦人科の院長</li>
				</ul>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12 button-wrapper">
			<div class="dark-pest-button">
				<a type="button" class="btn btn-lg btn-pest" href="http://tmclinic.online/" target="_blank">東京TMクリニック公式サイトはこちら</a>
			</div>
			<div class="dark-red-button">
				<a type="button" class="btn btn-lg btn-red-custom" href="<?=url('consultation')?>">不妊治療無料相談はこちら</a>
				
			</div>
		</div>
	</div>
</div>