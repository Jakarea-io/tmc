<section class="top-section">
    <div class="container">
        <div class="row top-about-bg-container">
            <div class="col-md-12 common-bg-wrapper">
                <div class="about-bg-content">
                    <h2 align="center">CONSULTATION</h2>
                    <h4 align="center">不妊治療無料相談問</h4>
                </div>
            </div>
	    </div>
	    <div class="row">
            <div class="content-container col-md-10 offset-md-1">
                <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="#">TOP <i class="fas fa-angle-right breadcumb-icon"></i></a></li>
                <li class="breadcrumb-item active">不妊治療無料相談</li>
                </ol>
                <p class="text-center">山下先生監修 不妊治療無料診断。<br> 下記の質問に答えていただくと、山下先生があなたの不妊治療に関するアドバイスをメールでお送りします。 <br>
                不妊治療サプリ「Ωフォリック」をご購入ご希望の方もこちらの問診票への回答が必要です。 </p>
            </div>
        </div>
        <div class="row question-ans-container">
            <div class="col-md-10 offset-md-1 question-ans-section">
                <form method='post' action="<?= url('consultation/post') ?>">
                    <div class="question1">
                        <h2 class="d-inline-block">Q1 .</h2>
                        <p class="d-inline">血縁のご家族に下記の病気の方はいますか</p>
                        <span>※必須</span>
                        <div class="question1-radio" id="question1Dropdown">
                            <div class="form-check-inline">
                                <label class="form-check-label">
                                <input type="radio" class="form-check-input" id="yesCheck" value='いる' name="question_1" <?= (old('question_1') == 'いる') ? 'checked':''?>> いる
                                </label>
                            </div>
                            <div class="form-check-inline">
                                <label class="form-check-label">
                                <input type="radio" class="form-check-input" value='はい' name="question_1" <?= (old('question_1') == 'はい') ? 'checked':''?>>いない
                                </label>
                            </div>
                        </div>
                        <div class="checkbox-wrapper cltn-q1-drpdown-wrap" id="showQuestion1Dropdown">
                            <div class="form-check">
                                <h5>脳梗塞</h5>
                                <div class="form-check-inline">
                                    <label class="form-check-label">
                                    <input type="checkbox" class="form-check-input" value='両親' name="question_1.1[]" >両親
                                    </label>
                                </div>
                                <div class="form-check-inline">
                                    <label class="form-check-label">
                                    <input type="checkbox" class="form-check-input" value='両親' name="question_1.1[]" >祖父母
                                    </label>
                                </div>
                                <div class="form-check-inline">
                                    <label class="form-check-label">
                                    <input type="checkbox" class="form-check-input" value='両親' name="question_1.1[]" >叔父・叔母
                                    </label>
                                </div>
                                <div class="form-check-inline">
                                    <label class="form-check-label">
                                    <input type="checkbox" class="form-check-input" value='両親' name="question_1.1[]" >兄弟姉妹
                                    </label>
                                </div>
                                <div class="form-check-inline">
                                    <label class="form-check-label">
                                    <input type="checkbox" class="form-check-input" value='両親' name="question_1.1[]" >その他
                                    </label>
                                </div>
                            </div>
                            <div class="form-check">
                                <h5>心筋梗塞</h5>
                                <div class="form-check-inline">
                                    <label class="form-check-label">
                                    <input type="checkbox" class="form-check-input" value='両親' name="question_1.2[]" >両親
                                    </label>
                                </div>
                                <div class="form-check-inline">
                                    <label class="form-check-label">
                                    <input type="checkbox" class="form-check-input" value='両親' name="question_1.2[]" >祖父母
                                    </label>
                                </div>
                                <div class="form-check-inline">
                                    <label class="form-check-label">
                                    <input type="checkbox" class="form-check-input" value='叔父・叔母' name="question_1.2[]" >叔父・叔母
                                    </label>
                                </div>
                                <div class="form-check-inline">
                                    <label class="form-check-label">
                                    <input type="checkbox" class="form-check-input" value='兄弟姉妹' name="question_1.2[]" >兄弟姉妹
                                    </label>
                                </div>
                                <div class="form-check-inline">
                                    <label class="form-check-label">
                                    <input type="checkbox" class="form-check-input" value='その他' name="question_1.2[]" >その他
                                    </label>
                                </div>
                            </div>
                            <div class="form-check">
                                <h5>狭心症</h5>
                                <div class="form-check-inline">
                                    <label class="form-check-label">
                                    <input type="checkbox" class="form-check-input" value='両親' name="question_1.3[]" >両親
                                    </label>
                                </div>
                                <div class="form-check-inline">
                                    <label class="form-check-label">
                                        <input type="checkbox" class="form-check-input" value='両親' name="question_1.3[]" >祖父母
                                    </label>
                                </div>
                                <div class="form-check-inline">
                                    <label class="form-check-label">
                                        <input type="checkbox" class="form-check-input" value='祖父母' name="question_1.3[]" >叔父・叔母
                                    </label>
                                </div>
                                <div class="form-check-inline">
                                    <label class="form-check-label">
                                        <input type="checkbox" class="form-check-input" value='兄弟姉妹' name="question_1.3[]" >兄弟姉妹
                                    </label>
                                </div>
                                <div class="form-check-inline">
                                    <label class="form-check-label">
                                        <input type="checkbox" class="form-check-input" value='その他' name="question_1.3[]" >その他
                                    </label>
                                </div>
                                
                            </div>
                            <div class="form-check">
                                <h5>肺塞栓症</h5>
                                <div class="form-check-inline">
                                    <label class="form-check-label">
                                    <input type="checkbox" class="form-check-input" value='両親' name="question_1.4[]" >両親
                                    </label>
                                </div>
                                <div class="form-check-inline">
                                    <label class="form-check-label">
                                        <input type="checkbox" class="form-check-input" value='両親' name="question_1.4[]" >祖父母
                                    </label>
                                </div>
                                <div class="form-check-inline">
                                    <label class="form-check-label">
                                        <input type="checkbox" class="form-check-input" value='祖父母' name="question_1.4[]" >叔父・叔母
                                    </label>
                                </div>
                                <div class="form-check-inline">
                                    <label class="form-check-label">
                                        <input type="checkbox" class="form-check-input" value='兄弟姉妹' name="question_1.4[]" >兄弟姉妹
                                    </label>
                                </div>
                                <div class="form-check-inline">
                                    <label class="form-check-label">
                                        <input type="checkbox" class="form-check-input" value='その他' name="question_1.4[]" >その他
                                    </label>
                                </div>
                            </div>
                            <div class="form-check">
                                <h5>深部静脈血栓症</h5>
                                <div class="form-check-inline">
                                    <label class="form-check-label">
                                    <input type="checkbox" class="form-check-input" value='両親' name="question_1.5[]" >両親
                                    </label>
                                </div>
                                <div class="form-check-inline">
                                    <label class="form-check-label">
                                        <input type="checkbox" class="form-check-input" value='両親' name="question_1.5[]" >祖父母
                                    </label>
                                </div>
                                <div class="form-check-inline">
                                    <label class="form-check-label">
                                        <input type="checkbox" class="form-check-input" value='祖父母' name="question_1.5[]" >叔父・叔母
                                    </label>
                                </div>
                                <div class="form-check-inline">
                                    <label class="form-check-label">
                                        <input type="checkbox" class="form-check-input" value='兄弟姉妹' name="question_1.5[]" >兄弟姉妹
                                    </label>
                                </div>
                                <div class="form-check-inline">
                                    <label class="form-check-label">
                                        <input type="checkbox" class="form-check-input" value='その他' name="question_1.5[]" >その他
                                    </label>
                                </div>
                            </div>
                            <div class="form-check">
                                <h5>高血圧</h5>
                                <div class="form-check-inline">
                                    <label class="form-check-label">
                                    <input type="checkbox" class="form-check-input" value='両親' name="question_1.6[]" >両親
                                    </label>
                                </div>
                                <div class="form-check-inline">
                                    <label class="form-check-label">
                                        <input type="checkbox" class="form-check-input" value='両親' name="question_1.6[]" >祖父母
                                    </label>
                                </div>
                                <div class="form-check-inline">
                                    <label class="form-check-label">
                                        <input type="checkbox" class="form-check-input" value='祖父母' name="question_1.6[]" >叔父・叔母
                                    </label>
                                </div>
                                <div class="form-check-inline">
                                    <label class="form-check-label">
                                        <input type="checkbox" class="form-check-input" value='兄弟姉妹' name="question_1.6[]" >兄弟姉妹
                                    </label>
                                </div>
                                <div class="form-check-inline">
                                    <label class="form-check-label">
                                        <input type="checkbox" class="form-check-input" value='その他' name="question_1.6[]" >その他
                                    </label>
                                </div>
                            </div>
                            <div class="form-check">
                                <h5>がん</h5>
                                <div class="form-check-inline">
                                    <label class="form-check-label">
                                    <input type="checkbox" class="form-check-input" value='両親' name="question_1.7[]" >両親
                                    </label>
                                </div>
                                <div class="form-check-inline">
                                    <label class="form-check-label">
                                        <input type="checkbox" class="form-check-input" value='両親' name="question_1.7[]" >祖父母
                                    </label>
                                </div>
                                <div class="form-check-inline">
                                    <label class="form-check-label">
                                        <input type="checkbox" class="form-check-input" value='祖父母' name="question_1.7[]" >叔父・叔母
                                    </label>
                                </div>
                                <div class="form-check-inline">
                                    <label class="form-check-label">
                                        <input type="checkbox" class="form-check-input" value='兄弟姉妹' name="question_1.7[]" >兄弟姉妹
                                    </label>
                                </div>
                                <div class="form-check-inline">
                                    <label class="form-check-label">
                                        <input type="checkbox" class="form-check-input" value='その他' name="question_1.7[]" >その他
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="questiondateRange">
                        <h2>【ご夫婦への問診】</h2>
                        <h2 class="d-inline-block">Q2 .</h2>
                        <p class="d-inline">避妊期間はありますか</p>
                        <div class="question4-radio" id="dateRangePullDown">
                            <div class="form-check-inline">
                                <label class="form-check-label">
                                <input type="radio" class="form-check-input" value='はい' name="question_2">はい
                                </label>
                            </div>
                            <div class="form-check-inline">
                                <label class="form-check-label">
                                <input type="radio" class="form-check-input" value='いいえ' name="question_2">いいえ
                                </label>
                            </div>
                        </div>
                        <!-- <div class="input-group input-daterange col-md-6" id="dateRangeWrapper">
                            <input id="startDate1" name="startdate_2.1" type="text"
                                class="form-control" readonly="readonly"> 
                            <span class="input-group-addon">to</span> <input id="endDate1"
                                name="endDate_2.2" type="text" class="form-control" readonly="readonly">
                        </div> -->
                    </div>
                    <div class="question5">
                        <h2 class="d-inline-block">Q3 .</h2>
                        <p class="d-inline">妊娠歴</p>
                        <div>
                            <div class="q5-inner-select d-inline-block">妊娠したことがありますか?</div>
                            <div class="d-inline radio-inline-q3" id="question3SelectPulldown">
                                <div class="form-check-inline">
                                    <label class="form-check-label">
                                    <input type="radio" class="form-check-input" value='はい' name="question_3">はい
                                    </label>
                                </div>
                                <div class="form-check-inline">
                                    <label class="form-check-label">
                                    <input type="radio" class="form-check-input" value='いいえ' name="question_3">いいえ
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="row wrapper-of-select-wrap question3-select-wrap" id="q2selectHide">
                            <table class="table table-bordered pregnency-history-table">
                                <thead class="thead-dark">
                                    <tr>
                                        <th>妊娠</th>
                                        <th>出産</th>
                                        <th>流産</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>
                                            <select  name = "question_3.1" class="browser-default custom-select contact-select question5-select">
                                                <option value="1">1</option>
                                                <option value="2">2</option>
                                                <option value="3">3</option>
                                                <option value="4">4</option>
                                                <option value="5">５回以上</option>
                                            </select>
                                        </td>
                                        <td>
                                            <select  name = "question_3.2" class="browser-default custom-select contact-select question5-select">
                                                <option value="1">1</option>
                                                <option value="2">2</option>
                                                <option value="3">3</option>
                                                <option value="4">4</option>
                                                <option value="5">５回以上</option>
                                            </select>
                                        </td>
                                        <td>
                                            <select  name = "question_3.3" class="browser-default custom-select contact-select question5-select">
                                                <option value="1">1</option>
                                                <option value="2">2</option>
                                                <option value="3">3</option>
                                                <option value="4">4</option>
                                                <option value="5">５回以上</option>
                                            </select>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="question4">
                        <h2 class="d-inline-block">Q.4 .</h2>
                        <p class="d-inline">般不妊検査歴</p>
                        <span>※必須</span>
                        <div class="question4-pulldown-wrapper">
                            <table class="table table-bordered">
                                <tbody>
                                    <tr>
                                        <td>精液検査</td>
                                        <td>
                                            <select  name = "question_4.1" class="browser-default custom-select contact-select question5-select">
                                                <option></option>
                                                <option value="1">有</option>
                                                <option value="2">無</option>
                                            </select>
                                        </td>
                                        <td>
                                            <select  name = "question_4.2" class="browser-default custom-select contact-select question5-select">
                                                <option></option>
                                                <option value="1">正常</option>
                                                <option value="2">異常</option>
                                            </select>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>フーナー検査</td>
                                        <td>
                                            <select  name = "question_4.3" class="browser-default custom-select contact-select question5-select">
                                                <option></option>
                                                <option value="1">有</option>
                                                <option value="2">無</option>
                                            </select>
                                        </td>
                                        <td>
                                            <select  name = "question_4.4" class="browser-default custom-select contact-select question5-select">
                                                <option></option>
                                                <option value="1">正常</option>
                                                <option value="2">異常</option>
                                            </select>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>子宮卵管造影</td>
                                        <td>
                                            <select  name = "question_4.5" class="browser-default custom-select contact-select question5-select">
                                                <option></option>
                                                <option value="1">有</option>
                                                <option value="2">無</option>
                                            </select>
                                        </td>
                                        <td>
                                            <select  name = "question_4.6" class="browser-default custom-select contact-select question5-select">
                                                <option></option>
                                                <option value="1">正常</option>
                                                <option value="2">異常</option>
                                            </select>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>子宮鏡検査</td>
                                        <td>
                                            <select  name = "question_4.7" class="browser-default custom-select contact-select question5-select">
                                                <option></option>
                                                <option value="1">有</option>
                                                <option value="2">無</option>
                                            </select>
                                        </td>
                                        <td>
                                            <select  name = "question_4.8" class="browser-default custom-select contact-select question5-select">
                                                <option></option>
                                                <option value="1">正常</option>
                                                <option value="2">異常</option>
                                            </select>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>クラミジア検査</td>
                                        <td>
                                            <select  name = "question_4.9" class="browser-default custom-select contact-select question5-select">
                                                <option></option>
                                                <option value="1">有</option>
                                                <option value="2">無</option>
                                            </select>
                                        </td>
                                        <td>
                                            <select  name = "question_4.10" class="browser-default custom-select contact-select question5-select">
                                                <option></option>
                                                <option value="1">正常</option>
                                                <option value="2">異常</option>
                                            </select>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>卵誘発剤</td>
                                        <td>
                                            <select  name = "question_4.11" class="browser-default custom-select contact-select question5-select">
                                                <option></option>
                                                <option value="1">有</option>
                                                <option value="2">無</option>
                                            </select>
                                        </td>
                                        <td>
                                            <select  name = "question_4.12" class="browser-default custom-select contact-select question5-select">
                                                <option></option>
                                                <option value="1">正常</option>
                                                <option value="2">異常</option>
                                            </select>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="question5">
                        <h2 class="d-inline-block">Q5 .</h2>
                        <p class="d-inline">治療歴</p>
                        <div class="row wrapper-of-select-wrap q5default-layout">
                            <table class="table table-bordered">
                                <tbody>
                                    <tr>
                                        <td>タイミング法</td>
                                        <td>人工授精</td>
                                        <td>採卵</td>
                                        <td>胚移植</td>
                                        <td>TESE</td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <select  name = "question_5.1" class="browser-default custom-select contact-select question5-select">
                                                <option></option>
                                                <option value="1">1</option>
                                                <option value="2">2</option>
                                                <option value="3">3</option>
                                                <option value="4">4</option>
                                                <option value="5">５回以上</option>
                                            </select>
                                        </td>
                                        <td>
                                            <select  name = "question_5.2" class="browser-default custom-select contact-select question5-select">
                                                <option></option>
                                                <option value="1">1</option>
                                                <option value="2">2</option>
                                                <option value="3">3</option>
                                                <option value="4">4</option>
                                                <option value="5">５回以上</option>
                                            </select>
                                        </td>
                                        <td>
                                            <select  name = "question_5.3" class="browser-default custom-select contact-select question5-select">
                                                <option></option>
                                                <option value="1">1</option>
                                                <option value="2">2</option>
                                                <option value="3">3</option>
                                                <option value="4">4</option>
                                                <option value="5">５回以上</option>
                                            </select>
                                        </td>
                                        <td>
                                            <select  name = "question_5.4" class="browser-default custom-select contact-select question5-select">
                                                <option></option>
                                                <option value="1">1</option>
                                                <option value="2">2</option>
                                                <option value="3">3</option>
                                                <option value="4">4</option>
                                                <option value="5">５回以上</option>
                                            </select>
                                        </td>
                                        <td>
                                            <select  name = "question_5.5" class="browser-default custom-select contact-select question5-select">
                                                <option></option>
                                                <option value="1">1</option>
                                                <option value="2">2</option>
                                                <option value="3">3</option>
                                                <option value="4">4</option>
                                            </select>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>

                    <div class="question6">
                        <h2 class="d-inline-block">Q6 .</h2>
                        <p class="d-inline">結婚(法律婚)していますか</p>
                        <div class="question4-radio" id="question4Dropdown">
                            <div class="form-check-inline">
                                <label class="form-check-label">
                                <input type="radio" class="form-check-input" value='はい（初婚）' name="question_6" checked>はい（初婚）
                                </label>
                            </div>
                            <div class="form-check-inline">
                                <label class="form-check-label">
                                <input type="radio" class="form-check-input" value='はい（再婚）' name="question_6">はい（再婚）
                                </label>
                            </div>
                            <div class="form-check-inline">
                                <label class="form-check-label">
                                <input type="radio" class="form-check-input" value='異常いいえ(離婚)'  name="question_6">いいえ(離婚)
                                </label>
                            </div>
                            <div class="form-check-inline">
                                <label class="form-check-label">
                                <input type="radio" class="form-check-input" value='いいえ(婚約)'  name="question_6">いいえ(婚約)
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="question5">
                        <h2 class="highlight-gender">【女性問診編】</h2>
                        <h2 class="">１－２ .</h2>
                        <p class="d-inline">結婚されている場合、その開始はいつですか？</p>
                        <div class="datepicker-wrapper">
                        日付: <input type="text" id="datepicker" name="Women_question_1.2">
                        </div>
                    </div>
                    <div class="question5">
                        <h2 class="">１－３ .</h2>
                        <p class="d-inline">１で再婚の場合、前夫との間に妊娠したことがありますか</p>
                        <div class="question4-radio">
                            <div class="form-check-inline">
                                <label class="form-check-label">
                                <input type="radio" class="form-check-input" value='はい' name="Women_question_1.3" checked>はい
                                </label>
                            </div>
                            <div class="form-check-inline">
                                <label class="form-check-label">
                                <input type="radio" class="form-check-input" value='いいえ' name="Women_question_1.3">いいえ
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="question5">
                        <h2 class="">１－４ .</h2>
                        <p class="d-inline">性行為に支障がありますか</p>
                        <div class="question4-radio">
                            <div class="form-check-inline">
                                <label class="form-check-label">
                                <input type="radio" class="form-check-input" value='はい' name="Women_question_1.4" checked>はい
                                </label>
                            </div>
                            <div class="form-check-inline">
                                <label class="form-check-label">
                                <input type="radio" class="form-check-input" value='いいえ' name="Women_question_1.4">いいえ
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="question2to">
                        <h2 class="d-inline-block">１－5</h2>
                        <p class="d-inline">月経についてお尋ねします</p>
                    </div>
                    <div class="question5 question2-inner question-2_1_inner">
                        <h2 class="">2 － 1 . </h2>
                        <div class="datepicker-wrapper d-inline question2-datepicker datepicker-common">
                            最終月経開始日: <input type="text" id="datepicker1" name="Women_question_2.1">
                        </div>
                    </div>
                    <div class="question5 question2-inner">
                        <h2 class="">２－２ .</h2>
                        <!-- <p class="d-inline">２－２．その前の月経開始日</p> -->
                        <div class="datepicker-wrapper1 d-inline-block datepicker-common">
                            その前の月経開始日: <input type="text" id="datepicker2" name="Women_question_2.2" >
                        </div>
                    </div>
                    <div class="question5">
                        <h2 class="">２－３</h2>
                        <p class="d-inline">月経周期</p>
                        <div class="question4-radio" id="question12fire">
                            <div class="form-check-inline">
                                <label class="form-check-label">
                                <input type="radio" class="form-check-input" value='整' name="Women_question_2.3">整
                                </label>
                            </div>
                            <div class="form-check-inline">
                                <label class="form-check-label">
                                <input type="radio" class="form-check-input" value='不整' name="Women_question_2.3.1">不整
                                </label>
                            </div>
                        </div>
                        <div class="question-13-adjustment-pulldown" id="adjustmentPulldown">
                            <div class="md-form col-md-6">
                                <select  name ="Women_question_2.3.1" class="browser-default custom-select contact-select question5-select">
                                    <option value="15">15</option>
                                    <option value="20">20</option>
                                    <option value="25">25</option>
                                    <option value="30">30</option>
                                </select>
                            </div>
                        </div>
                        <div class="question-13-iregularity-pulldown" id="iregularityPulldown">
                            <div class="row">
                                <div class="md-form col-md-6">
                                    <select  name = "Women_question_2.3.1" class="browser-default custom-select contact-select question5-select">
                                        <option value="7">7</option>
                                        <option value="14">14</option>
                                        <option value="21">21</option>
                                        <option value="28">28</option>
                                        <option value="35">35</option>
                                        <option value="42">42</option>
                                        <option value="49">49</option>
                                    
                                    </select>
                                    <p class="d-inline-block">日周期</span>
                                    
                                </div>
                                <div class="md-form col-md-6">
                                    <select  name = "Women_question_2.3.2" class="browser-default custom-select contact-select question5-select">
                                        <option value="7">7</option>
                                        <option value="14">14</option>
                                        <option value="21">21</option>
                                        <option value="28">28</option>
                                        <option value="35">35</option>
                                        <option value="42">42</option>
                                        <option value="49">49</option>
                                    </select>
                                    <div class="d-inline-block">日周期</div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="question5">
                        <h2 class="">２－4 .</h2>
                        <p class="d-inline">月経の量</p>
                        <span>※必須</span>
                        <div class="md-form col-md-6">
                            <select  name = "Women_question_2.4" class="browser-default custom-select contact-select question5-select">
                                <option value="so many">とてもたくさん</option>
                                <option value="Many">たくさんの</option>
                                <option value="usually">通常</option>
                                <option value="Few">ほんの少し</option>
                                <option value="Very little">ほんの少し</option>
                                <option value="Other">その他の</option>
                            </select>
                        </div>
                    </div>
                    <div class="question5">
                        <h2 class="">２－５.</h2>
                        <p class="d-inline">月経痛</p>
                        <span>※必須</span>
                        <div class="md-form col-md-6">
                            <select  name = "Women_question_2.5" class="browser-default custom-select contact-select question5-select">
                                <option value="Everyday">日常生活はとても痛いので難しいです</option>
                                <option value="To hurt">傷つけるt</option>
                                <option value="Sometimes ache">時々痛み</option>
                                <option value="It does not hurt too much.">あまり痛くないです</option>
                                <option value="No pain">痛みなし</option>
                                <option value="Other">その他の</option>
                            </select>
                        </div>
                    </div>
                    <div class="question5">
                        <h2 class="">２－６ .</h2>
                        <p class="d-inline">初潮</p>
                        <div class="md-form col-md-6">
                            <select  name = "Women_question_2.6" class="browser-default custom-select contact-select question5-select">
                                <option value="10 to 1 up to 20 years old">10歳から20歳まで</option>
                                <option value="over 20 years old">20歳以上</option>
                            </select>
                        </div>
                    </div>
                    <div class="question5 q3smoke-cigarrate-wrapper">
                        <h2 class="d-inline-block">Q3 .</h2>
                        <p class="d-inline">タバコを吸いますか</p>
                        <div class="question4-radio" id="q3smokeCigarrateWrapper">
                            <div class="form-check-inline">
                                <label class="form-check-label">
                                <input type="radio" class="form-check-input" value='はい' name="Women_question_3" checked>はい
                                </label>
                            </div>
                            <div class="form-check-inline">
                                <label class="form-check-label">
                                <input type="radio" class="form-check-input" value='いいえ' name="Women_question_3">いいえ
                                </label>
                            </div>
                        </div>
                        <div class="col-md-6 pulldownsmoke" id="q3smokeCigarrate">
                            <select  name = "Women_question_3.1" class="browser-default custom-select contact-select question5-select">
                                <option></option>
                                <option value="1">1</option>
                                <option value="2">2</option>
                                <option value="3">3</option>
                                <option value="4">4</option>
                                <option value="5">5</option>
                                <option value="6">6</option>
                                <option value="7">7</option>
                                <option value="8">8</option>
                                <option value="9">9</option>
                                <option value="10">10</option>
                                <option value="11">11</option>
                                <option value="12">12</option>
                                <option value="13">13</option>
                                <option value="14">14</option>
                                <option value="15">15</option>
                                <option value="16">16</option>
                                <option value="17">17</option>
                                <option value="18">18</option>
                                <option value="19">19</option>
                                <option value="20">20</option>
                                <option value="21">21</option>
                                <option value="22">22</option>
                                <option value="23">23</option>
                                <option value="24">24</option>
                                <option value="25">25</option>
                                <option value="26">26</option>
                                <option value="4">27</option>
                                <option value="4">28</option>
                                <option value="4">29</option>
                                <option value="4">more than 30</option>
                            </select>
                        </div>
                    </div>
                    <div class="question5">
                        <h2 class="d-inline-block">Q4 .</h2>
                        <p class="d-inline">お酒を飲みますか</p>
                        <div class="question4-radio" id="q3alcoholWrapper">
                            <div class="form-check-inline">
                                <label class="form-check-label">
                                <input type="radio" class="form-check-input" value='はい' name="Women_question_4">はい
                                </label>
                            </div>
                            <div class="form-check-inline">
                                <label class="form-check-label">
                                <input type="radio" class="form-check-input" value='いいえ' name="Women_question_4">いいえ
                                </label>
                            </div>
                        </div>
                        <div class="col-md-6 pulldownsmoke" id="q3Alcohol">
                            <p class="d-inline-block">週 :</p>
                            <select  name = "Women_question_4.1" class="browser-default custom-select contact-select question5-select d-inline">
                                <option></option>
                                <option value="1">1</option>
                                <option value="2">2</option>
                                <option value="3">3</option>
                                <option value="4">4</option>
                                <option value="5">5</option>
                                <option value="6">6</option>
                                <option value="7">7</option>
                            </select>
                        </div>
                    </div>
                    <div class="question5">
                        <h2 class="d-inline-block">Q5 .</h2>
                        <p class="d-inline">職業上、有機溶剤を使用していますか？</p>
                        <div class="question4-radio">
                            <div class="form-check-inline">
                                <label class="form-check-label">
                                <input type="radio" class="form-check-input" value='はい' name="Women_question_5" checked>はい
                                </label>
                            </div>
                            <div class="form-check-inline">
                                <label class="form-check-label">
                                <input type="radio" class="form-check-input" value='いいえ' name="Women_question_5">いいえ
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="question5">
                        <h2 class="d-inline-block">Q6 .</h2>
                        <p class="d-inline">職業上、放射性や放射性物質を使用していますか</p>
                        <div class="question4-radio">
                            <div class="form-check-inline">
                                <label class="form-check-label">
                                <input type="radio" class="form-check-input" value='はい' name="Women_question_6" checked>はい
                                </label>
                            </div>
                            <div class="form-check-inline">
                                <label class="form-check-label">
                                <input type="radio" class="form-check-input" value='いいえ' name="Women_question_6">いいえ
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="question5">
                        <h2 class="d-inline-block">Q7 .</h2>
                        <p class="d-inline">大豆アレルギーはありますか？</p>
                        <div class="question4-radio">
                            <div class="form-check-inline">
                                <label class="form-check-label">
                                <input type="radio" class="form-check-input" value='はい' name="Women_question_7">はい
                                </label>
                            </div>
                            <div class="form-check-inline">
                                <label class="form-check-label">
                                <input type="radio" class="form-check-input" value='いいえ' name="Women_question_7">いいえ
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="question5">
                        <h2 class="d-inline-block">Q8 .</h2>
                        <p class="d-inline">子宮頸がん検査を受けたことがありますか？</p>
                        <div class="question4-radio">
                            <div class="form-check-inline">
                                <label class="form-check-label">
                                <input type="radio" class="form-check-input" value='はい' name="Women_question_8">はい
                                </label>
                            </div>
                            <div class="form-check-inline">
                                <label class="form-check-label">
                                <input type="radio" class="form-check-input" value='いいえ' name="Women_question_8">いいえ
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="question5">
                        <h2 class="d-inline-block">Q9 .</h2>
                        <p class="d-inline">薬剤や食べ物で症状(アレルギー等)が出た事はありますか？</p>
                        <div class="question4-radio" id="allergyCHeckboxask">
                            <div class="form-check-inline">
                                <label class="form-check-label">
                                <input type="radio" class="form-check-input" value='はい' name="Women_question_9">はい
                                </label>
                            </div>
                            <div class="form-check-inline">
                                <label class="form-check-label">
                                <input type="radio" class="form-check-input" value='いいえ' name="Women_question_9">いいえ
                                </label>
                            </div>
                        </div>
                        <div class="double-checkbox-wrapper" id="doubleCheckboxPulldown">
                            <p>原因物質</p>
                            <div class="form-check form-check-first-wrapper">
                                <div class="form-check-inline">
                                    <label class="form-check-label">
                                    <input type="checkbox" class="form-check-input" value='卵' name="Women_question_9.1[]" >卵
                                    </label>
                                </div>
                                <div class="form-check-inline">
                                    <label class="form-check-label">
                                    <input type="checkbox" class="form-check-input" value='乳' name="Women_question_9.1[]" >乳
                                    </label>
                                </div>
                                <div class="form-check-inline">
                                    <label class="form-check-label">
                                    <input type="checkbox" class="form-check-input" value='小麦' name="Women_question_9.1[]" >小麦
                                    </label>
                                </div>
                                <div class="form-check-inline">
                                    <label class="form-check-label">
                                    <input type="checkbox" class="form-check-input" value='そば' name="Women_question_9.1[]" >そば
                                    </label>
                                </div>
                                <div class="form-check-inline">
                                    <label class="form-check-label">
                                    <input type="checkbox" class="form-check-input" value='落花生' name="Women_question_9.1[]" >落花生
                                    </label>
                                </div>
                                <div class="form-check-inline">
                                    <label class="form-check-label">
                                    <input type="checkbox" class="form-check-input" value='えび' name="Women_question_9.1[]" >えび
                                    </label>
                                </div>
                                <div class="form-check-inline">
                                    <label class="form-check-label">
                                    <input type="checkbox" class="form-check-input" value='かに' name="Women_question_9.1[]" >かに
                                    </label>
                                </div>
                            </div>
                            <p>症状</p>
                            <div class="form-check form-check-second-wrapper">
                                <div class="form-check-inline">
                                    <label class="form-check-label">
                                    <input type="checkbox" class="form-check-input" value='かゆみ' name="Women_question_9.2[]" >かゆみ
                                    </label>
                                </div>
                                <div class="form-check-inline">
                                    <label class="form-check-label">
                                    <input type="checkbox" class="form-check-input" value='じんましん' name="Women_question_9.2[]" >じんましん
                                    </label>
                                </div>
                                <div class="form-check-inline">
                                    <label class="form-check-label">
                                    <input type="checkbox" class="form-check-input" value='赤くなる' name="Women_question_9.2[]" >赤くなる
                                    </label>
                                </div>
                                <div class="form-check-inline">
                                    <label class="form-check-label">
                                    <input type="checkbox" class="form-check-input" value='下痢' name="Women_question_9.2[]" >下痢
                                    </label>
                                </div>
                                <div class="form-check-inline">
                                    <label class="form-check-label">
                                    <input type="checkbox" class="form-check-input" value='吐き気' name="Women_question_9.2[]" >吐き気
                                    </label>
                                </div>
                                <div class="form-check-inline">
                                    <label class="form-check-label">
                                    <input type="checkbox" class="form-check-input" value='くしゃみ' name="Women_question_9.2[]" >くしゃみ・せき
                                    </label>
                                </div>
                                <div class="form-check-inline">
                                    <label class="form-check-label">
                                    <input type="checkbox" class="form-check-input" value='鼻づまり' name="Women_question_9.2[]" >鼻づまり・鼻水
                                    </label>
                                </div>
                                
                                <div class="form-check-inline">
                                    <label class="form-check-label">
                                    <input type="checkbox" class="form-check-input" value='アナフィラキシーショック' name="Women_question_9.2[]" >アナフィラキシーショック	
                                    </label>
                                </div>
                                <div class="form-check-inline">
                                    <label class="form-check-label">
                                    <input type="checkbox" class="form-check-input" value='チアノーゼ' name="Women_question_9.2[]" >チアノーゼ
                                    </label>
                                </div>
                                <div class="form-check-inline">
                                    <label class="form-check-label">
                                    <input type="checkbox" class="form-check-input" value='血圧低下' name="Women_question_9.2[]" >血圧低下
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="question5">
                        <h2 class="d-inline-block">Q10 .</h2>
                        <p class="d-inline">病院で処方されている薬はありますか？</p>
                        <div class="question4-radio" id="maleqInputPulldown">
                            <div class="form-check-inline">
                                <label class="form-check-label">
                                <input type="radio" class="form-check-input" id="inputPulldown" value='はい' name="Women_question_10">はい
                                </label>
                            </div>
                            <div class="form-check-inline">
                                <label class="form-check-label">
                                <input type="radio" class="form-check-input" value='いいえ' name="Women_question_10">いいえ
                                </label>
                            </div>
                            <div class="input-wrapper">
                                <input type="text" id="" name="Women_question_10.1" value="" class="form-control" placeholder="(自由記入)">
                            </div>
                        </div>
                    </div>
                    <div class="question5">
                        <h2 class="d-inline-block">Q11 .</h2>
                        <p class="d-inline">下記のサプリメントを服用していますか？</p>
                        <div class="question4-radio">
                            <div class="form-check-inline">
                                <label class="form-check-label">
                                <input type="radio" class="form-check-input" value='はい' name="Women_question_11">はい
                                </label>
                            </div>
                            <div class="form-check-inline">
                                <label class="form-check-label">
                                <input type="radio" class="form-check-input" value='いいえ' name="Women_question_11">いいえ
                                </label>
                            </div>
                        </div>
                        <!-- <p>「はい」の場合、チェックボックスで選択</p> -->
                        <div class="question4-radio">
                            <div class="form-check-inline">
                                <label class="form-check-label">
                                <input type="radio" class="form-check-input" value='はい' name="Women_question_11.1">ビタミンD
                                </label>
                            </div>
                            <div class="form-check-inline">
                                <label class="form-check-label">
                                <input type="radio" class="form-check-input" value='いいえ' name="Women_question_11.1">DHEA
                                </label>
                            </div>
                            <div class="form-check-inline">
                                <label class="form-check-label">
                                <input type="radio" class="form-check-input" value='いいえ' name="Women_question_11.1">葉酸
                                </label>
                            </div>
                            <div class="form-check-inline">
                                <label class="form-check-label">
                                <input type="radio" class="form-check-input" value='いいえ' name="Women_question_11.1">イノシトール
                                </label>
                            </div>
                            <div class="form-check-inline">
                                <label class="form-check-label">
                                <input type="radio" class="form-check-input" value='いいえ' name="Women_question_11.1">亜鉛
                                </label>
                            </div>
                        </div>
                    </div>

                    <div class="question5">
                        <h2 class="highlight-gender">【男性問診編】 </h2>
                        <h2 class="d-inline-block">Q1 .</h2>
                        <p class="d-inline">結婚(法律婚)していますか？</p>
                        <div class="question4-radio">
                            <div class="form-check-inline">
                                <label class="form-check-label">
                                <input type="radio" class="form-check-input" value='はい' name="man_question_1">はい（初婚）
                                </label>
                            </div>
                            <div class="form-check-inline">
                                <label class="form-check-label">
                                <input type="radio" class="form-check-input" value='はい（再婚)' name="man_question_1">はい（再婚)
                                </label>
                            </div>
                            <div class="form-check-inline">
                                <label class="form-check-label">
                                <input type="radio" class="form-check-input" value='いいえ(離婚)' name="man_question_1">いいえ(離婚)
                                </label>
                            </div>
                            <div class="form-check-inline">
                                <label class="form-check-label">
                                <input type="radio" class="form-check-input" value='いいえ' name="man_question_1">いいえ（婚約)
                                </label>
                            </div>
                        </div>
                    </div>

                    <div class="question5">
                        <h2 class="">１－２ .</h2>
                        <p class="d-inline">結婚されている場合、それはいつですか？　</p>
                        <div class="question4-radio" id="maleQuestionWrapper">
                            <div class="form-check-inline">
                                <label class="form-check-label">
                                <input type="radio" class="form-check-input" value='はい' name="man_question_1.2">はい
                                </label>
                            </div>
                            <div class="form-check-inline">
                                <label class="form-check-label">
                                <input type="radio" class="form-check-input" value='いいえ' name="man_question_1.2">いいえ
                                </label>
                            </div>
                        </div>
                        <div  id="maleQuestionPulldown">
                            <div class="datepicker-wrapper d-inline question2-datepicker datepicker-common">
                                <input type="text" id="datepicker3" name="man_question_1.2.1">
                            </div>
                        </div>
                    </div>
                    <div class="question5" id="maleQuestionWrapper">
                        <h2 class="">１－３．</h2>
                        <p class="d-inline">再婚の場合、前妻が妊娠したことがありますか？</p>
                        <div class="question4-radio">
                            <div class="form-check-inline">
                                <label class="form-check-label">
                                <input type="radio" class="form-check-input" value='yes' name="man_question_1.3">はい
                                </label>
                            </div>
                            <div class="form-check-inline">
                                <label class="form-check-label">
                                <input type="radio" class="form-check-input" value='no' name="man_question_1.3">いいえ
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="question5" id="maleQuestionWrapper">
                        <h2 class="d-inline-block">Q２ ．</h2>
                        <p class="d-inline">性行為に支障がありますか？</p>
                        <div class="question4-radio">
                            <div class="form-check-inline">
                                <label class="form-check-label">
                                <input type="radio" class="form-check-input" value='yes' name="man_question_2">はい
                                </label>
                            </div>
                            <div class="form-check-inline">
                                <label class="form-check-label">
                                <input type="radio" class="form-check-input" value='no' name="man_question_2">いいえ
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="question5" id="maleQuestionWrapper">
                        <h2 class="d-inline-block">Q３ ．</h2>
                        <p class="d-inline">性欲はありますか？　</p>
                        <div class="question4-radio">
                            <div class="form-check-inline">
                                <label class="form-check-label">
                                <input type="radio" class="form-check-input" value='yes' name="man_question_3">はい
                                </label>
                            </div>
                            <div class="form-check-inline">
                                <label class="form-check-label">
                                <input type="radio" class="form-check-input" value='no' name="man_question_3">いいえ
                                </label>
                            </div>
                        </div>
                     </div>
                     <div class="question5" id="maleQuestionWrapper">
                        <h2 class="d-inline-block">Q４ ．</h2>
                        <p class="d-inline">勃起しますか？　　</p>
                        <div class="question4-radio">
                            <div class="form-check-inline">
                                <label class="form-check-label">
                                <input type="radio" class="form-check-input" value='yes' name="man_question_4">はい
                                </label>
                            </div>
                            <div class="form-check-inline">
                                <label class="form-check-label">
                                <input type="radio" class="form-check-input" value='no' name="man_question_4">いいえ
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="question5" id="maleQuestionWrapper">
                        <h2 class="d-inline-block">Q5 ．</h2>
                        <p class="d-inline">性行為の回数(1週間あたり) :</p>
                        <div class="col-md-6 pulldownsmoke maleq6">
                            <select  name = "man_question_5" class="browser-default custom-select contact-select question5-select d-inline">
                                <option></option>
                                <option value="1">1</option>
                                <option value="2">2</option>
                                <option value="3">3</option>
                                <option value="4">4</option>
                                <option value="5">5</option>
                                <option value="6">6</option>
                                <option value="7">7</option>
                                <option value="8">8</option>
                                <option value="9">9</option>
                                <option value="10">10</option>
                            </select>
                        </div>

                    </div>
                    <div class="question5" id="">
                        <h2 class="d-inline-block">Q6 ．</h2>
                        <p class="d-inline">精液検査を受けた事がありますか？</p>
                        <div class="question4-radio">
                            <div class="form-check-inline">
                                <label class="form-check-label">
                                <input type="radio" class="form-check-input" value='yes' name="man_question_6">はい
                                </label>
                            </div>
                            <div class="form-check-inline">
                                <label class="form-check-label">
                                <input type="radio" class="form-check-input" value='no' name="man_question_6">いいえ
                                </label>
                            </div>
                        </div>
                    </div>

                    <!-- <div class="question5" id="">
                        <h2 class="d-inline-block">Q7 ．</h2>
                        <p class="d-inline"> 精液検査を受けた事がありますか？</p>
                        <div class="question4-radio">
                            <div class="form-check-inline">
                                <label class="form-check-label">
                                <input type="radio" class="form-check-input" value='yes' name="question_male8_radio">はい
                                </label>
                            </div>
                            <div class="form-check-inline">
                                <label class="form-check-label">
                                <input type="radio" class="form-check-input" value='no' name="question_male8_radio">いいえ
                                </label>
                            </div>
                        </div>
                    </div> -->
                    <div class="question5" id="">
                        <h2 class="d-inline-block">Q8 ．</h2>
                        <p class="d-inline"> 39℃以上の熱が出た事がありますか？　</p>
                        <div class="question4-radio">
                            <div class="form-check-inline">
                                <label class="form-check-label">
                                <input type="radio" class="form-check-input" value='yes' name="man_question_8">はい
                                </label>
                            </div>
                            <div class="form-check-inline">
                                <label class="form-check-label">
                                <input type="radio" class="form-check-input" value='no' name="man_question_8">いいえ
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="question5" id="">
                        <h2 class="d-inline-block">Q9 ．</h2>
                        <p class="d-inline"> 性病にかかったことがありますか？　</p>
                        <div class="question4-radio" id="maleq10">
                            <div class="form-check-inline">
                                <label class="form-check-label">
                                <input type="radio" class="form-check-input" value='はい' name="man_question_9">はい
                                </label>
                            </div>
                            <div class="form-check-inline">
                                <label class="form-check-label">
                                <input type="radio" class="form-check-input" value='no' name="man_question_9">いいえ
                                </label>
                            </div>
                        </div>
                        <div class="col-md-6 pulldownsmoke" id="selectmaleQuestion10">
                            <select  name = "man_question_9.1" class="browser-default custom-select contact-select question5-select">
                                <option value="Chlamydia">クラミジア</option>
                                <option value="Gonorrhea">淋病</option>
                                <option value="Herpes">ヘルペス</option>
                                <option value="Candida">カンジダ</option>
                                <option value="Syphilis">梅毒</option>
                                <option value="Condyloma of condyloma">コンジローマのコンジローマ</option>
                                <option value="Other ()">その他（）</option>
                            </select>
                        </div>
                    </div>

                    <div class="question5" id="">
                        <h2 class="d-inline-block">Q10 ．</h2>
                        <p class="d-inline"> 睾丸を打って腫れたことがありますか？</p>
                        <div class="question4-radio">
                            <div class="form-check-inline">
                                <label class="form-check-label">
                                <input type="radio" class="form-check-input" value='はい' name="man_question_10">はい
                                </label>
                            </div>
                            <div class="form-check-inline">
                                <label class="form-check-label">
                                <input type="radio" class="form-check-input" value='いいえ' name="man_question_10">いいえ
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="question5" id="">
                        <h2 class="d-inline-block">Q11 ．</h2>
                        <p class="d-inline"> 睾丸を降ろす手術をしたことがありますか？</p>
                        <div class="question4-radio">
                            <div class="form-check-inline">
                                <label class="form-check-label">
                                <input type="radio" class="form-check-input" value='はい' name="man_question_11">はい
                                </label>
                            </div>
                            <div class="form-check-inline">
                                <label class="form-check-label">
                                <input type="radio" class="form-check-input" value='いいえ' name="man_question_11">いいえ
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="question5" id="">
                        <h2 class="d-inline-block">Q12 ．</h2>
                        <p class="d-inline"> 鼠径ヘルニア(脱腸)の手術をしたことがありますか？</p>
                        <div class="question4-radio">
                            <div class="form-check-inline">
                                <label class="form-check-label">
                                <input type="radio" class="form-check-input" value='はい' name="man_question_12">はい
                                </label>
                            </div>
                            <div class="form-check-inline">
                                <label class="form-check-label">
                                <input type="radio" class="form-check-input" value='いいえ' name="man_question_12">いいえ
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="question5" id="">
                        <h2 class="d-inline-block">Q13 ．</h2>
                        <p class="d-inline"> 睾丸の袋(陰のう)に水が溜まったことがありますか？</p>
                        <div class="question4-radio">
                            <div class="form-check-inline">
                                <label class="form-check-label">
                                <input type="radio" class="form-check-input" value='はい' name="man_question_13">はい
                                </label>
                            </div>
                            <div class="form-check-inline">
                                <label class="form-check-label">
                                <input type="radio" class="form-check-input" value='いいえ' name="man_question_13">いいえ
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="question5" id="">
                        <h2 class="d-inline-block">Q14 ．</h2>
                        <p class="d-inline">次の病気でなったことがあるものを選択　</p>
                        <div class="form-check-inline">
                            <label class="form-check-label">
                                <input type="checkbox" class="form-check-input" value='結核' name="man_question_14[]" >結核
                            </label>
                        </div>
                        <div class="form-check-inline">
                            <label class="form-check-label">
                                <input type="checkbox" class="form-check-input" value='糖尿病' name="man_question_14[]">糖尿病
                            </label>
                        </div>
                        <div class="form-check-inline">
                            <label class="form-check-label">
                                <input type="checkbox" class="form-check-input"  value='おたふく風邪'  name="man_question_14[]">おたふく風邪
                            </label>
                        </div>
                    </div>
                    <div class="question5" id="">
                        <h2 class="d-inline-block">Q15 .</h2>
                        <p class="d-inline"> ステロイドホルモンまたは精神科の薬を使った事がありますか？</p>
                        <div class="question4-radio">
                            <div class="form-check-inline">
                                <label class="form-check-label">
                                <input type="radio" class="form-check-input" value='はい' name="man_question_15">はい
                                </label>
                            </div>
                            <div class="form-check-inline">
                                <label class="form-check-label">
                                <input type="radio" class="form-check-input" value='いいえ' name="man_question_15">いいえ
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="question5" id="">
                        <h2 class="d-inline-block">Q16 .</h2>
                        <p class="d-inline"> 大豆アレルギーはありますか？</p>
                        <div class="question4-radio">
                            <div class="form-check-inline">
                                <label class="form-check-label">
                                <input type="radio" class="form-check-input" value='はい' name="man_question_16">はい
                                </label>
                            </div>
                            <div class="form-check-inline">
                                <label class="form-check-label">
                                <input type="radio" class="form-check-input" value='いいえ' name="man_question_16">いいえ
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="question5" id="">
                        <h2 class="d-inline-block">Q17 .</h2>
                        <p class="d-inline"> 薬剤や食べ物で症状(アレルギー等)が出た事はありますか？</p>
                        <div class="question4-radio" id="maleq19doubleCheckWrapper">
                            <div class="form-check-inline">
                                <label class="form-check-label">
                                <input type="radio" class="form-check-input" value='はい' name="man_question_17">はい
                                </label>
                            </div>
                            <div class="form-check-inline">
                                <label class="form-check-label">
                                <input type="radio" class="form-check-input" value='いいえ' name="man_question_17">いいえ
                                </label>
                            </div>
                        </div>
                        <div class="double-checkbox-wrapper" id="maleq19doubleCheck">
                            <p>原因物質</p>
                            <div class="form-check form-check-first-wrapper">
                                <div class="form-check-inline">
                                    <label class="form-check-label">
                                    <input type="checkbox" class="form-check-input" value='卵' name="man_question_17.1[]" >卵
                                    </label>
                                </div>
                                <div class="form-check-inline">
                                    <label class="form-check-label">
                                    <input type="checkbox" class="form-check-input" value='乳' name="man_question_17.1[]" >乳
                                    </label>
                                </div>
                                <div class="form-check-inline">
                                    <label class="form-check-label">
                                    <input type="checkbox" class="form-check-input" value='小麦' name="man_question_17.1[]" >小麦
                                    </label>
                                </div>
                                <div class="form-check-inline">
                                    <label class="form-check-label">
                                    <input type="checkbox" class="form-check-input" value='そば' name="man_question_17.1[]" >そば
                                    </label>
                                </div>
                                <div class="form-check-inline">
                                    <label class="form-check-label">
                                    <input type="checkbox" class="form-check-input" value='落花生' name="man_question_17.1[]" >落花生
                                    </label>
                                </div>
                                <div class="form-check-inline">
                                    <label class="form-check-label">
                                    <input type="checkbox" class="form-check-input" value='えび' name="man_question_17.1[]" >えび
                                    </label>
                                </div>
                                <div class="form-check-inline">
                                    <label class="form-check-label">
                                    <input type="checkbox" class="form-check-input" value='かに' name="man_question_17.1[]" >かに
                                    </label>
                                </div>
                            </div>
                            <p> 症状 </p>
                            <div class="form-check form-check-second-wrapper">
                                <div class="form-check-inline">
                                    <label class="form-check-label">
                                    <input type="checkbox" class="form-check-input" value='かゆみ' name="man_question_17.2[]" >かゆみ
                                    </label>
                                </div>
                                <div class="form-check-inline">
                                    <label class="form-check-label">
                                    <input type="checkbox" class="form-check-input" value='じんましん' name="man_question_17.2[]" >じんましん
                                    </label>
                                </div>
                                <div class="form-check-inline">
                                    <label class="form-check-label">
                                    <input type="checkbox" class="form-check-input" value='赤くなる' name="man_question_17.2[]" >赤くなる
                                    </label>
                                </div>
                                <div class="form-check-inline">
                                    <label class="form-check-label">
                                    <input type="checkbox" class="form-check-input" value='下痢' name="man_question_17.2[]" >下痢
                                    </label>
                                </div>
                                <div class="form-check-inline">
                                    <label class="form-check-label">
                                    <input type="checkbox" class="form-check-input" value='吐き気' name="man_question_17.2[]" >吐き気
                                    </label>
                                </div>
                                <div class="form-check-inline">
                                    <label class="form-check-label">
                                    <input type="checkbox" class="form-check-input" value='くしゃみ' name="man_question_17.2[]" >くしゃみ・せき
                                    </label>
                                </div>
                                <div class="form-check-inline">
                                    <label class="form-check-label">
                                    <input type="checkbox" class="form-check-input" value='せき' name="man_question_17.2[]" >鼻づまり・鼻水
                                    </label>
                                </div>
                                <div class="form-check-inline">
                                    <label class="form-check-label">
                                    <input type="checkbox" class="form-check-input" value='アナフィラキシーショック' name="man_question_17.2[]" >アナフィラキシーショック	
                                    </label>
                                </div>
                                <div class="form-check-inline">
                                    <label class="form-check-label">
                                    <input type="checkbox" class="form-check-input" value='チアノーゼ' name="man_question_17.2[]" >チアノーゼ
                                    </label>
                                </div>
                                <div class="form-check-inline">
                                    <label class="form-check-label">
                                    <input type="checkbox" class="form-check-input" value='血圧低下' name="man_question_17.2[]" >血圧低下
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="question5" id="">
                        <h2 class="d-inline-block">Q18 .</h2>
                        <p class="d-inline">タバコを吸いますか？</p>
                        <div class="question4-radio" id="male20Wrapper">
                            <div class="form-check-inline">
                                <label class="form-check-label">
                                <input type="radio" class="form-check-input" value='はい' name="man_question_18">はい
                                </label>
                            </div>
                            <div class="form-check-inline">
                                <label class="form-check-label">
                                <input type="radio" class="form-check-input" value='いいえ' name="man_question_18">いいえ
                                </label>
                            </div>
                        </div>
                        <div class="col-md-6 pulldownsmoke" id="male20Pulldown">
                            <p>本 :</p>
                            <select  name = "man_question_18.1" class="browser-default custom-select contact-select question5-select">
                                <option></option>
                                <option value="1">1</option>
                                <option value="2">2</option>
                                <option value="3">3</option>
                                <option value="4">4</option>
                                <option value="5">5</option>
                                <option value="6">6</option>
                                <option value="7">7</option>
                                <option value="8">8</option>
                                <option value="9">9</option>
                                <option value="10">10</option>
                                <option value="11">11</option>
                                <option value="12">12</option>
                                <option value="13">13</option>
                                <option value="14">14</option>
                                <option value="15">15</option>
                                <option value="16">16</option>
                                <option value="17">17</option>
                                <option value="18">18</option>
                                <option value="19">19</option>
                                <option value="20">20</option>
                                <option value="21">21</option>
                                <option value="22">22</option>
                                <option value="23">23</option>
                                <option value="24">24</option>
                                <option value="25">25</option>
                                <option value="26">26</option>
                                <option value="4">27</option>
                                <option value="4">28</option>
                                <option value="4">29</option>
                                <option value="4">more than 30</option>
                            </select>
                        </div>

                    </div>
                    <div class="question5" id="">
                        <h2 class="d-inline-block">Q19 .</h2>
                        <p class="d-inline">お酒を飲みますか？</p>
                        <div class="question4-radio" id="male21Wrapper">
                            <div class="form-check-inline">
                                <label class="form-check-label">
                                <input type="radio" class="form-check-input" value='はい' name="man_question_19">はい
                                </label>
                            </div>
                            <div class="form-check-inline">
                                <label class="form-check-label">
                                <input type="radio" class="form-check-input" value='いいえ' name="man_question_19">いいえ
                                </label>
                            </div>
                        </div>
                        <div class="col-md-6 pulldownsmoke" id="male21Pulldown">
                            <p>週　回:</p>
                            <select  name = "man_question_19.1" class="browser-default custom-select contact-select question5-select">
                                <option></option>
                                <option value="1">1</option>
                                <option value="2">2</option>
                                <option value="3">3</option>
                                <option value="4">4</option>
                                <option value="5">5</option>
                                <option value="6">6</option>
                                <option value="7">7</option>
                            </select>
                        </div>

                    </div>
                    <div class="question5" id="">
                        <h2 class="d-inline-block">Q20 .</h2>
                        <p class="d-inline">食欲はありますか？</p>
                        <div class="question4-radio">
                            <div class="form-check-inline">
                                <label class="form-check-label">
                                <input type="radio" class="form-check-input" value='とてもある' name="man_question_20">とてもある
                                </label>
                            </div>
                            <div class="form-check-inline">
                                <label class="form-check-label">
                                <input type="radio" class="form-check-input" value='ある' name="man_question_20">ある
                                </label>
                            </div>
                            <div class="form-check-inline">
                                <label class="form-check-label">
                                <input type="radio" class="form-check-input" value='普通' name="man_question_20">普通
                                </label>
                            </div>
                            <div class="form-check-inline">
                                <label class="form-check-label">
                                <input type="radio" class="form-check-input" value='あまりない' name="man_question_20">あまりない
                                </label>
                            </div>
                            <div class="form-check-inline">
                                <label class="form-check-label">
                                <input type="radio" class="form-check-input" value='全くない' name="man_question_20">全くない
                                </label>
                            </div>
                            <div class="form-check-inline">
                                <label class="form-check-label">
                                <input type="radio" class="form-check-input" value='その他' name="man_question_20">その他(　　)
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="question5" id="">
                        <h2 class="d-inline-block">Q21 .</h2>
                        <p class="d-inline">睡眠は良好ですか？</p>
                        <div class="question4-radio">
                            <div class="form-check-inline">
                                <label class="form-check-label">
                                <input type="radio" class="form-check-input" value='とても良好' name="man_question_21">とても良好
                                </label>
                            </div>
                            <div class="form-check-inline">
                                <label class="form-check-label">
                                <input type="radio" class="form-check-input" value='良好' name="man_question_21">良好
                                </label>
                            </div>
                            <div class="form-check-inline">
                                <label class="form-check-label">
                                <input type="radio" class="form-check-input" value='普通' name="man_question_21">普通
                                </label>
                            </div>
                            <div class="form-check-inline">
                                <label class="form-check-label">
                                <input type="radio" class="form-check-input" value='やや不眠' name="man_question_21">やや不眠
                                </label>
                            </div>
                            <div class="form-check-inline">
                                <label class="form-check-label">
                                <input type="radio" class="form-check-input" value='全く眠れない' name="man_question_21">全く眠れない
                                </label>
                            </div>
                            <div class="form-check-inline">
                                <label class="form-check-label">
                                <input type="radio" class="form-check-input" value='その他' name="man_question_21">その他(　　)
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="question5" id="">
                        <h2 class="d-inline-block">Q22 .</h2>
                        <p class="d-inline">便通は良好ですか？</p>
                        <div class="question4-radio">
                            <div class="form-check-inline">
                                <label class="form-check-label">
                                <input type="radio" class="form-check-input" value='とても良好' name="man_question_22">とても良好
                                </label>
                            </div>
                            <div class="form-check-inline">
                                <label class="form-check-label">
                                <input type="radio" class="form-check-input" value='良好' name="man_question_22">良好
                                </label>
                            </div>
                            <div class="form-check-inline">
                                <label class="form-check-label">
                                <input type="radio" class="form-check-input" value='普通' name="man_question_22">普通
                                </label>
                            </div>
                            <div class="form-check-inline">
                                <label class="form-check-label">
                                <input type="radio" class="form-check-input" value='あまり良くない' name="man_question_22">あまり良くない
                                </label>
                            </div>
                            <div class="form-check-inline">
                                <label class="form-check-label">
                                <input type="radio" class="form-check-input" value='悪い' name="man_question_22">悪い
                                </label>
                            </div>
                            <div class="form-check-inline">
                                <label class="form-check-label">
                                <input type="radio" class="form-check-input" value='その他(自由入力)' name="man_question_22">その他(自由入力)
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="question5" id="">
                        <h2 class="d-inline-block">Q23 .</h2>
                        <p class="d-inline">入院や手術を伴うような病気(高血圧や喘息含む)や手術経験はありますか？</p>
                        <div class="question4-radio" id="male26Wrapper">
                            <div class="form-check-inline">
                                <label class="form-check-label">
                                <input type="radio" class="form-check-input" value='はい' name="man_question_23">はい
                                </label>
                            </div>
                            <div class="form-check-inline">
                                <label class="form-check-label">
                                <input type="radio" class="form-check-input" value='いいえ' name="man_question_23">いいえ
                                </label>
                            </div>
                        </div>
                        <div class="inputWrapperq26">
                            <input type="text" id="name" name="man_question_23.1" value="" class="form-control" placeholder="(病名 自由記入)">
                        </div>
                    </div>
                    <div class="question5" id="">
                        <h2 class="d-inline-block">Q24 .</h2>
                        <p class="d-inline">入院や手術を伴うような病気(高血圧や喘息含む)や手術経験はありますか？</p>
                        <div class="question4-radio" id="maleq27Wrapper">
                            <div class="form-check-inline">
                                <label class="form-check-label">
                                <input type="radio" class="form-check-input" value='はい' name="man_question_24">はい
                                </label>
                            </div>
                            <div class="form-check-inline">
                                <label class="form-check-label">
                                <input type="radio" class="form-check-input" value='いいえ' name="man_question_24">いいえ
                                </label>
                            </div>
                        </div>
                        <div class="inputWrapperq27">
                            <input type="text" id="name" name="man_question_24.1" value="" class="form-control" placeholder="（病名　自由入力）">
                        </div>
                    </div>
                    <div class="question5" id="">
                        <h2 class="d-inline-block">Q25 .</h2>
                        <p class="d-inline">病院で処方されている薬はありますか？</p>
                        <div class="question4-radio">
                            <div class="form-check-inline">
                                <label class="form-check-label">
                                <input type="radio" class="form-check-input" value='はい（薬の名前・自由入力）' name="man_question_25">はい（薬の名前・自由入力）
                                </label>
                            </div>
                            <div class="form-check-inline">
                                <label class="form-check-label">
                                <input type="radio" class="form-check-input" value='いいえ' name="man_question_25">いいえ
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="question5" id="maleq28Wrapper">
                        <h2 class="d-inline-block">Q26 .</h2>
                        <p class="d-inline">下記のサプリメントを服用していますか？</p>
                        <div class="question4-radio">
                            <div class="form-check-inline">
                                <label class="form-check-label">
                                <input type="radio" class="form-check-input" value='はい（薬の名前・自由入力）' name="man_question_26">はい
                                </label>
                            </div>
                            <div class="form-check-inline">
                                <label class="form-check-label">
                                <input type="radio" class="form-check-input" value='いいえ' name="man_question_26">いいえ
                                </label>
                            </div>
                        </div>
                        <div class="question_male29_wrapper">
                            <div class="form-check-inline">
                                <label class="form-check-label">
                                    <input type="checkbox" class="form-check-input" name="man_question_26.1[]" value="ビタミンD">ビタミンD
                                </label>
                            </div>
                            <div class="form-check-inline">
                                <label class="form-check-label">
                                    <input type="checkbox" class="form-check-input" name="man_question_26.1[]" value="DHEA">DHEA
                                </label>
                            </div>
                            <div class="form-check-inline">
                                <label class="form-check-label">
                                    <input type="checkbox" class="form-check-input" name="man_question_26.1[]" value="葉酸">葉酸
                                </label>
                            </div>
                            <div class="form-check-inline">
                                <label class="form-check-label">
                                    <input type="checkbox" class="form-check-input" name="man_question_26.1[]" value="イノシトール">イノシトール
                                </label>
                            </div>
                            <div class="form-check-inline">
                                <label class="form-check-label">
                                    <input type="checkbox" class="form-check-input" name="man_question_26.1[]"  value="亜鉛">亜鉛
                                </label>
                            </div>
                        </div>

                    </div>
                    <div class="question5" id="maleq29Wrapper">
                        <h2 class="d-inline-block">Q27 .</h2>
                        <p class="d-inline">★ご質問などあれば、自由にお書き下さい。(2000文字)</p>
                        <div class="form-group maleq29textarea">
                            <textarea class="form-control" rows="5" id="comment" name="man_question_27" ></textarea>
                        </div>
                    </div>
                    <a type="" class="btn btn-custom-cons text-center" id="nextConsultBtn" onclick="myFunction()">次へ</a>
                    <div id="basicInfoBlock">
                        <div class="question5 basicInfoWrapper">
                            <h2>【基本情報】</h2>
                            <p class="d-inline">ご相談のきっかけ</p>
                            <span>必須</span>
                            <div class="question4-radio">
                                <div class="form-check-inline">
                                    <label class="form-check-label">
                                    <input type="radio" class="form-check-input" value='Google 検索' name="source">Google 検索
                                    </label>
                                </div>
                                <div class="form-check-inline">
                                    <label class="form-check-label">
                                    <input type="radio" class="form-check-input" value='Yahoo検索' name="source">Yahoo検索
                                    </label>
                                </div>
                                <div class="form-check-inline">
                                    <label class="form-check-label">
                                    <input type="radio" class="form-check-input" value='ブログ' name="source">ブログ
                                    </label>
                                </div>
                                <div class="form-check-inline">
                                    <label class="form-check-label">
                                    <input type="radio" class="form-check-input" value='友人・知人からの紹介' name="source">友人・知人からの紹介
                                    </label>
                                </div>
                                <div class="form-check-inline">
                                    <label class="form-check-label">
                                    <input type="radio" class="form-check-input" value='その他(　　　 )' name="source">その他(　　　 )
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="md-form">
                            <i class="fas fa-user-ninja contact-icon-color prefix"></i>
                            <input type="text"  name="name" value="" class="form-control">
                            <label for="name">氏名（ニックネーム可）</label>
                        </div>
                        <div class="row basic-radio-wrapper">
                            <div class="col-md-6">
                                <div class="md-form <?= hasError('sex')? "has-danger":''?>">
                                <div class="left-icon-title">
                                    <div class="genter-title">性別</div>
                                    <i class="fa fa-transgender <?= hasError('sex')? "danger-icon":'contact-icon-color'?> icon-custom"></i>
                                </div>
                                <!-- Default inline 1-->
                                <div class="custom-radio-wrapper">
                                    <div class="custom-control custom-radio custom-control-inline profile-radio-wrap">
                                    <input type="radio"  value= '女性' class="custom-control-input" id="sex" name="sex">
                                    <label class="custom-control-label" for="sex">男性</label>
                                    </div>
                                    <!-- Default inline 2-->
                                    <div class="custom-control custom-radio custom-control-inline profile-radio-wrap">
                                    <input type="radio"  value= '男性' class="custom-control-input" id="defaultInline2" name="sex" >
                                    <label class="custom-control-label" for="defaultInline2">女性</label>
                                    </div>
                                </div>
                                </div>
                                <div class="form-control-feedback feedback" id="fed_sex"><?= showError('sex') ?></div>
                            </div>
                            <div class="col-md-6">
                                <div class="md-form <?= hasError('age')? "has-danger":''?>">
                                    <div class="left-icon-title">
                                        <div class="genter-title">年齢</div>
                                    <!-- <i class="fas fa-calendar <?= hasError('age')? "danger-icon":'contact-icon-color'?> icon-custom"></i> -->
                                    </div>
                                    <select  name = "age" class="browser-default custom-select contact-select">
                                        <option value="30 歳未満">30 歳未満</option>
                                        <option value="30〜39 歳">30〜39 歳</option>
                                        <option value="40〜49 歳">40〜49 歳</option>
                                        <option value="50 歳以上">50 歳以上</option>
                                    </select>
                                </div>
                                <div class="form-control-feedback feedback" id="fed_question"><?= showError('age') ?></div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="genter-title">職業</div>
                                <select  name = "profession" class="browser-default custom-select contact-select">
                                    <option value="主婦・主夫">主婦・主夫</option>
                                    <option value="専門・技術職">専門・技術職</option>
                                    <option value="管理職">管理職</option>
                                    <option value="事務職">事務職</option>
                                    <option value="販売職">販売職</option>
                                    <option value="サービス職">サービス職</option>
                                    <option value="運輸・通信的職種">運輸・通信的職種</option>
                                    <option value="保安的職種">保安的職種</option>
                                    <option value="技能・労務職">技能・労務職</option>
                                    <option value="農・林・漁業">農・林・漁業</option>
                                    <option value="その他（　　　　）">その他（　　　　）</option>
                                </select>
                            </div>
                            <div class="col-md-6">
                                <p class="genter-title">販売職</p>
                                <input type="checkbox" name="Sales_position" value="check" class="agree-check" id="agree" />プライバシーポリシー（確認後後送します）

                            </div>
                        </div>
                        <div class="row">
                            <div class="consult-btn-wrap text-center consult-end-consult-btn">
                                <button type="submit" href="<?=url('thankyou')?>" class="btn btn-custom-cons text-center">プライバシーポリシーに同意して送信する</button>
                                <!-- <button type="submit" class="btn btn-custom-cons text-center">問合せ先メールアドレス</button> -->
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>