<section class="top-section">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <?= image('img/consultation-profile.png','common-img','consprofile-image')?>
            </div>
        </div>
        <div class="row">
            <div class="content-container col-md-10 offset-md-1">
                <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="#">TOP </a></li>
                <li class="breadcrumb-item active">》 WEB問診票「無料相談</li>
                </ol>
                <p class="text-center">無料相談（サプリメント外来問診）にご回答いただきありがとうございました。<br>
                    ご登録のメールアドレスに、無料相談の結果をお届けしました。<br>1日たっても届かない場合は、こちらまでお問い合わせください。
                </p>
            </div>
        </div>
        <a  role="button" href="<?= url('') ?>" class="btn btn-custom-cons btn-thankyou text-center">トップに戻る     <i class="fas fa-chevron-right"></i></a>
    </div>
</section>