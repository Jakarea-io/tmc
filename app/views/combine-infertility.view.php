<div class="container columnunder_container">
	<div class="row">
		<div class="col-md-12">
			<div class="content-container">
				<ol class="breadcrumb">
					<li class="breadcrumb-item"><a href="<?php url('')?>">TOP  <i class="fas fa-angle-right breadcumb-icon"></i> </a></li>
					<li class="breadcrumb-item"><a href="<?php url('column')?>">コラム <i class="fas fa-angle-right breadcumb-icon"></i></a></li>
					<li class="breadcrumb-item active">  男性不妊と女性不妊</li>
				</ol>
			</div>
		</div>
    </div>
    <div class="row">
		<div class="col-md-12">
			<div class="col-wrapper">
				<div class="col-under-firstone">
					<?= image('img/col-under-top.png','common-img colunder-top1-web','col-under-top')?>
					<?= image('img/colunder-top-mobile1.png','common-img colunder-top-mobile1','colunder-top-mobile1')?>
					<div class="colunder-top1-contentwrapper">
						<div class="col-under-top1-wrapper">
							<?= image('img/col-under-top1-arrow.png','','col-under-top1-arrow')?>
							<h5 class="colunder-arrow-text">目からウロコの</h5>
							
						</div>
						<h3>不妊治療<span class="col-top1-span">なるほどコラム</span></h3>
					</div>
				</div>
				<div class="col-under-secondone">
					<?= image('img/combine-infertility-web.png','common-img colunder-top-mobile2web','combine-infertility-web')?>
                    <?= image('img/combine-top-mobile.png','common-img colunder-top-mobile2mobile','combine-infertility-mobile')?>
                    <div class="media colunder-common-media inconvenient-media">
						<div class="col-common-pink-circle">Column</div>
						<div class="media-body">
							<div class="common-mid-text-banner">
								<h4>男性不妊と女性不妊</h4>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-9 col-sm-12">
			<div class="column-under-top-list-wrapper">
				<div class="line-wrapper">
					<h3 class="column-under-list-title blue-text-common">目次</h3>
				</div>
				<ul class="columnunder-bottom-before-list">
					<li><a href="#infertile-couples-are-also-attributable">不妊カップルの48%は男性側にも原因あり</a></li>
					<li><a href="#there-are-no-one-in-32-prefectures">32県に1人もいない。男性不妊の生殖医療専門医</a></li>
                    <li><a href="#causes-of-male-infertility-with-low-sperm">「精子が少ない」男性不妊原因の80%は体質改善で解決可能</a></li>
                    <li><a href="#british-infertility-program">男性も女性も一緒に参加する英国式不妊治療プログラム</a></li>
				</ul>
			</div>
			
            <div class="row column-under-text-1" id="infertile-couples-are-also-attributable">
				<div class="col-md-12">
                    <div class="column-bottom-before-title" >
                        <a href="">不妊カップルの48%は男性側にも原因あり</a>
                    </div>
                    <div class="media arrange-order">
						<div class="col-under-media-text-container one">
							<p>WHOの報告によれば、男性にのみ不妊原因があるカップルは全体の24%、女性にのみ不妊原因があるカップルが41%、男女ともに原因があるカップルが24%、原因不明が11%とされています。</p>
                            <p>つまり、<span class="yellow-bg">不妊カップルの約半数において、男性側にも原因がある</span>事になります。</p>
                            <p>しかし、<span class="common-bold-black">不妊専門クリニックには、「男性不妊外来」を併設しているクリニックが極めて少ないのです。</span></p>
						</div>
						<div class="media-body">
							<?= image('img/combine-media1.png','common-responsive-img','cost-media1.png')?>
						</div>
                    </div>
                 </div>
            </div>

            <div class="row column-under-text-1" id="there-are-no-one-in-32-prefectures">
				<div class="col-md-12">
				    <div class="column-bottom-before-title"><a href="">32県に1人もいない。男性不妊の生殖医療専門医</a></div>
					<div class="media cost-of-media-wrap">
						<?= image('img/combine-media2.png','common-responsive-img','combine-media2')?>
						<div class="media-body common-media-body">
							<p>男性不妊の治療は、泌尿器科での受診になったりするのですが、日本生殖医学会が認定する生殖医療専門医は、2017年4月時点で649人。<br>
                                ほとんどが産婦人科医で、泌尿器科医はわずか51人しか居ません。</p>
                            <p>また、地域の偏在も問題です。<br>
                            <span class="yellow-bg">	生殖医療専門医は東京に９人、神奈川に６人、大阪に１０人と半数が都市部に集中しており、１人もいない自治体が３２県に上ります。</span></p>
						</div>
					</div>
					<br/>この結果、男性不妊治療専門の施設が少なく、ほとんどの不妊治療専門施設では産婦人科医のみが診察、精液検査しか行われないことも少なくないのが現状なのです。
					<p>(産経ニュース： <a href=" https://www.sankei.com/premium/news/171113/prm1711130007-n1.html" class="hyperlink-cost">https://www.sankei.com/premium/news/171113/prm1711130007-n1.html)</a></p>
				</div>
            </div>
            
            <div class="row column-under-text-1" id="causes-of-male-infertility-with-low-sperm">
				<div class="col-md-12">
                    <div class="column-bottom-before-title">
                        <a href="">「精子が少ない」男性不妊原因の80%は体質改善で解決可能</a>
                    </div>
                    <div class="media arrange-order">
						<div class="col-under-media-text-container one">
							<p>また、2016年に公表された全国調査の結果では、男性不妊原因の内訳は、造精機能障害が82.4%、精路通過障害が3.9%、性機能障害が13.5%でした。</p>

                            <p>造精機能障害とは、要するに「精子がたくさん作れない」事を指します。</p>

                            <p>つまり、<strong><span class="red-text"> <b>男性の体質改善によってたくさんの精子が作られるようになれば、男性不妊の80%以上が解決し、自然妊娠が可能になるのです。</b></span></strong></p>
						</div>
						<div class="media-body">
							<?= image('img/combine-media3.png','common-responsive-img','cpmbine-media3')?>
						</div>
                    </div>
                    <p>しかし、男性側は精子を1匹でも提供できれば、造精機能障害の治療を行わなくても、体外・顕微授精が可能な上に、前述の通り、男性不妊の専門医が極端に少ない為、根本的に男性不妊を解決し、自然妊娠を目指すカップルは少数派です。</p>

                    <p>現在、約1%の日本人男性が、精子を1匹も提供できない「無精子症」と言われています。</p>

                    <p>この1%の方々に限っては、基本、精子の通過路を開通させる手術か、顕微鏡下精巣内精子採取術が必要になります。</p>

                    <p>けれども、<span class="common-bold-black">82.4%の男性は、自然妊娠を実現するには精子の数が「少ない」だけですので、結局、男性側の体質改善を試みずに、女性が体外・顕微授精を受ける事になりやすいのです。</span><br>
                    (※ちなみに、たとえ「無精子症」だったとしても、射精は可能ですし精液も通常通り排出します。)</p>

                    <p>ただ、排出された精液の中に、精子が居ないのですが、これは検査をしなければ判明しない為、男性が自覚症状を得る事はありません)
                    <br/>(日本メンズヘルス医学会：<a href="http://www.mens-health.jp/364" class="common-anchor">http://www.mens-health.jp/364)</a><br/>
                    体外・顕微授精を行う場合、女性は服薬や自己注射で薬漬けになり、採卵や移植で何度も通院しなければなりません。</p>

                    <p>しかし、男性はそのような投薬等必要なく、精子を提供するのみでOKです。</p>
                    <p class="common-bold-black">女性側からすれば、自分ばかり負担が大きく、不公平に感じられるかもしれませんが、たとえ不妊原因の男女比がほぼ等しくても、これが日本の不妊治療現場の実状なのです。</p>
                 </div>
            </div>

            <div class="row column-under-text-1" id="british-infertility-program">
				<div class="col-md-12">
				    <div class="column-bottom-before-title"><a href="">男性も女性も一緒に参加する英国式不妊治療プログラム</a></div>
					<div class="media cost-of-media-wrap">
						<?= image('img/combine-media4.png','common-responsive-img','combine-media4')?>
						<div class="media-body common-media-body">
						<p>	<span class="yellow-bg">一方、</span><span class="common-bg-yellowred">日本初上陸にして、日本唯一の妊娠率89%英国式療法</span>とは、</p>
                            <p class="common-bold-black">女性だけが治療を受ける事は絶対にありません。</p>
                            <p class="common-bold-black">男性だけが治療を受ける事もありません。</p>
                            <p class="common-bold-black">必ず、夫婦揃って参加するプログラムです。</p>
						</div>
					</div>
					<p class="">そもそも、先ほど、「男性は精子が1匹でも提供できれば体外受精が可能」と述べましたが、それは体外受精が可能なだけであって、体外受精＝妊娠成立ではありません。</p>
                    <p>他のコラムでもお伝えしましたように、<span class="common-bold-black">体外受精から出産へ至る確率は、11%程度</span>なのです。</p>

				</div>
            </div>
            
		











			
			
			
			<div class="row column-under-text-1">
				<div class="col-md-12">
					
					<p><span class="common-bold-black">不妊治療や巷の「妊活」には、様々なアプローチ法が存在しますが、私は、不妊治療は「<span class="red-text">妊娠率(分娩率)</span>」と「<span class="red-text">安全性</span>」で選ぶべきだと考えております。</span></p>
					<p class="common-bg-yellowred">だからこそ、外科的な処置が一切伴わない、妊娠率89%を達成した英国式療法をお勧めしております。</p>
					<p class="column-after-red-text ml-3">※この治療法は、患者様の症状に合わせて最適な処方が異なります。<br>もしご自身に合った治療法をお知りになりたい場合は、下記の無料相談(問診)を通して<br>お問い合せ下さい。</p>
	　			</div>
			</div>
			


            <div class="dark-red-button red-button-common">
				<a href="http://projects.cloudproduction.co.bd/tmclinic/consultation" type="button" class="btn btn-lg btn-red-custom waves-effect waves-light">不妊治療無料相談はこちら</a>
			</div>
			<div class="ceo-bottom-container">
				<div class="line-wrapper">
					<h3 class="column-under-list-title blue-text-common">監修</h3>
				</div>
				<div class="colunder-bottommedia-container">
					<div class="media">
						<div class="col-under-media-text-container">
							<?= image('img/ceo1.png','','ceo')?>
						</div>
						<div class="media-body">
							<p class="blue-text-common ceo-right-text">外来部長 <span class="pt-up-font">山下明子先生</span><span>医学博士、内科医、神経内科専門医、抗加齢医学専門医</span></p>
						</div>
					</div>
					<div class="media">
						<div class="col-under-media-text-container">
							<?= image('img/ceo2.png','','ceo2')?>
						</div>
						<div class="media-body">
							<p class="blue-text-common ceo-right-text">顧問医師 <span class="pt-up-font">身原正哉先生</span><span>産婦人科専門医・日本抗加齢医学会専門医</span></p>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-3 colunder-right-section">
			<?php view('common/ceo') ?>
		</div>
	</div>
</div>
