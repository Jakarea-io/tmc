<div class="container columnunder_container">
	<div class="row">
		<div class="col-md-12">
			<div class="content-container">
				<ol class="breadcrumb">
					<li class="breadcrumb-item"><a href="#a7">TOP  <i class="fas fa-angle-right breadcumb-icon"></i> </a></li>
					<li class="breadcrumb-item">コラム <i class="fas fa-angle-right breadcumb-icon"></i></li>
					<li class="breadcrumb-item active"> 不妊治療ステップアップの先の負担</li>
				</ol>
			</div>
		</div>
    </div>
    <div class="row">
		<div class="col-md-12">
			<div class="col-wrapper">
				<div class="col-under-firstone">
					<?= image('img/col-under-top.png','common-img colunder-top1-web','col-under-top')?>
					<?= image('img/colunder-top-mobile1.png','common-img colunder-top-mobile1','colunder-top-mobile1')?>
					<div class="colunder-top1-contentwrapper">
						<div class="col-under-top1-wrapper">
							<?= image('img/col-under-top1-arrow.png','','col-under-top1-arrow')?>
							<h5 class="colunder-arrow-text">目からウロコの</h5>
							
						</div>
						<h3>不妊治療<span class="col-top1-span">なるほどコラム</span></h3>
					</div>
				</div>
				<div class="col-under-secondone">
					<?= image('img/absence-of-infertility-web.png','common-img colunder-top-mobile2web','absence-of-infertility-web')?>
                    <?= image('img/absence-of-infertility-mobile.png','common-img colunder-top-mobile2mobile','absence-of-infertility-mobile')?>
                    <div class="media colunder-common-media inconvenient-media">
						<div class="col-common-pink-circle">Column</div>
						<div class="media-body">
							<div class="common-mid-text-banner">
								<h4>不妊治療の不条理</h4>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-9 col-sm-12">
			<div class="column-under-top-list-wrapper">
				<div class="line-wrapper">
					<h3 class="column-under-list-title blue-text-common">目次</h3>
				</div>
				<ul class="columnunder-bottom-before-list">
					<li><a href="#a1">日本の不妊治療の定番は「ステップアップ」だが</a></li>
					<li><a href="#a2">実施のタイミングが大事なヒューナーテスト</a></li>
					<li><a href="#a3">クリニックの都合が優先される不条理な日本の「体外受精」</a></li>
				</ul>
			</div>
			<div class="column-bottom-before-title mobile-br">
				<a href="#a4">日本の不妊治療の定番は<br/>「ステップアップ」だが</a>
			</div>
			
            
			<div class="row column-under-text-1 incon-truth-second-wrapper cost-of-middle-container">
                <div class="col-md-12">
					<p><span class="common-bold-black">私は、日本では不妊治療の定番となっている「ステップアップ」自体に疑問を持っています。</span><br/>
					※私だけではなく、中には最初から体外受精しか勧めないクリニックなど、ステップアップに否定的な見解を持つクリニックもあります。<br>
					ここではあくまでも、一般論(多数派)についてお話しています。</p>
					
					<p>日本のステップアップ不妊治療では、</p>

					<div class="absence-under-arrow-wrap">
                    	<div class="absence-bottom-arrow common-bold-black">タイミング法で妊娠しなければ、人工授精</div>
						<div class="absence-bottom-arrow common-bold-black">人工授精で妊娠しなければ、体外受精 </div>
						<div class="absence-last-no-arrow">
							<span class="common-bold-black">体外受精で妊娠しなければ、顕微授精
							</span>
							<br/><br/>(※体外受精と顕微授精は同時に行われる事もありますが)
						</div>
					</div>
					<p>と段階的に治療を進めて行きます。</p>
					<p>まず、タイミング法が有効なのは、</p>
					<ul class="circle-before">
						<li class="circle common-bold-black">これまで全く排卵日前後に性交した事がない</li>
						<li class="circle common-bold-black">月経周期が極めて不規則</li>
					</ul>
					<p>という場合のみです。</p>
					<p>基礎体温表や市販の排卵検査薬を使えば、排卵日はほぼ自分で予測できるだけでなく、何かしら不妊の原因が無い限り、精子は女性の体内で数日間生きています。<br>
						その為、排卵日から数日のズレがあったとしても、概ね排卵日付近に性交していれば、1年以内(遅くとも2年以内)には自然妊娠できるはずなのです。<br>
						そうでない場合は、<span class="common-bold-black">性交のタイミングとは別に不妊の原因があります。</span></p>
						<p>ですから、<span class="yellow-bg">先ほどお伝えしたケースに当てはまらない限りは、タイミング法は時間の無駄にしかなりかねません。</span></p>
						<p>次の段階の治療は人工授精ですが、<span class="yellow-bg">ヒューナーテストの結果が良好なのに妊娠に至れていないのであれば、人工授精も無意味です。</span><br>
						ヒューナーテストの結果が良好というのは、射精された精子が子宮まで無事届いている事を意味します。</p>
						<p>人工授精は、精子が子宮まで辿り着いてすらいない場合には助けになりますが、それ以上には妊娠成立の助けにならないのです。<br>
						</br>
							医療側が請け負っているのは、排卵のタイミングに合わせて、洗浄濃縮した精子の精鋭部隊を子宮の奥に直接注入することに限られており、精子が卵子を目指す旅の距離を、性交時と比べて10cm（子宮の長さ分）ほど短縮しているに過ぎません。<br></br>
							ですから不妊治療の現場では、人工授精までは一般不妊治療と呼び、人工授精で結果が出た場合は、自然妊娠と捉えることもあるほどなのです。</p>
				</div>
			</div>
			<div class="row column-under-text-1">
				<div class="col-md-12">
                    <div class="column-bottom-before-title mobile-br"><a href="#a5">実施のタイミングが大事な<br/>ヒューナーテスト</a></div>
                    <div class="media cost-of-media-wrap">
						<?= image('img/cost-media1.png','common-responsive-img','cost-media1')?>
						<div class="media-body common-media-body">
							<p>ヒューナーテストの結果は良好なので、自然妊娠できますよ」<br>
								と言う医師が居ますが、それよりも、<br>
								「ヒューナーテストの結果は良好なのに、なぜ自然妊娠に至れずに居るのか？」<br>
								を究明しなければなりません。</p>

							<p class="common-bold-black">排卵日付近に精子が子宮に辿り着いているにも関わらず、自然妊娠に至らないのは、受精や着床に問題がある事になります。</p>
						</div>
					</div>
					<ul class="absence-media-list-wrapper">
						<li>そもそも精子と卵子が出会っても受精していないのか？</li>
						<li>それは精子の所為なのか、卵子の所為なのか？</li>
						<li>はたまた、受精までは成立しているけれど、着床に至れていないのか？</li>
						<li>実は受精卵の着床までは上手く行っているのだが、</li>
						<li>そこから妊娠を継続できずに居るのか？</li>
						<li>(女性が妊娠を自覚する前に、化学流産を繰り返しているのか？)</li>
						<br>
					</ul>
					<p><span class="common-bold-black">これらは、体外受精をしてみなければ分かりません。</span><br>
						にもかかわらず、無意味な人工授精を何度も繰り返される患者が後を絶ちません。</p>
						<p>ヒューナーテストの結果が良好な場合、人工授精が無意味なのであれば、少なくとも、先にヒューナーテストを行ってから人工授精をすれば良いと思うのですが、人工授精が失敗してからヒューナーテストを受ける患者も珍しくありません。</p>

					<p>また、<span class="common-bg-yellowred">ヒューナーテストは、実施のタイミングが非常に重要な検査</span>です。<br>
					排卵期でない女性は頸管粘液が出にくく、単に検査する日が女性の体調とズレていただけで、結果が「不良」と出ます。<br>
					その為、何度もヒューナーテストを繰り返すと、一度「不良」という結果が出た女性でも、結果が「良好」に転じる事もあるのです。</p>

					<p>さらに、<span class="common-bold-black">人工授精1回あたりの妊娠率は、わずか5～10%</span>と言われています。<br>
					確かに、人工授精は体外受精よりは安価に受けられますが、かといって、<br>
					<span class="common-bold-black">もし人工授精では妊娠できないのであれば、1回数万円の治療を何度も繰り返すのは、無意味ではないでしょうか？</span></p>
                 </div>
			</div>


			<div class="row column-under-text-1">
				<div class="col-md-12">
					<div class="column-bottom-before-title mobile-br"><a href="#a6">クリニックの都合が優先される<br/>不条理な日本の「体外受精」</a></div>
					<div class="media arrangeorder">
						<div class="col-under-media-text-container two">
							<p>人工授精の次の段階が「体外受精」ですが、このステップでの治療方針も、不条理なケースが多いと感じます。</p>
							<p>ハッキリ言って、医師は何度も体外受精を繰り返してくれる患者の方が儲かるのですが、患者の立場からしますと、間違いなく体外受精の回数は、少なければ少ないほど良いはずです。</p>
							<p class="common-bold-black">体外受精は、1回で何十万円と費用が掛かりますし、通院回数も増え、女性の心身への負担も大きな治療法です。</p>
						</div>
						<div class="media-body">
						<?= image('img/burden-media2png.png','common-responsive-img','burden-media2png.png')?>
						</div>
					</div>
					<p>体外受精までステップアップした患者の誰もが、できることなら、1回の体外受精で出産まで至りたいと願う事でしょう。</p>

					<p>であれば、いくら検査も費用が掛かるとは言え、体外受精の費用よりは安いのですから、体外受精が失敗してしまう要因を取り除いてから、体外受精へ臨んだ方が良いに決まっています。</p>

					<p>ところが、<span class="common-bold-black">体外受精を行っている現場でも、「失敗してから検査」「ダメだったら別の方法」というフローを採っているクリニックが少なくありません。</span></p>

					<p>たとえば、体外受精をしても妊娠を継続できない不育症の患者には、先に不育症の治療を施してから体外受精を勧めるべきなのですが、体外受精が何度も失敗して初めて、不育症の検査を勧めたり。</p>

					<p>卵管に問題があると、分割胚移植では妊娠しにくい為、最初から胚盤胞移植で体外受精すれば良いものを、初回の体外受精は、分割胚移植でしか行わない方針のクリニックなど。</p>

					<p>また、他院での検査結果は認めず、全く同じ結果が出る事が分かり切っている項目でも、自院の提携クリニックで再検査しなければ治療を進めない、と患者に強要するクリニックも在ります。</p>

					<p>その上、<span class="common-bg-yellowred">苦労して体外受精までステップアップしても、約80%の患者が妊娠できない</span>のです。</p>

					<p>つくづく、日本の不妊治療は不条理で、患者側の負担が無視されていると感じざるを得ません。</p>
				</div>
			</div>
			
			
			<div class="row column-under-text-1">
				<div class="col-md-12">
					
					<p><span class="common-bold-black">不妊治療や巷の「妊活」には、様々なアプローチ法が存在しますが、私は、不妊治療は<span class="common-bg-yellowred">「妊娠率(分娩率)」</span>と<span class="common-bg-yellowred">「安全性」</span>で選ぶべきだと考えております。</span></p>
					<p class="common-bg-yellowred">だからこそ、外科的な処置が一切伴わない、妊娠率89%を達成した英国式療法をお勧めしております。</p>
					<p class="column-after-red-text">※この治療法は、患者の症状に合わせて最適な処方が異なります。<br>もしご自身に合った治療法をお知りになりたい場合は、下記の無料相談(問診)を通して<br>お問い合せ下さい。</p>
	　			</div>
			</div>
			


            <div class="dark-red-button red-button-common">
				<a href="http://projects.cloudproduction.co.bd/tmclinic/consultation" type="button" class="btn btn-lg btn-red-custom waves-effect waves-light">不妊治療無料相談はこちら</a>
			</div>
			<div class="ceo-bottom-container">
				<div class="line-wrapper">
					<h3 class="column-under-list-title blue-text-common">監修</h3>
				</div>
				<div class="colunder-bottommedia-container">
					<div class="media">
						<div class="col-under-media-text-container">
							<?= image('img/ceo1.png','','ceo')?>
						</div>
						<div class="media-body">
							<p class="blue-text-common ceo-right-text">外来部長 <span class="pt-up-font">山下明子先生</span><span>医学博士、内科医、神経内科専門医、抗加齢医学専門医</span></p>
						</div>
					</div>
					<div class="media">
						<div class="col-under-media-text-container">
							<?= image('img/ceo2.png','','ceo2')?>
						</div>
						<div class="media-body">
							<p class="blue-text-common ceo-right-text">顧問医師 <span class="pt-up-font">身原正哉先生</span><span>産婦人科専門医・日本抗加齢医学会専門医</span></p>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-3 colunder-right-section">
			<?php view('common/ceo') ?>
		</div>
	</div>
</div>