<div class="container common-pages-container">
	<div class="row top-about-bg-container">
		<div class="col-md-12 common-bg-wrapper">
			<div class="about-bg-content">
				<h2 align="center">CONTENTS AND PRICE</h2>
				<h4 align="center">診療内容と費用</h4>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="content-container col-md-10 offset-md-1 col-12">
			<ol class="breadcrumb">
				<li class="breadcrumb-item"><a href="#">TOP  <i class="fas fa-angle-right breadcumb-icon"></i> </a></li>
				<li class="breadcrumb-item active">診療内容と費用</li>
			</ol>
		</div>
	</div>
	<div class="row contents-middle">
		<div class="col-md-8 offset-md-2 contents-middle-inner">
			<a type="button" class="btn btn-lg btn-border-custom">不妊外来について</a>
			<div class="media contents-media">
				<div class="contents-media-left">
					<p>当外来の 診療内容</p>
				</div>
				<div class="media-body">
					<p>英国で89%の成功率を収めている自然な不妊治療を当院でも目指しています。</p>
					<p>日本で初めて、英国式療法で用いられているサプリメントを
						患者の症状・体質等に合わせて処方し、
						身体にやさしい不妊治療を行う日本唯一の不妊外来です。
					</p>
				</div>
			</div>
		</div>
	</div>
	
	<div class="row">
		<div class="col-md-10 offset-md-1">
			<div class="about-image-with-title contents-img-with-title" align="center">
				<?= image('img/about-ribbon.png','ribbon-image','ribbon-image')?>
				<h2>PRICE</h2>
				<h3>費用について</h3>
			</div>
			<div class="row contents-price-wrapper">
				<div class="col-md-6 content-price-left">
					<div class="content-price-left-inner">
						<?= image('img/cover.jpg','common-img','contents-price-left-image')?>
						<!-- <h3 align="center">1ヵ月分<br>15000円</h3> -->
					</div>
				</div>
				<div class="col-md-6 contents-price-right">
					<!-- <div class="card">
						<h5 class="card-header">3ヵ月コース</h5>
						<div class="card-body contents-price-right-inner">
							<ul>
								<li>
									<div class="contents-list-order">1</div>
									女性１本・男性１本
								</li>
								<li>
									<div class="contents-list-order">2</div>
									女性2本・男性１本
								</li>
							</ul>
							<ul>
								<li>
									<div class="contents-list-order">1</div>
									女性１本・男性2本
								</li>
								<li>
									<div class="contents-list-order">2</div>
									女性2本・男性2本
								</li>
							</ul>
						</div>
					</div>
					<div class="card contents-second-card">
						<h5 class="card-header">6ヵ月コース</h5>
						<div class="card-body contents-price-right-inner">
							<ul>
								<li>
									<div class="contents-list-order">1</div>
									女性１本・男性１本
								</li>
								<li>
									<div class="contents-list-order">2</div>
									女性2本・男性１本
								</li>
							</ul>
							<ul>
								<li>
									<div class="contents-list-order">1</div>
									女性１本・男性2本
								</li>
								<li>
									<div class="contents-list-order">2</div>
									女性2本・男性2本
								</li>
							</ul>
						</div>
					</div> -->
					<div class="price-right-cont">
						<h3 class="cont-text-bold">1ヶ月あたり 3～7万円<br><h3>
						<h3>※1ヶ月分の男女サプリ代含む</h3>
					</div>	
					<h6>このサイトからの購入はできません。まずは無料相談をお送りください。</h6>		

				</div>
			</div>
			<div class="row legend-wrapper">
				<div class="col-md-10 offset-md-1">
					<fieldset class="border p-2">
						<legend  class="w-auto">考えられるリスク</legend>
						<ul>
							<li>サプリメントを摂ることで、大豆アレルギーの方は、食品アレルギーの症状が出るリスクがあります。</li>
							<li>この英国式療法が採用された過去20年において、その他に報告されているリスクはありません(2019年3月現在)が、今後とも注意深く観察致します。</li>
						</ul>
					</fieldset>
					<div class="dark-red-button content-red-wrap">
						<a type="button" class="btn btn-lg btn-red-custom" href="<?=url('consultation')?>">不妊治療無料相談はこちら</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>