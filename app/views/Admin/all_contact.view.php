

<div class='container'>

<ul class="list-group">
<li class="list-group-item">

    
    <table class="table table-bordered pricing-table">
        <thead class="thead-light">
            <tr>
            <th scope="col">名前</th>
            <th scope="col">メール</th>
            <th scope="col">性別</th>
            <th scope="col">年齢</th>
            <th scope="col">質問</th>
            <th scope="col">アクション</th>
            </tr>
        </thead>
        <?php foreach($contacts as $contact) :?>
        <tbody>
            <tr>
                <td><?= $contact->name ?></td>
                <td><?= $contact->email ?></td>
                <td><?= $contact->sex ?></td>
                <td><?= $contact->age ?></td>
                <td><?= $contact->question ?></td>
                <td> <a href="<?= url('admin/contact/show?id='.$contact->id)?>" class="btn btn-primary" role="button" > 見せる</a></td>
            </tr>
        </tbody>

        <?php endforeach?>
    </table>
</div>











