<!DOCTYPE html>
<html lang="en">
	<head>
		<title><?= isset($title)?$title:'訪問介護　たまのケア';  ?></title>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<?php style('css/bootstrap.min.css'); ?>
		<link href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" rel="stylesheet" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
		<link href="https://cdnjs.cloudflare.com/ajax/libs/mdbootstrap/4.5.15/css/mdb.min.css" rel="stylesheet">
		<link href="https://fonts.googleapis.com/css?family=Montserrat:400,500,800" rel="stylesheet">
		<?php style('css/style.css'); ?>
	</head>
	<body>
		<section class="top-section">
			<div class="top-menu container">
				<a class="navbar-brand" href="#"><?= image('images/logo.png','logo','logo')?><span class="logo-span">訪問介護　たまのケア</span></a>
				<button class="navbar-toggler top-right-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent1" aria-controls="navbarSupportedContent1" aria-expanded="false" aria-label="Toggle navigation">
				<i class="fas fa-bars navbar-toggler-icon"></i>
				</button>
				<div class="collapse navbar-collapse right-item" id="navbarSupportedContent1">
					<ul class="navbar-nav bottom-menu-item">
						<li class="fixed-logo-cover">
							<a class="navbar-brand" href="index.html"><img src="assets/images/logo.png" class="logo-fixed" alt="logo"></a>
						</li>
						<li class="nav-item <?= ($page == '') ? 'active':''; ?>">
							<a class="nav-link" href="<?= url('')?>">ホーム</a>
						</li>
						<li class="nav-item  <?= ($page == 'contact/show') ? 'active':''; ?>">
							<a class="nav-link" href="<?= url('contact/show')?>">事業所案内</a> 
						</li>
						<li class="nav-item">
							<a class="nav-link" href="service.html">サービス内容</a>
						</li>
						<li class="nav-item">
							<a class="nav-link" href="rates.html">ご利用料金</a>
						</li>
						<!-- <li class="nav-item dropdown">
							<a class="nav-link dropdown-toggle" href="" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">サービス内容</a>
							<div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink"><a class="dropdown-item" href="#">日の流れ</a>
							<a class="dropdown-item" href="#">ご利用料金</a>
							</div>
							</li> -->
						<li class="nav-item">
							<a class="nav-link" href="gretting.html">ごあいさつ</a>
						</li>
						<li class="nav-item">
							<a class="nav-link" href="staff.html">スタッフ紹介</a>
						</li>
						<li class="nav-item">
							<a class="nav-link" href="career.html">採用情報</a>
						</li>
						<li class="nav-item">
							<a class="nav-link" href="contact.html">お問い合わせ</a>
						</li>
					</ul>
				</div>
				<ul class="navbar-nav top-header right-item-top pull-right">
					<li class="nav-item first-nav-item">
						<a class="nav-link" href="#">TEL: 0120-1234-5678<br>受付0：00～0：00</a>
					</li>
				</ul>
			</div>
		</section>
		<?php $page = array_reverse(explode('/', parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH)))[0]; ?>
		<nav class="navbar navbar-toggleable-md navbar-inverse container-fluid main-navbar">
			<div class="main-navbar-wrapper opaque-navbar">
				<div class="container">
					<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
					<span class="navbar-toggler-icon"></span>
					</button>
					<div class="collapse navbar-collapse right-item" id="navbarSupportedContent">
						<ul class="navbar-nav bottom-menu-item">
							<li class="nav-item <?= ($page == 'users') ? 'active':''; ?>">
								<a class="nav-link" href="<?= url('admin/users')?>" > ユーザー</a>
							</li>
							<li class="nav-item <?= ($page == 'show') ? 'active':''; ?>">
								<a class="nav-link" href="<?= url('admin/contact/show')?>" > 接触</a>
							</li>
							<li class="nav-item dropdown">
								<a class="nav-link dropdown-toggle" href="" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">スタッフ</a>
								<div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
									<a class="dropdown-item" href="<?= url('admin/staff')?>">すべて スタッフ</a> 
									<a class="dropdown-item" href="<?= url('admin/staffadd')?>">追加する スタッフ</a>
								</div>
							</li>
							<li class="nav-item dropdown">
								<a class="nav-link dropdown-toggle" href="" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">通知</a>
								<div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
									<a class="dropdown-item" href="<?= url('admin/notice')?>">すべて 通知</a>
									<a class="dropdown-item" href="<?= url('admin/noticeadd')?>">追加する 通知</a> 
								</div>
							</li>
							<li class="nav-item <?= ($page == 'addprice') ? 'active':''; ?> ">
								<a class="nav-link" href="<?= url('admin/addprice')?>" >価格を追加</a>
							</li>
							<li class="nav-item <?= ($page == 'add-job') ? 'active':''; ?> ">
								<a class="nav-link" href="<?= url('admin/add-job')?>" >仕事を追加</a>
							</li>
						</ul>
						<ul class="nav navbar-nav ml-auto">
							<li class="nav-item">
								<a class="nav-link" href="<?= url('logout')?>" > <i class="fas fa-sign-out-alt"></i>ログアウト</a>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</nav>