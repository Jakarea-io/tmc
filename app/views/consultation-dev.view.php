<section class="top-section">
	<div class="container">
		<div class="row">
			<div class="content-container col-md-12">
				<table class="table table-striped w-auto">
				  <thead>
				    <tr>
				      <th>Name</th>
				      <th>Sex</th>
				      <th>男性不妊</th>
				      <th>流産</th>
				      <th>精液検査</th>
				      <th>TSE</th>
				      <th>人工授精</th>
				      <th>採卵</th>
				      <th>胚移植</th>
				      <th>Pattern</th>
				    </tr>
				  </thead>
				  <tbody>
				  	<?php foreach($consultations as $cons): ?>
				    <tr class="table-info">
				      <td><?= $cons->name?></td>
				      <td><?= $cons->sex?></td>
				      <td><?= $cons->question_2 ?></td>
				      <td><?= $cons->question_3_3 ?></td>
				      <td><?= $cons->question_4_2 ?></td>
				      <td><?= $cons->question_5_5 ?></td>
				      <td><?= $cons->question_5_2 ?></td>
				      <td><?= $cons->question_5_3 ?></td>
				      <td><?= $cons->question_5_4 ?></td>
				      <td><?= $cons->score ?></td>
				    </tr>
					<?php endforeach ?>
				  </tbody>
				</table>
			</div>
		</div>
	</div>
</section>