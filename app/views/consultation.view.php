<section class="top-section">
	<div class="container">
		<div class="row top-about-bg-container">
			<div class="col-md-12 common-bg-wrapper">
				<div class="about-bg-content">
					<h2 align="center">CONSULTATION</h2>
					<h4 align="center">不妊治療無料相談</h4>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="content-container col-md-10 offset-md-1">
				<ol class="breadcrumb">
					<li class="breadcrumb-item"><a href="#">TOP <i class="fas fa-angle-right breadcumb-icon"></i></a></li>
					<li class="breadcrumb-item active">不妊治療無料相談</li>
				</ol>
				<p class="text-center">日本初上陸・妊娠率89%の英国式療法では、 患者の症状に合わせた医師の指示に沿って、
					夫婦共にサプリメントを服用する治療法がメインで、
					外科的な処置は伴いません。

					問診(無料相談)を通して、本プログラムに則った服用方法と、
					ご夫婦に最適なサプリメントの用量をお伝え致します。

					その他、何かご相談等がございましたら、
					<a href="<?=url('contact')?>" class="cons-anchor-top">こちら</a> からお気軽にお問い合わせ下さいませ。
				</p>
			</div>
		</div>
        
		<div class="row question-ans-container">
			<div class="col-md-10 offset-md-1 question-ans-section">
			
			<form method='post' action="<?= url('consultation-emailsend') ?>" id="emailsubmit"  style ="display: <?= isset($_SESSION['successMail']) ? 'none':'block' ?>">

					<div class="md-form">
						<i class="fas fa-envelope contact-icon-color prefix"></i>
						<input type="text" name="email" value="<?= old('email') ?>" class="form-control">

						<label for="email" class="">メールアドレス</label>
						<div class="form-control-feedback feedback" id="email"> <?= showError('validEmail') ?></div>
					</div>

					<div class="md-form">
						<i class="fas fa-envelope contact-icon-color prefix"></i>
						<input type="text" name="confirmEmail" value="<?= old('confirmEmail') ?>" class="form-control">
						<label for="email" class="">メールアドレス(確認用)</label>
						<div class="form-control-feedback feedback" id="confirmEmail"> <?= showError('confirmeMailSame') ?></div>
					</div>

                    <button type="submit" class="btn btn-custom-cons text-center waves-effect waves-light" > 次へ</button>
			</form>


				<form method='post' action="<?= url('consultation/post') ?>"  id="questionsubmit"   style ="display: <?= isset($_SESSION['successMail']) ? 'block':'none' ?>">
                    <div class="questiondateRange ">
                        
                        <div class="row">
                            <h2 class="d-inline-block ">Q1 .</h2>
                                <div>
                                    <p class="d-inline ">避妊期間はありますか </p>
                                </div>
                                <div>
                                    <div class="form-control-feedback feedback" id="fed_sex"><span>* <?= showError('question_1')  ?> </span></div>
                                </div>
                            </div>
                        <div class="question4-radio" id="dateRangePullDown_2">
                            <div class="form-check-inline">
                                <label class="form-check-label">
                                <input type="radio" class="form-check-input"  value ='はい'  <?= (old('question_1') == 'はい') ? 'checked ' : '' ?> name="question_1">はい
                                </label>
							</div>
						
                            <div class="form-check-inline">
                                <label class="form-check-label">
                                <input type="radio" class="form-check-input"  value ='いいえ' <?= (old('question_1') == 'いいえ') ? 'checked ' : '' ?> name="question_1">いいえ
                                </label>
                            </div>
                        </div>
                    </div>


					<div class="questiondateRange">
						<h2 class="d-inline-block">Q2 .</h2>
						<p class="d-inline">不妊期間はどれくらい経過していますか <span class="form-control-feedback feedback">* <?= showError('question_2')  ?> </span> </p>  
						<div class="question4-radio" id="dateRangePullDown">
							<div class="row">
								<div class="form-check-inline">
									<label class="form-check-label">
									<input type="radio" class="form-check-input" value='半年以上' <?= (old('question_2') == '半年以上') ? 'checked ' : '' ?> name = "question_2" > 半年以上 
									</label>
								</div>
							</div>
							<div class="row">
								<div class="form-check-inline">
									<label class="form-check-label">
									<input type="radio" class="form-check-input" value='１年以上'  <?= (old('question_2') == '１年以上') ? 'checked ' : '' ?> name="question_2">１年以上
									</label>
								</div>
							</div>
							<div class="row">
								<div class="form-check-inline">
									<label class="form-check-label">
									<input type="radio" class="form-check-input" value='２年以上 '  <?= (old('question_2') == '２年以上') ? 'checked ' : '' ?>  name="question_2">２年以上 
									</label>
								</div>
							</div>
							<div class="row">
								<div class="form-check-inline ">
									<label class="form-check-label">
									<input type="radio" class="form-check-input" value='男性不妊' <?= (old('question_2') == '男性不妊') ? 'checked ' : '' ?>  name="question_2">男性不妊
									</label>
								</div>
							</div>
						</div>
					</div>


					<div class="question5">
						<h2 class="d-inline-block">Q3 .</h2>
						<p class="d-inline">妊娠歴</p>
						<div>
							<div class="q5-inner-select d-inline-block">妊娠したことがありますか?  <span class="form-control-feedback feedback">* <?= showError('question_3')  ?> </span> </div> 
							<div class="d-inline radio-inline-q3" id="question3SelectPulldown">
								<div class="form-check-inline">
									<label class="form-check-label">
									<input type="radio" class="form-check-input" value='はい' <?= (old('question_3') == 'はい') ? 'checked ' : '' ?> name="question_3" >はい
									</label>
								</div>
								<div class="form-check-inline">
									<label class="form-check-label">
									<input type="radio" class="form-check-input" value='いいえ' <?= (old('question_3') == 'いいえ') ? 'checked ' : '' ?> name="question_3" >いいえ
									</label>
								</div>
							</div>
						</div>
						<div class="row wrapper-of-select-wrap question3-select-wrap" id="q2selectHide">
							<table class="table table-bordered pregnency-history-table table-responsive">
								<thead class="thead-dark">
									<tr>
										<th>妊娠</th> 
										<th>出産</th> 
										<th>流産</th> 
									</tr>
								</thead>
								<tbody>
									<tr>
										<td>
											<select  name = "question_3_1" class="browser-default custom-select contact-select question5-select">
											    <option></option>
												<option value="1" <?= (old('question_3_1') == '1') ? 'selected ' : '' ?>>1</option>
												<option value="2" <?= (old('question_3_1') == '2') ? 'selected ' : '' ?>>2</option>
												<option value="3" <?= (old('question_3_1') == '3') ? 'selected ' : '' ?>>3</option>
												<option value="4"<?= (old('question_3_1') == '4') ? 'selected ' : '' ?>>4</option>
												<option value="5" <?= (old('question_3_1') == '5') ? 'selected ' : '' ?>>５回以上</option>
											</select>
										</td>
										<td>
											<select  name = "question_3_2" class="browser-default custom-select contact-select question5-select">
											    <option></option>
												<option value="1" <?= (old('question_3_2') == '1') ? 'selected ' : '' ?> >1</option>
												<option value="2" <?= (old('question_3_2') == '2') ? 'selected ' : '' ?>>2</option>
												<option value="3" <?= (old('question_3_2') == '3') ? 'selected ' : '' ?>>3</option>
												<option value="4" <?= (old('question_3_2') == '4') ? 'selected ' : '' ?>>4</option>
												<option value="5" <?= (old('question_3_2') == '4') ? 'selected ' : '' ?>>５回以上</option>
											</select>
										</td>
										<td>
											<select  name = "question_3_3" class="browser-default custom-select contact-select question5-select">
											    <option></option>
												<option value="1" <?= (old('question_3_3') == '1') ? 'selected ' : '' ?>>1</option>
												<option value="2" <?= (old('question_3_3') == '2') ? 'selected ' : '' ?>>2</option>
												<option value="3" <?= (old('question_3_3') == '3') ? 'selected ' : '' ?>>3</option>
												<option value="4" <?= (old('question_3_3') == '4') ? 'selected ' : '' ?>>4</option>
												<option value="5" <?= (old('question_3_3') == '5') ? 'selected ' : '' ?>>５回以上</option>
											</select>
										</td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>

					<div class="question4">
						<h2 class="d-inline-block">Q.4 .</h2>
						<p class="d-inline">般不妊検査歴</p>
                
						<div class="question4-pulldown-wrapper">
							<table class="table table-bordered table-responsive">
								<tbody>
									<tr>
                                        <td>精液検査 <span>* <?= showError('question_4_1')  ?> </span>  </td>    

										<td>
											<select  name ="question_4_1" class="browser-default custom-select contact-select question5-select">
												<option></option>
												<option value="有" <?= (old('question_4_1') == '有') ? 'selected ' : '' ?>>有</option>
												<option value="無" <?= (old('question_4_1') == '無') ? 'selected ' : '' ?>>無</option>
											</select>
										</td>
										<td>
											<select  name = "question_4_2" class="browser-default custom-select contact-select question5-select">
												<option></option>
												<option value="正常" <?= (old('question_4_2') == '正常') ? 'selected ' : '' ?>>正常</option>
												<option value="異常" <?= (old('question_4_2') == '異常') ? 'selected ' : '' ?>>異常</option>
											</select>
										</td>
									</tr>
									<tr>
										<td>フーナー検査  <span>* <?= showError('question_4_3')  ?> </span>   </td>
										<td>
											<select  name = "question_4_3" class="browser-default custom-select contact-select question5-select">
												<option></option>
												<option value="有"  <?= (old('question_4_3') == '有') ? 'selected ' : '' ?>>有</option>
												<option value="無" <?= (old('question_4_3') == '無') ? 'selected ' : '' ?>>無</option>
											</select>
										</td>
										<td>
											<select  name = "question_4_4" class="browser-default custom-select contact-select question5-select">
												<option></option>
												<option value="正常" <?= (old('question_4_4') == '正常') ? 'selected ' : '' ?>>正常</option>
												<option value="異常" <?= (old('question_4_4') == '異常') ? 'selected ' : '' ?>>異常</option>
											</select>
										</td>
									</tr>
									<tr>
										<td>子宮卵管造影 <span>* <?= showError('question_4_5')  ?> </span> </td>
										<td>
											<select  name = "question_4_5" class="browser-default custom-select contact-select question5-select">
												<option></option>
												<option value="有"  <?= (old('question_4_5') == '有') ? 'selected ' : '' ?>>有</option>
												<option value="無"  <?= (old('question_4_5') == '無') ? 'selected ' : '' ?>>無</option>
											</select>
										</td>
										<td>
											<select  name = "question_4_6" class="browser-default custom-select contact-select question5-select">
												<?= showError('question_4_6')  ?>
												<option></option>
												<option value="正常" <?= (old('question_4_6') == '正常') ? 'selected ' : '' ?>>正常</option>
												<option value="異常" <?= (old('question_4_6') == '異常') ? 'selected ' : '' ?>>異常</option>
											</select>
										</td>
									</tr>
									<tr>
										<td>子宮鏡検査 <span>* <?= showError('question_4_7')  ?> </span></td>
										<td>
											<select  name = "question_4_7" class="browser-default custom-select contact-select question5-select">
												<option></option>
												<option value="有" <?= (old('question_4_7') == '有') ? 'selected ' : '' ?>>有</option>
												<option value="無" <?= (old('question_4_7') == '無') ? 'selected ' : '' ?>>無</option>
											</select>
										</td>
										<td>
											<select  name = "question_4_8" class="browser-default custom-select contact-select question5-select">
												<?= showError('question_4_8')  ?>
												<option></option>
												<option value="正常" <?= (old('question_4_8') == '正常') ? 'selected ' : '' ?>>正常</option>
												<option value="異常" <?= (old('question_4_8') == '異常') ? 'selected ' : '' ?>>異常</option>
											</select>
										</td>
									</tr>
									<tr>
										<td>クラミジア検査 <span>* <?= showError('question_4_9')  ?> </span> </td>
										<td>

											<select  name = "question_4_9" class="browser-default custom-select contact-select question5-select">
												<option></option>
												<option value="有" <?= (old('question_4_9') == '有') ? 'selected ' : '' ?>>有</option>
												<option value="無" <?= (old('question_4_9') == '無') ? 'selected ' : '' ?>>無</option>
											</select>
										</td>
										<td>

											<select  name = "question_4_10" class="browser-default custom-select contact-select question5-select">
												<?= showError('question_4_10')  ?>
												<option></option>
												<option value="正常" <?= (old('question_4_10') == '正常') ? 'selected ' : '' ?>>正常</option>
												<option value="異常" <?= (old('question_4_10') == '異常') ? 'selected ' : '' ?>>異常</option>
											</select>
										</td>
									</tr>
									<tr>
										<td>卵誘発剤 <span>* <?= showError('question_4_11')  ?> </span> </td>
										<td>
											<select  name = "question_4_11" class="browser-default custom-select contact-select question5-select">
												<option></option>
												<option value="有" <?= (old('question_4_11') == '有') ? 'selected ' : '' ?>>有</option>
												<option value="無" <?= (old('question_4_11') == '無') ? 'selected ' : '' ?>>無</option>
											</select>
										</td>
										<td>
											<select  name = "question_4_12" class="browser-default custom-select contact-select question5-select">
												<option></option>
												<?= showError('question_4_12')  ?>
												<option value="正常" <?= (old('question_4_12') == '正常') ? 'selected ' : '' ?>>正常</option>
												<option value="異常" <?= (old('question_4_12') == '異常') ? 'selected ' : '' ?>>異常</option>
											</select>
										</td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
					<div class="question5">
						<h2 class="d-inline-block">Q5 .</h2>
						<p class="d-inline">治療歴</p>
						<div class="row wrapper-of-select-wrap q5default-layout">
							<table class="table table-bordered table-responsive">
								<tbody>
									<tr>
										<td>タイミング法</td>
										<td>人工授精</td>
										<td>採卵</td>
										<td>胚移植</td>
										<td>TESE</td>
									</tr>
									<tr>
										<td>
											<select  name = "question_5_1" class="browser-default custom-select contact-select question5-select">
												<option></option>
												<option value="1" <?= (old('question_5_1') == '1') ? 'selected ' : '' ?>>1</option>
												<option value="2" <?= (old('question_5_1') == '2') ? 'selected ' : '' ?>>2</option>
												<option value="3" <?= (old('question_5_1') == '3') ? 'selected ' : '' ?>>3</option>
												<option value="4" <?= (old('question_5_1') == '4') ? 'selected ' : '' ?>>4</option>
												<option value="5" <?= (old('question_5_1') == '5') ? 'selected ' : '' ?>>５回以上</option>
											</select>
										</td>
										<td>
											<select  name = "question_5_2" class="browser-default custom-select contact-select question5-select">
												<option></option>
												<option value="1" <?= (old('question_5_2') == '1') ? 'selected ' : '' ?>>1</option>
												<option value="2" <?= (old('question_5_2') == '2') ? 'selected ' : '' ?>>2</option>
												<option value="3" <?= (old('question_5_2') == '3') ? 'selected ' : '' ?>>3</option>
												<option value="4" <?= (old('question_5_2') == '4') ? 'selected ' : '' ?>>4</option>
												<option value="5" <?= (old('question_5_2') == '5') ? 'selected ' : '' ?>>５回以上</option>
											</select>
										</td>
										<td>
											<select  name = "question_5_3" class="browser-default custom-select contact-select question5-select">
												<option></option>
												<option value="1" <?= (old('question_5_3') == '1') ? 'selected ' : '' ?>>1</option>
												<option value="2" <?= (old('question_5_3') == '2') ? 'selected ' : '' ?>>2</option>
												<option value="3" <?= (old('question_5_3') == '3') ? 'selected ' : '' ?>>3</option>
												<option value="4" <?= (old('question_5_3') == '4') ? 'selected ' : '' ?>>4</option>
												<option value="5" <?= (old('question_5_3') == '5') ? 'selected ' : '' ?>>５回以上</option>
											</select>
										</td>
										<td>
											<select  name = "question_5_4" class="browser-default custom-select contact-select question5-select">
												<option></option>
												<option value="1" <?= (old('question_5_4') == '1') ? 'selected ' : '' ?>>1</option>
												<option value="2" <?= (old('question_5_4') == '2') ? 'selected ' : '' ?>>2</option>
												<option value="3" <?= (old('question_5_4') == '3') ? 'selected ' : '' ?>>3</option>
												<option value="4" <?= (old('question_5_4') == '4') ? 'selected ' : '' ?>>4</option>
												<option value="5" <?= (old('question_5_4') == '5') ? 'selected ' : '' ?>>５回以上</option>
											</select>
										</td>
										<td>
											<select  name = "question_5_5" class="browser-default custom-select contact-select question5-select">
												<option></option>
												<option value="1" <?= (old('question_5_5') == '1') ? 'selected ' : '' ?>>1</option>
												<option value="2" <?= (old('question_5_5') == '2') ? 'selected ' : '' ?>>2</option>
												<option value="3" <?= (old('question_5_5') == '3') ? 'selected ' : '' ?>>3</option>
												<option value="4" <?= (old('question_5_5') == '4') ? 'selected ' : '' ?>>4</option>
											</select>
										</td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
					

                    <div class="question5" id="">
                        <h2 class="d-inline-block">Q6 ．</h2>
                        <p class="d-inline"> 性病にかかったことがありますか？ <span class="form-control-feedback feedback">* <?= showError('man_question_9')  ?> </span> </p>
                        <div class="question4-radio" id="maleq10">
                            <div class="form-check-inline">
                                <label class="form-check-label">
                                <input type="radio" class="form-check-input" value='はい' <?= (old('man_question_9') == 'はい') ? 'checked ' : '' ?> name="man_question_9">はい
                                </label>
                            </div>
                            <div class="form-check-inline">
                                <label class="form-check-label">
                                <input type="radio" class="form-check-input" value='いいえ' <?= (old('man_question_9') == 'いいえ') ? 'checked ' : '' ?> name="man_question_9">いいえ
                                </label>
                            </div>
                        </div>
                        <div class="col-md-6 pulldownsmoke" id="selectmaleQuestion10">
                            <select  name = "man_question_9_1" class="browser-default custom-select contact-select question5-select">
                                <option value="クラミジア" <?= (old('man_question_9_1') == 'クラミジア') ? 'selected ' : '' ?>>クラミジア</option>
                                <option value="淋病" <?= (old('man_question_9_1') == '淋病') ? 'selected ' : '' ?>>淋病</option>
                                <option value="ヘルペス" <?= (old('man_question_9_1') == 'ヘルペス') ? 'selected ' : '' ?>>ヘルペス</option>
                                <option value="カンジダ" <?= (old('man_question_9_1') == 'カンジダ') ? 'selected ' : '' ?>>カンジダ</option>
                                <option value="梅毒" <?= (old('man_question_9_1') == '梅毒') ? 'selected ' : '' ?>>梅毒</option>
                                <option value="コンジローマのコンジローマ" <?= (old('man_question_9_1') == 'コンジローマのコンジローマ') ? 'selected ' : '' ?>>コンジローマのコンジローマ</option>
                                <option value="その他（）" <?= (old('man_question_9_1') == 'その他（）') ? 'selected ' : '' ?>>その他（）</option>
                            </select>
                        </div>
                    </div>





					<div class="question5 q3smoke-cigarrate-wrapper">
						<h2 class="d-inline-block">Q7 .</h2>
                        <p class="d-inline">タバコを吸いますか <span class="form-control-feedback feedback">* <?= showError('Women_question_3')  ?> </span></p>
                    
						<div class="question4-radio" id="q3smokeCigarrateWrapper">
							<div class="form-check-inline">
								<label class="form-check-label">
								<input type="radio" class="form-check-input" value='はい' <?= (old('Women_question_3') == 'はい') ? 'checked ' : '' ?>  name="Women_question_3" >はい
								</label>
							</div>
							<div class="form-check-inline">
								<label class="form-check-label">
								<input type="radio" class="form-check-input" value='いいえ' <?= (old('Women_question_3') == 'いいえ') ? 'checked ' : '' ?>  name="Women_question_3">いいえ
								</label>
							</div>
						</div>
						<div class="col-md-6 pulldownsmoke" id="q3smokeCigarrate">
							<select  name = "Women_question_3_1" class="browser-default custom-select contact-select question5-select">
								<option></option>
								<option value="1"  <?= (old('Women_question_3_1') == '1') ? 'selected' : '' ?> >1</option>
								<option value="2"  <?= (old('Women_question_3_1') == '2') ? 'selected' : '' ?>> 2</option>
								<option value="3"  <?= (old('Women_question_3_1') == '3') ? 'selected' : '' ?>>3</option>
								<option value="4"  <?= (old('Women_question_3_1') == '4') ? 'selected' : '' ?>>4</option>
								<option value="5"  <?= (old('Women_question_3_1') == '5') ? 'selected' : '' ?>>5</option>
								<option value="6"  <?= (old('Women_question_3_1') == '6') ? 'selected' : '' ?>>6</option>
								<option value="7"  <?= (old('Women_question_3_1') == '7') ? 'selected' : '' ?>>7</option>
								<option value="8"  <?= (old('Women_question_3_1') == '8') ? 'selected' : '' ?>>8</option>
								<option value="9" <?= (old('Women_question_3_1') == '9') ? 'selected' : '' ?>>9</option>
								<option value="10"  <?= (old('Women_question_3_1') == '10') ? 'selected' : '' ?>>10</option>
								<option value="11"  <?= (old('Women_question_3_1') == '11') ? 'selected' : '' ?>>11</option>
								<option value="12"  <?= (old('Women_question_3_1') == '12') ? 'selected' : '' ?>>12</option>
								<option value="13" <?= (old('Women_question_3_1') == '13') ? 'selected' : '' ?>>13</option>
								<option value="14" <?= (old('Women_question_3_1') == '14') ? 'selected' : '' ?>>14</option>
								<option value="15"  <?= (old('Women_question_3_1') == '15') ? 'selected' : '' ?>>15</option>
								<option value="16"  <?= (old('Women_question_3_1') == '16') ? 'selected' : '' ?>>16</option>
								<option value="17"  <?= (old('Women_question_3_1') == '17') ? 'selected' : '' ?>>17</option>
								<option value="18"  <?= (old('Women_question_3_1') == '18') ? 'selected' : '' ?>>18</option>
								<option value="19" <?= (old('Women_question_3_1') == '19') ? 'selected' : '' ?>>19</option>
								<option value="20" <?= (old('Women_question_3_1') == '20') ? 'selected' : '' ?>>20</option>
								<option value="21" <?= (old('Women_question_3_1') == '21') ? 'selected' : '' ?>>21</option>
								<option value="22" <?= (old('Women_question_3_1') == '22') ? 'selected' : '' ?>>22</option>
								<option value="23"  <?= (old('Women_question_3_1') == '23') ? 'selected' : '' ?>>23</option>
								<option value="24"  <?= (old('Women_question_3_1') == '24') ? 'selected' : '' ?>>24</option>
								<option value="25"  <?= (old('Women_question_3_1') == '25') ? 'selected' : '' ?>>25</option>
								<option value="26" <?= (old('Women_question_3_1') == '26') ? 'selected' : '' ?>>26</option>
								<option value="27"  <?= (old('Women_question_3_1') == '27') ? 'selected' : '' ?>>27</option>
								<option value="28"  <?= (old('Women_question_3_1') == '28') ? 'selected' : '' ?>>28</option>
								<option value="29" <?= (old('Women_question_3_1') == '29') ? 'selected' : '' ?>>29</option>
								<option value=" more than 30" <?= (old('Women_question_3_1') == 'more than 30') ? 'selected' : '' ?>>more than 30</option>
							</select>
						</div>
					</div>

					<div class="question5">
                            <h2 class="d-inline-block">Q8 .</h2>
                            <p class="d-inline">お酒を飲みますか <span class="form-control-feedback feedback">* <?= showError('Women_question_4')  ?> </span></p>
                        
                            <div class="question4-radio" id="q3alcoholWrapper">
                                <div class="form-check-inline">
                                    <label class="form-check-label">
                                    <input type="radio" class="form-check-input" value='はい' <?= (old('Women_question_4') == 'はい') ? 'checked' : '' ?> name="Women_question_4">はい
                                    </label>
                                </div>
                                <div class="form-check-inline">
                                    <label class="form-check-label">
                                    <input type="radio" class="form-check-input" value='いいえ' <?= (old('Women_question_4') == 'いいえ') ? 'checked' : '' ?> name="Women_question_4">いいえ
                                    </label>
                                </div>
                            </div>
                            <div class="col-md-6 pulldownsmoke" id="q3Alcohol">
                                <p class="d-inline-block">週 :</p>
                                <select  name = "Women_question_4_1" class="browser-default custom-select contact-select question5-select d-inline">
                                    <option></option>
                                    <option value="1" <?= (old('Women_question_4_1') == '1') ? 'selected' : '' ?>>1</option>
                                    <option value="2" <?= (old('Women_question_4_1') == '2') ? 'selected' : '' ?>>2</option>
                                    <option value="3" <?= (old('Women_question_4_1') == '3') ? 'selected' : '' ?>>3</option>
                                    <option value="4" <?= (old('Women_question_4_1') == '4') ? 'selected' : '' ?>>4</option>
                                    <option value="5" <?= (old('Women_question_4_1') == '5') ? 'selected' : '' ?>>5</option>
                                    <option value="6" <?= (old('Women_question_4_1') == '6') ? 'selected' : '' ?>>6</option>
                                    <option value="7" <?= (old('Women_question_4_1') == '7') ? 'selected' : '' ?>>7</option>
                                </select>
                            </div>
					</div>
					
					
					<div class="question5">
						<h2 class="d-inline-block">Q9 .</h2>
                        <p class="d-inline">大豆アレルギーはありますか？ <span class="form-control-feedback feedback">* <?= showError('Women_question_7')  ?> </span> </p>
                
                            <div class="question4-radio">
                                <div class="form-check-inline">
                                    <label class="form-check-label">
                                    <input type="radio" class="form-check-input" value='はい' <?= (old('Women_question_7') == 'はい') ? 'checked' : '' ?> name="Women_question_7">はい
                                    </label>
                                </div>
                                <div class="form-check-inline">
                                    <label class="form-check-label">
                                    <input type="radio" class="form-check-input" value='いいえ' <?= (old('Women_question_7') == 'いいえ') ? 'checked' : '' ?> name="Women_question_7">いいえ
                                    </label>
                                </div>
                            </div>
					</div>
					
				
					<div class="question5">
						<h2 class="d-inline-block">Q10 .</h2>
                        <p class="d-inline">下記のサプリメントを服用していますか？  <span class="form-control-feedback feedback">* <?= showError('Women_question_11')  ?> </span> </p>
            
                            <div class="question4-radio" id="q10Wrapper">
                                <div class="form-check-inline" id="questionnumber10">
                                    <label class="form-check-label">
                                    <input type="radio" class="form-check-input" value='はい' <?= (old('Women_question_11') == 'いいえ') ? 'checked' : '' ?> name="Women_question_11">はい
                                    </label>
                                </div>
                                <div class="form-check-inline">
                                    <label class="form-check-label">
                                    <input type="radio" class="form-check-input" value='いいえ' <?= (old('Women_question_11') == 'いいえ') ? 'checked' : '' ?> name="Women_question_11">いいえ
                                    </label>
                                </div>
                            </div>
						
                            <div class="question4-radio q10blockContainer">
                                <div class="form-check-inline">
                                    <label class="form-check-label">
                                    <input type="radio" class="form-check-input" value='ビタミンD'  name="Women_question_11_1">ビタミンD
                                    </label>
                                </div>
                                <div class="form-check-inline">
                                    <label class="form-check-label">
                                    <input type="radio" class="form-check-input" value='DHEA'  name="Women_question_11_1">DHEA
                                    </label>
                                </div>
                                <div class="form-check-inline">
                                    <label class="form-check-label">
                                    <input type="radio" class="form-check-input" value='葉酸'  name="Women_question_11_1">葉酸
                                    </label>
                                </div>
                                <div class="form-check-inline">
                                    <label class="form-check-label">
                                    <input type="radio" class="form-check-input" value='イノシトール'  name="Women_question_11_1">イノシトール
                                    </label>
                                </div>
                                <div class="form-check-inline">
                                    <label class="form-check-label">
                                    <input type="radio" class="form-check-input" value='亜鉛'  name="Women_question_11_1">亜鉛
                                    </label>
                                </div>
                            </div>
					</div>
				
					<a type="" class="btn btn-custom-cons text-center" id="nextConsultBtn" onclick="showProfileFrom()">次へ</a>
					<div id="basicInfoBlock"  style ="display: <?= isset($_SESSION['errors']['count']) ? 'block':'none' ?>">
						<div class="question5 basicInfoWrapper">
							<h2>【基本情報】</h2>
							<p class="d-inline">ご相談のきっかけ</p> <<span> <?= showError('source') ?></span>
                    

							<div class="question4-radio">
								<div class="form-check-inline">
									<label class="form-check-label">
									<input type="radio" class="form-check-input" value='Google 検索'   <?= (old('source') == 'Google 検索') ? 'checked' : '' ?> name="source">Google 検索
									</label>
								</div>
								<div class="form-check-inline">
									<label class="form-check-label">
									<input type="radio" class="form-check-input" value='Yahoo検索'  <?= (old('source') == 'Yahoo検索') ? 'checked' : '' ?> name="source">Yahoo検索
									</label>
								</div>
								<div class="form-check-inline">
									<label class="form-check-label">
									<input type="radio" class="form-check-input" value='ブログ'  <?= (old('source') == 'ブログ') ? 'checked' : '' ?> name="source">ブログ
									</label>
								</div>
								<div class="form-check-inline">
									<label class="form-check-label">
									<input type="radio" class="form-check-input" value='友人・知人からの紹介'   <?= (old('source') == '友人・知人からの紹介') ? 'checked' : '' ?> name="source">友人・知人からの紹介
									</label>
								</div>
								<div class="form-check-inline">
									<label class="form-check-label">
									<input type="radio" class="form-check-input" value='その他(　　　 )'  <?= (old('source') == 'その他(　　　 )') ? 'checked' : '' ?> name="source">その他(　　　 )
									</label>
								</div>
							</div>
						</div>
						<div class="md-form">
							<i class="fas fa-user-ninja contact-icon-color prefix"></i>
							<input type="text"  name="name" value="<?= old('name')?>" class="form-control">
                            <label for="name">氏名（ニックネーム可）</label>
                            <div class="form-control-feedback feedback" id="fed_sex"><?= showError('name') ?></div>
						</div>
						<div class="row basic-radio-wrapper">
							<div class="col-md-6">
								<div class="md-form <?= hasError('sex')? "has-danger":''?>">
									<div class="left-icon-title">
										<div class="genter-title">性別 <span class="feedback"> <?= showError('sex') ?></span></div>           
									

										<i class="fa fa-transgender <?= hasError('sex')? "danger-icon":'contact-icon-color'?> icon-custom"></i>
									</div>
									<!-- Default inline 1-->
									<div class="custom-radio-wrapper">
										<div class="custom-control custom-radio custom-control-inline profile-radio-wrap">
											<input type="radio"  value= '女性' <?= (old('sex') == '女性') ? 'checked' : '' ?> class="custom-control-input" id="sex" name="sex">
											<label class="custom-control-label" for="sex">女性</label>
										</div>
										<!-- Default inline 2-->
										<div class="custom-control custom-radio custom-control-inline profile-radio-wrap">
											<input type="radio"  value= '男性' <?= (old('sex') == '男性') ? 'checked' : '' ?> class="custom-control-input" id="defaultInline2" name="sex" >
											<label class="custom-control-label" for="defaultInline2">男性</label>
										</div>
									</div>
								</div>
								
							</div>
							<div class="col-md-6">
								<div class="md-form <?= hasError('age')? "has-danger":''?>">
									<div class="left-icon-title">
										<div class="genter-title">年齢</div>
										<!-- <i class="fas fa-calendar <?= hasError('age')? "danger-icon":'contact-icon-color'?> icon-custom"></i> -->
									</div>
									<select  name = "age" class="browser-default custom-select contact-select">
										<option value="30 歳未満" <?= (old('age') == '30 歳未満') ? 'selected' : '' ?>>30 歳未満</option>
										<option value="30〜39 歳" <?= (old('age') == '30〜39 歳') ? 'selected' : '' ?>>30〜39 歳</option>
										<option value="40〜49 歳" <?= (old('age') == '40〜49 歳') ? 'selected' : '' ?>>40〜49 歳</option>
										<option value="50 歳以上" <?= (old('age') == '50 歳以上') ? 'selected' : '' ?>>50 歳以上</option>
									</select>
								</div>
								<div class="form-control-feedback feedback" id="fed_question"><?= showError('age') ?></div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-6">
								<div class="genter-title">職業</div>
								<select  name = "profession" class="browser-default custom-select contact-select">
									<option value="主婦・主夫" <?= (old('profession') == '主婦・主夫') ? 'selected' : '' ?>>主婦・主夫</option>
									<option value="専門・技術職" <?= (old('profession') == '専門・技術職') ? 'selected' : '' ?>>専門・技術職</option>
									<option value="管理職" <?= (old('profession') == '管理職') ? 'selected' : '' ?>>管理職</option>
									<option value="事務職" <?= (old('profession') == '事務職') ? 'selected' : '' ?>>事務職</option>
									<option value="販売職" <?= (old('profession') == '販売職') ? 'selected' : '' ?>>販売職</option>
									<option value="サービス職" <?= (old('profession') == 'サービス職') ? 'selected' : '' ?>>サービス職</option>
									<option value="運輸・通信的職種" <?= (old('profession') == '運輸・通信的職種') ? 'selected' : '' ?>>運輸・通信的職種</option>
									<option value="保安的職種" <?= (old('profession') == '保安的職種') ? 'selected' : '' ?>>保安的職種</option>
									<option value="技能・労務職" <?= (old('profession') == '技能・労務職') ? 'selected' : '' ?>>技能・労務職</option>
									<option value="農・林・漁業" <?= (old('profession') == '農・林・漁業') ? 'selected' : '' ?>>農・林・漁業</option>
									<option value="その他（　　　　）" <?= (old('profession') == 'その他（　　　　）') ? 'selected' : '' ?>>その他（　　　　）</option>
								</select>
							</div>
							<div class="col-md-6">
								<p class="genter-title">販売職</p>
								<input type="checkbox" name="Sales_position" value="check" <?= (old('Sales_position') == 'check') ? 'checked' : '' ?>  class="agree-check" id="agree" />プライバシーポリシー（確認後後送します）
							</div>
						</div>
						<div class="row">
							<div class="consult-btn-wrap text-center consult-end-consult-btn">
								<button type="submit" href="<?=url('thankyou')?>" class="btn btn-custom-cons text-center">プライバシーポリシーに同意して送信する</button>
								<!-- <button type="submit" class="btn btn-custom-cons text-center">問合せ先メールアドレス</button> -->
							</div>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</section>