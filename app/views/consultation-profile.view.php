
<section class="top-section">
    <div class="container">
        <div class="row top-about-bg-container">
    		<div class="col-md-12 common-bg-wrapper">
    			<div class="about-bg-content">
    				<h2 align="center">PROFILE</h2>
    				<h4 align="center">個人情報記入</h4>
    			</div>
    		</div>
	   </div>
        <div class="row">
            <div class="content-container col-md-10 offset-md-1">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="#">TOP <i class="fas fa-angle-right breadcumb-icon"></i></a></li>
                    <li class="breadcrumb-item active">WEB問診票「無料相談」</li>
                </ol>
            </div>
        </div>
        <div class="row question-ans-container">
            <div class="col-md-10 offset-md-1 question-ans-section">
                <form method='post' action="<?= url('consultation-profile') ?>" class="counsult-profile" >
                    <div class="form-group">
                        <div class="col-8">
                            <label for="exampleInputEmail1">名前<span class="label-span">※必須 </span></label>
                            <input type="text" class="form-control" name='name' aria-describedby="emailHelp" placeholder="">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-8 col-md-8 col-lg-4 col-sm-8">
                            <label for="exampleFormControlSelect1">年齢<span class="label-span">※必須 </span></label>
                            <select name='age' class="form-control" >
                                <option >選択してください</option>
                                <option value="25~30">25~30</option>
                                <option value="31~35">31~35</option>
                                <option value="36~40">36~40</option>
                                <option value="41~45">41~45</option>
                                <option value="46~50">46~50</option>
                                <option value="over 51">over 51</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-8">
                            <label for="exampleFormControlSelect1">性別<span class="label-span">※必須 </span></label>
                            <div class="checkbox-container">
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="radio" name="gender"  value="Female" id="inlineRadio1">
                                    <label class="form-check-label" for="inlineRadio1">女性</label>
                                </div>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="radio" name="gender" value="male" id="inlineRadio2">
                                    <label class="form-check-label" for="inlineRadio2">男性</label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-8">
                            <label for="exampleInputEmail1">メールアドレス<span class="label-span">※必須 </span></label>
                            <input type="text" name ='email' class="form-control" id="exampleInputText" aria-describedby="emailHelp" placeholder="">
                        </div>
                    </div>
                    <div class="form-group">
                        <textarea class="form-control" rows="5" id="comment">プライバシーポリシーテキストダミープライバシーポリシーテキストダミープライバシーポリシーテキストダミープライバシーポリシーテキストダミープライバシーポリシーテキストダミープライバシーポリシーテキストダミープライバシーポリシーテキストダミープライバシーポリシーテキストダミープライバシーポリシーテキストダミープライバシーポリシーテキストダミープライバシーポリシーテキストダミープライバシ
                        ーポリシーテキストダミープライバシーポリシーテキストダミープライバシーポリシーテキストダミープライバシーポリシーテキストダミープライバシーポリシーテキストダミープライバシーポリシーテキストダミープライバシーポリシーテキストダミー</textarea>
                    </div>
                    <button type="submit" class="btn btn-custom-cons text-center">プライバシーポリシーに同意して送信する   <i class="fas fa-chevron-right"></i></button>
                </form>
            </div>
        </div>
    </div>
</section>

