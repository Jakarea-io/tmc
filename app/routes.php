<?php 

//$route->get('/','UsersController@getIndex');
$route->get('/','IndexController@index');

$route->get('registration','UsersController@getRegistrationFrom');
$route->post('registration','UsersController@postRegistration');

$route->get('login','UsersController@getIndex');
$route->post('login','UsersController@postLogin');

$route->get('logout','UsersController@getLogout');

$route->get('contact','ContactController@getIndex');

$route->post('contact','ContactController@postContact');

$route->get('faq','ConsultationController@getFaq');
$route->get('about','ConsultationController@getAbout');
$route->get('column','ConsultationController@getColumn');  
$route->get('column-under','ConsultationController@getColumnUnder');  
$route->get('inconvenient-truth','ConsultationController@getInconvenientTruth'); 
$route->get('cost-of-infertility','ConsultationController@getCostOfInfertility'); 
$route->get('burden-of-infertility','ConsultationController@getBurdenOfInfertility'); 
$route->get('absence-of-infertility','ConsultationController@getAbsenceOfInfertility'); 
$route->get('combine-infertility','ConsultationController@getCombineInfertility'); 
$route->get('one-out-of-every-hundread','ConsultationController@getOneOutOfHundread');
$route->get('dont-you-get','ConsultationController@getdontYouGet');  

$route->get('contents','ConsultationController@getContents');   

$route->get('consultation','ConsultationController@getIndex');
$route->get('see-consultation-dev','ConsultationController@getConsultations');

$route->post('consultation/post','ConsultationController@postConsultation');

$route->get('consultation-profile','ConsultationController@consultationProfile');
$route->post('consultation-profile','ConsultationController@consultationProfilePost');
$route->post('consultation-emailsend','ConsultationController@sendMailToCustomer');
$route->get('thankyou','ConsultationController@consultationThankyou');
$route->get('sample','ConsultationController@consultationSample');



$route->get('shedulemailer','CronjobController@getIndex');

$route->get('shedulemailer-logic-1','CronjobController@getIndexfor_1');
$route->get('shedulemailer-logic-2','CronjobController@getIndexfor_2');
$route->get('shedulemailer-logic-3','CronjobController@getIndexfor_3');
$route->get('shedulemailer-logic-4','CronjobController@getIndexfor_4');


////////after login

$route->get('admin/contact/show','ContactController@getAllContact');
$route->get('admin/users','RegisterUserController@getAllRegisterUser');
