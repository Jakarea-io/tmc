<?php 
namespace app\models;

class Consultation{

	function validateConsultation(){
		$errors['count']= 0;
		$old = [];

		if(!$_POST['question_1']){
			$errors['count']++;
			$errors['question_1'] ="※必須";
		}else{
			$old['question_1'] = $_POST['question_1'];
		}

    if(!$_POST['question_2']){
			$errors['count']++;
			$errors['question_2'] ="※必須";
		}else{
			$old['question_2'] = $_POST['question_2'];
		}

		if(!$_POST['question_3']){
			$errors['count']++;
			$errors['question_3'] ="※必須";
		}else{
			$old['question_3'] = $_POST['question_3'];
		}
		
		if(!$_POST['question_4_1'] || !$_POST['question_4_2']){
			$errors['count']++;
			$errors['question_4_1'] ="※必須";
		}else{
			$old['question_4_1'] = $_POST['question_4_1'];
			$old['question_4_2'] = $_POST['question_4_2'];
		}

		if(!$_POST['question_4_3'] || !$_POST['question_4_4']){
			$errors['count']++;
			$errors['question_4_3'] ="※必須";
		}else{
			$old['question_4_3'] = $_POST['question_4_3'];
			$old['question_4_4'] = $_POST['question_4_4'];
		}

		if( ! $_POST['question_4_5'] || ! $_POST['question_4_6'] ){
			$errors['count']++;
			$errors['question_4_5'] ="※必須";
		}else{
			$old['question_4_5'] = $_POST['question_4_5'];
			$old['question_4_6'] = $_POST['question_4_6'];
		}

		if(!$_POST['question_4_7'] || !$_POST['question_4_8']){
			$errors['count']++;
			$errors['question_4_7'] ="※必須";
		}else{
			$old['question_4_7'] = $_POST['question_4_7'];
			$old['question_4_8'] = $_POST['question_4_8'];

		}

		if(!$_POST['question_4_9'] || !$_POST['question_4_10'] ){
			$errors['count']++;
			$errors['question_4_9'] ="※必須";
		}else{
			$old['question_4_9'] = $_POST['question_4_9'];
			$old['question_4_10'] = $_POST['question_4_10'];
		}


		if(!$_POST['question_4_11']){
			$errors['count']++;
			$errors['question_4_11'] ="※必須";
		}else{
			$old['question_4_11'] = $_POST['question_4_11'];
		}

		if(!$_POST['question_4_12']){
			$errors['count']++;
		}else{
			$old['question_4_12'] = $_POST['question_4_12'];
		}

		
		// if(!$_POST['question_5_1']){
		// 	$errors['count']++;
		// 	$errors['question_5_1'] ="※必須";
		// }else{
		// 	$old['question_5_1'] = $_POST['question_5_1'];
		// }


		if(!$_POST['man_question_9']){
			$errors['count']++;
			$errors['man_question_9'] ="※必須";
		}else{
			$old['man_question_9'] = $_POST['man_question_9'];
		}


		if(!$_POST['Women_question_3']){
			$errors['count']++;
			$errors['Women_question_3'] ="※必須";
		}else{
			$old['Women_question_3'] = $_POST['Women_question_3'];
		}
	
		if(!$_POST['Women_question_4']){
			$errors['count']++;
			$errors['Women_question_4'] ="※必須";
		}else{
			$old['Women_question_4'] = $_POST['Women_question_4'];
		}

		if(!$_POST['Women_question_7']){
			$errors['count']++;
			$errors['Women_question_7'] ="※必須";
		}else{
			$old['Women_question_7'] = $_POST['Women_question_7'];
		}

		
		if(!$_POST['Women_question_11']){
			$errors['count']++;
			$errors['Women_question_11'] ="※必須";
		}else{
			$old['Women_question_11'] = $_POST['Women_question_11'];
		}


		if(!$_POST['source']){
			$errors['count']++;
			$errors['source'] ="※必須";
		}else{
			$old['source'] = $_POST['source'];
		}


		if(!$_POST['name']){
			$errors['count']++;
			$errors['name'] ="※必須";
		}else{
			$old['name'] = $_POST['name'];
		}


		if(!$_POST['sex']){
			$errors['count']++;
			$errors['sex'] ="※必須";
		}else{
			$old['sex'] = $_POST['sex'];
		}



		$_SESSION['errors'] = $errors;
		$_SESSION['old'] = $old;
		
		if($errors['count'] > 0){
			$response["message"] = $errors;
		//	echo json_encode($response);
			redirect();
		}
	}
}