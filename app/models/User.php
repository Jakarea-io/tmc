<?php 
namespace app\models;

class User{

    function validateRegistration()
    {
        $errors['count']= 0;
        $old = [];

        if(!$_POST['username']){
            $errors['count']++;
            $errors['username'] ="username is required";
        }else{
            $old['username'] = $_POST['username'];
        }

        if(!$_POST['email']){
            $errors['count']++;
            $errors['email'] ="email is required";
        }else{
            $old['email'] = $_POST['email'];
        }
        
        $_SESSION['errors'] = $errors;
        $_SESSION['old'] = $old;
        
        if($errors['count'] > 0){
            redirect();
        }
    }

}