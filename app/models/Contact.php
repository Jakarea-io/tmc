<?php 
namespace app\models;

class Contact{

	function validateContact(){
		$errors['count']= 0;
		$old = [];

		if(!$_POST['name']){
			$errors['count']++;
			$errors['name'] ="name is required";
		}else{
			$old['name'] = $_POST['name'];
		}

		if(!$_POST['sex']){
			$errors['count']++;
			$errors['sex'] ="性別を選択してください";
		}else{
			$old['sex'] = $_POST['sex'];
		}

		if(!$_POST['age']){
			$errors['count']++;
			$errors['age'] ="年齢を選択してください";
		}else{
			$old['age'] = $_POST['age'];
		}

		if(!$_POST['email']){
			$errors['count']++;
			$errors['email'] ="メールアドレスを入力してください";
		}else{
			if (!filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)) {
				$errors['email']  = "Invalid email format"; 
				}
			$old['email'] = $_POST['email'];
		} 

        
    if(!$_POST['question']){
			$errors['count']++;
			$errors['question'] ="質問事項を入力してください";
		}else{
			$old['question'] = $_POST['question'];
		}



		$_SESSION['errors'] = $errors;
		$_SESSION['old'] = $old;
		if($errors['count'] > 0){
			$response["message"] = $errors;
			//echo json_encode($response);
			redirect();
		}
	}
}