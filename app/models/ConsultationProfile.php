<?php 
namespace app\models;

class ConsultationProfile{

	function validateContact(){
		$errors['count']= 0;
		$old = [];

		if(!$_POST['name']){
			$errors['count']++;
			$errors['name'] ="name is required";
		}else{
			$old['name'] = $_POST['name'];
		}

    if(!$_POST['age']){
			$errors['count']++;
			$errors['age'] ="age is required";
		}else{
			$old['age'] = $_POST['age'];
		}

		if(!$_POST['gender']){
			$errors['count']++;
			$errors['gender'] ="gender is required";
		}else{
			$old['gender'] = $_POST['gender'];
		}

		if(!$_POST['email']){
			$errors['count']++;
			$errors['email'] ="email is required";
		}else{
			$old['email'] = $_POST['email'];
		}


		

		$_SESSION['errors'] = $errors;
		$_SESSION['old'] = $old;
		if($errors['count'] > 0){
			$response["message"] = $errors;
			echo json_encode($response);
			redirect();
		}
	}
}