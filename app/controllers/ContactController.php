<?php 

namespace app\controllers;
use app\core\App;
use app\core\System;
use app\models\Contact;

class ContactController{

	public function getIndex(){
		$title = "訪問介護　たまのケア";
		view('partial/header',compact('title'));
		view('contact');
		view('partial/footer');
	}

	public function getAllContact(){

		if(!isset($_SESSION['user'])){
			return redirect('/');
		}else{
			if(!empty($_GET['id'])){
				$contacts = App::get('db')->getData('contacts',['id' => $_GET['id']]);
				view('Admin/header');
				view('singleContactInfo',compact('contacts'));
				view('partial/footer');
			}else{
				$contacts = App::get('db')->getData('contacts');
				view('Admin/header');
				view('Admin/all_contact',compact('contacts'));
				view('partial/footer');
			}
		}
	}

	public function postContact(){
		$contact  = new Contact;
		$contact->validateContact();  
		$data['name'] 		      = $_POST['name'];
		$data['email'] 	          = $_POST['email'];
		$data['sex'] 		      = $_POST['sex'];
		$data['age'] 		      = $_POST['age'];
		$data['question'] 		  = $_POST['question'];
		
		$result = App::get('db')->insertData('contacts',$data);
		
		$this->sendMail($data);
		
		if($result){
			unset($_SESSION['old']);
			$_SESSION["message"] = 'フォームが正常に送信されました。';
		
			redirect('contact');
		}else{
			$_SESSION["message"] = 'Something went wrong! Please try again.';
			redirect('contact');
		}
	}

    public function sendMail($data){

        $headers = "MIME-Version: 1.0" . "\r\n";
        $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
        $headers .= 'From: " TM クリニック ビジタ" <no-replay@cloudproduction.co.bd>' . "\r\n";
    
        // $html = file_get_contents('/home/cloudpro/projects.cloudproduction.co.bd/tmclinic/public/email-template/contact.html');
		// return mail('asifrahman449@gmail.com','問診票に回答いただきありがとうございました',$html,$headers);

		$templateContent =  file_get_contents('/home/cloudpro/projects.cloudproduction.co.bd/tmclinic/public/email-template/contact.html');

		$placeHolders = [
			    '%Name%',
				'%Email%',
				'%Sex%',
				'%Age%',
				'%Question%',
		];
	
		$values = [
			$data["name"] ,
			$data["email"],
			$data["sex"],
			$data["age"],
			$data["question"]
			
		];
	
		$rendered = str_replace($placeHolders, $values, $templateContent);

		return mail('asifrahman449@gmail.com','連絡を求める',$rendered,$headers);
    }

}