<?php 

namespace app\controllers;
use app\core\App;
use app\core\System;
use app\models\Contact;

class RegisterUserController{


	public function getAllRegisterUser(){

		if(isset($_SESSION['user'])){
			$users = App::get('db')->getData('users');
		
		    view('Admin/header');
		    view('Admin/allregisters',compact('users'));
			view('partial/footer');
		}else{
			
			redirect();
		}
			
	}


}