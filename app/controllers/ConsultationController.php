<?php 

namespace app\controllers;
use app\core\App;
use app\core\System;
use app\models\Consultation;
use app\models\ConsultationProfile;
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

class ConsultationController{

	public function getIndex(){

		$title = "クリニックトップページ";
		view('partial/header',compact('title'));
		view('consultation');
		view('partial/footer');
		unset($_SESSION['errors']['count']);
	}
	
	public function getConsultations(){
		$consultations = App::get('db')->getData('consultations');
		$title = "クリニックトップページ";
		view('partial/header',compact('title'));
		view('consultation-dev',compact('consultations'));
		view('partial/footer');
		unset($_SESSION['errors']['count']);
	}

	public function getFaq(){
		$title = "Clinic top page";
		view('partial/header',compact('title'));
		view('faq');
		view('partial/footer');
	}
	
	public function getContents(){
		$title = "診療内容と費用";
		view('partial/header',compact('title'));
		view('contents');
		view('partial/footer');
	}

	public function getAbout(){
		$title = "Clinic top page";
		view('partial/header',compact('title'));
		view('about');
		view('partial/footer');
	}
	
	public function getColumn(){
		$title = "コラム";
		view('partial/header',compact('title'));
		view('column');
		view('partial/footer');
	}

	public function getColumnUnder(){
		$title = "TM Clinic";
		view('partial/header',compact('title'));
		view('column-under');
	    view('partial/footer');
	}

	public function getInconvenientTruth(){
		$title = "TM Clinic";
		view('partial/header',compact('title'));
		view('inconvenient-truth');
	    view('partial/footer');
	}

	public function getCostOfInfertility(){
		$title = "TM Clinic";
		view('partial/header',compact('title'));
		view('cost-of-infertility');
	    view('partial/footer');
	}

	public function getBurdenOfInfertility(){
		$title = "TM Clinic";
		view('partial/header',compact('title'));
		view('burden-of-infertility');
	    view('partial/footer');
	}

	public function getAbsenceOfInfertility(){
		$title = "TM Clinic";
		view('partial/header',compact('title'));
		view('absence-of-infertility');
	    view('partial/footer');
	}

	public function getCombineInfertility(){
		$title = "TM Clinic";
		view('partial/header',compact('title'));
		view('combine-infertility');
	    view('partial/footer');
	}

	public function getOneOutOfHundread(){
		$title = "TM Clinic";
		view('partial/header',compact('title'));
		view('one-out-of-every-hundread');
	    view('partial/footer');
	}

	public function getdontYouGet(){
		$title = "TM Clinic";
		view('partial/header',compact('title'));
		view('dont-you-get');
	    view('partial/footer');
	}

	public function getAllContact(){

		if(!isset($_SESSION['user'])){
			return redirect('/');
		}else{
			if(!empty($_GET['id'])){
				$contacts = App::get('db')->getData('contacts',['contact_id' => $_GET['id']]);
				view('Admin/header');
				view('singleContactInfo',compact('contacts'));
				view('partial/footer');
			}else{
				$contacts = App::get('db')->getData('contacts');
				view('Admin/header');
				view('Admin/all_contact',compact('contacts'));
				view('partial/footer');
			}
		}
	}

	public function postConsultation(){

		$consultation  = new Consultation;
		$consultation->validateConsultation(); 
		$data['question_1'] 	     = $_POST['question_1'];
		$data['question_2'] 	     = $_POST['question_2'];
		$data['question_3'] 	     = $_POST['question_3'];
		$data['question_3_1'] 	     = !empty($_POST['question_3_1'])?$_POST['question_3_1']:0;
		$data['question_3_2']        = !empty($_POST['question_3_2'])?$_POST['question_3_2']:0;
		$data['question_3_3'] 	     = !empty($_POST['question_3_3'])?$_POST['question_3_3']:0;
		$data['question_4_1'] 	     = $_POST['question_4_1'];
		$data['question_4_2'] 	     = $_POST['question_4_2'];
		$data['question_4_3'] 	     = $_POST['question_4_3'];
		$data['question_4_4']        = $_POST['question_4_4'];
		$data['question_4_5'] 	     = $_POST['question_4_5'];
		$data['question_4_6'] 	     = $_POST['question_4_6'];
		$data['question_4_7']        = $_POST['question_4_7'];
		$data['question_4_8']        = $_POST['question_4_8'];
		$data['question_4_9'] 	     = $_POST['question_4_9'];
		$data['question_4_10'] 	     = $_POST['question_4_10'];
		$data['question_4_11'] 	     = $_POST['question_4_11'];
		$data['question_4_12']       = $_POST['question_4_12'];
		$data['question_5_1'] 	     = $_POST['question_5_1'];
		$data['question_5_2'] 	     = !empty($_POST['question_5_2'])?$_POST['question_5_2']:0;
		$data['question_5_3'] 	     = !empty($_POST['question_5_3'])?$_POST['question_5_3']:0;
		$data['question_5_4'] 	     = !empty($_POST['question_5_4'])?$_POST['question_5_4']:0;
		$data['question_5_5']        = !empty($_POST['question_5_5'])?$_POST['question_5_5']:0;
		$data['man_question_9']      = $_POST['man_question_9'];
		$data['man_question_9_1']    = $_POST['man_question_9_1'];
		$data['Women_question_3']    = $_POST['Women_question_3'];
		$data['Women_question_3_1']  = $_POST['Women_question_3_1'];
		$data['Women_question_4']    = $_POST['Women_question_4'];
		$data['Women_question_4_1']  = $_POST['Women_question_4_1'];
		$data['Women_question_7']    = $_POST['Women_question_7'];
		$data['Women_question_11']   = $_POST['Women_question_11'];
		$data['source']              = $_POST['source'];
		$data['name']                = $_POST['name'];
		$data['sex']                 = $_POST['sex'];
		$data['age']                 = $_POST['age'];
		$data['profession']          = $_POST['profession'];
		$data['email']                = $_SESSION['post-email'];

		$_SESSION['consultation'] = $data;
		
		$data['score'] ='0';

    	if(($data['question_2'] != '男性不妊') && ($data['question_3_3'] == 0) && ($data['question_4_2'] == '正常') && ($data['question_5_5']  == 0) && ($data['question_5_4'] == 0) && ($data['question_5_3'] == 0) && ($data['question_5_2'] == 0)){
    	
    			$data['score'] ='1';
    		}


    	if(($data['question_2'] != '男性不妊') && ($data['question_3_3'] > 0) && ($data['question_4_2'] == '正常') && ($data['question_5_5'] == 0 ) && ($data['question_5_4'] > 0) && ($data['question_5_3'] > 0) && ($data['question_5_2'] > 0)){
    		
    		$data['score'] ='2';

    	}

    	if(($data['question_2'] == '男性不妊') && ($data['question_3_3'] == 0) && ($data['question_4_2'] == '異常') && ($data['question_5_5'] > 0 ) && ($data['question_5_4'] == 0) && ($data['question_5_3'] == 0) && ($data['question_5_2'] == 0)){
    		$data['score'] ='3';
    	}

    	if(($data['question_2'] == '男性不妊') && ($data['question_3_3'] > 0) && ($data['question_4_2'] == '異常') && ($data['question_5_5'] > 0 ) && ($data['question_5_4'] > 0) && ($data['question_5_3'] > 0) && ($data['question_5_2'] > 0)){
    		$data['score'] ='4';
    	}

		$result = App::get('db')->insertData('consultations',$data);
		
		if($result){
			$sending_email	= $data['email'];
			$tamplate = $data['score'];
			$sendmail = $this->sendMailToCoustomer( $sending_email, $tamplate);			
			unset($_SESSION['successMail']);
			redirect('thankyou');
		}else{
			redirect();
		}
	}

	public function sendMailToCoustomer($email, $tamplate){

        $headers = "MIME-Version: 1.0" . "\r\n";
        $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
        $headers .= 'From: "外来問診結果 / TM クリニック" <no-replay@cloudproduction.co.bd>' . "\r\n";
    
        $html = file_get_contents('/home/cloudpro/projects.cloudproduction.co.bd/tmclinic/public/email-template/emailcontent.'.$tamplate.'.html');

        return mail($email,'問診票に回答いただきありがとうございました',$html,$headers);
        
    }

	public function consultationProfile(){
		
		if(!isset($_SESSION['consultation'])){
			return redirect('consultation');
		}
	
		$title = "相談プロフィール";
		
		view('partial/header',compact('title'));
		
		view('consultation-profile');
		view('partial/footer');
		
	}

	public function consultationProfilePost(){
		
		$consultation  = new ConsultationProfile;
		$consultation->validateContact();  
		$data['name']       	= $_POST['name']; 
		$data['age'] 	        = $_POST['age'];
		$data['gender'] 	    = $_POST['gender'];
		$data['email'] 	        = $_SESSION['post-email'] ;
		
		$data = array_merge($_SESSION['consultation'], $data);

		// pre($_SESSION['consultation']);

        if(($_SESSION['consultation']['question_1'] == "はい") && ($_SESSION['consultation']['question_2']== "はい") && ($_SESSION['consultation']['question_4'] == "はい")){
			
			if(($_SESSION['consultation']['question_5'] == "男性")){

				$data['score'] ='1';
			}else{
				$data['score'] ='2';
			}

		}

		if(($_SESSION['consultation']['question_1'] =="いいえ") && ($_SESSION['consultation']['question_2']=="いいえ") && ($_SESSION['consultation']['question_4'] =="いいえ")){

			if(($_SESSION['consultation']['question_5'] == "男性")){
				$data['score'] ='3';
			}else{
	        	$data['score'] ='4';
			}
		}

		$result = App::get('db')->insertData('consultations',$data);

		if($result){

			$checkmail = $this->sendMail($data);
			if($checkmail){
				return redirect('thankyou');
			}else{
				return redirect('consultation');
			}
			
		}else{
			return redirect('consultation');
		}

	}
	
	public function consultationThankyou(){

		if(!isset($_SESSION['consultation'])){
			return redirect('consultation');
		}
		$title = "Consultation Thankyou";
		view('partial/header',compact('title'));
		view('consultation-thankyou');
		view('partial/footer');
		unset($_SESSION['consultation']);
	}

	public function consultationSample(){
		$title = "症例集";
		view('partial/header',compact('title'));
		view('sample');
		view('partial/footer');
	}
	

	public function sendMail($data){
	
	$headers = "MIME-Version: 1.0" . "\r\n";
    $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
    $headers .= 'From: "外来問診結果 / TM クリニック" <no-replay@cloudproduction.co.bd>' . "\r\n";

    $html = file_get_contents('/home/cloudpro/projects.cloudproduction.co.bd/tmclinic/public/email-template/emailcontent.html');
    return mail($data['email'],'問診票に回答いただきありがとうございました',$html,$headers);

	}


	public function sendMailToCustomer(){

		$_SESSION['post-email'] = $_POST['email'];


		if( !filter_var( $_POST['email'], FILTER_VALIDATE_EMAIL)){
			
			$errors['validEmail'] = "Email Address is not valid";
			$old['email']        = $_POST['email'];

			$_SESSION['errors'] = $errors;
			$_SESSION['old'] = $old;
			redirect();
		}

		if( $_POST['email'] != $_POST['confirmEmail'] ){

			$errors['confirmeMailSame'] ="Email doesn't match ";
			$old['email']        = $_POST['email'];
			$old['confirmEmail'] = $_POST['confirmEmail'];

			$_SESSION['errors'] = $errors;
			$_SESSION['old'] = $old;
			redirect();

		}else{
			
			$headers = "MIME-Version: 1.0" . "\r\n";
			$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
			$headers .= 'From: "外来問診結果 / TM クリニック" <no-replay@cloudproduction.co.bd>' . "\r\n";
		
			$html = file_get_contents('/home/cloudpro/projects.cloudproduction.co.bd/tmclinic/public/email-template/contact.1.html');
			
			$successMail =  mail( $_POST['email'],'問診票への返信メール文面',$html,$headers);

			
			if( $successMail == 1 ){
				$_SESSION['successMail'] = 1;
				
				redirect();
			}else{
				$_SESSION['successMail'] = 0;
				redirect();
			}
		} 
	}
}