-- phpMyAdmin SQL Dump
-- version 4.8.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 06, 2019 at 02:21 PM
-- Server version: 10.1.33-MariaDB
-- PHP Version: 7.2.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `tmclinic`
--

-- --------------------------------------------------------

--
-- Table structure for table `consultations`
--

CREATE TABLE `consultations` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `age` varchar(30) DEFAULT NULL,
  `email` varchar(100) NOT NULL,
  `source` varchar(80) DEFAULT NULL,
  `sex` varchar(80) NOT NULL,
  `profession` varchar(80) DEFAULT NULL,
  `Sales_position` varchar(80) DEFAULT NULL,
  `question_1` varchar(80) DEFAULT NULL,
  `question_1_1` varchar(80) DEFAULT NULL,
  `question_1_2` varchar(80) DEFAULT NULL,
  `question_1_3` varchar(80) DEFAULT NULL,
  `question_1_4` varchar(80) DEFAULT NULL,
  `question_1_5` varchar(80) DEFAULT NULL,
  `question_1_6` varchar(80) DEFAULT NULL,
  `question_1_7` varchar(80) DEFAULT NULL,
  `question_2` varchar(80) DEFAULT NULL,
  `startdate_2_1` varchar(80) DEFAULT NULL,
  `endDate_2_2` varchar(80) DEFAULT NULL,
  `question_3` varchar(80) DEFAULT NULL,
  `question_3_1` varchar(80) DEFAULT NULL,
  `question_3_2` varchar(80) DEFAULT NULL,
  `question_3_3` varchar(80) DEFAULT NULL,
  `question_4_1` varchar(80) DEFAULT NULL,
  `question_4_2` varchar(80) DEFAULT NULL,
  `question_4_3` varchar(80) DEFAULT NULL,
  `question_4_4` varchar(80) DEFAULT NULL,
  `question_4_5` varchar(80) DEFAULT NULL,
  `question_4_6` varchar(80) DEFAULT NULL,
  `question_4_7` varchar(80) DEFAULT NULL,
  `question_4_8` varchar(80) DEFAULT NULL,
  `question_4_9` varchar(80) DEFAULT NULL,
  `question_4_10` varchar(80) DEFAULT NULL,
  `question_4_11` varchar(80) DEFAULT NULL,
  `question_4_12` varchar(80) DEFAULT NULL,
  `question_5_1` varchar(80) DEFAULT NULL,
  `question_5_2` varchar(80) DEFAULT NULL,
  `question_5_3` varchar(80) DEFAULT NULL,
  `question_5_4` varchar(80) DEFAULT NULL,
  `question_5_5` varchar(80) DEFAULT NULL,
  `question_6` varchar(80) DEFAULT NULL,
  `Women_question_1_2` varchar(80) DEFAULT NULL,
  `Women_question_1_3` varchar(80) DEFAULT NULL,
  `Women_question_1_4` varchar(80) DEFAULT NULL,
  `Women_question_2_1` varchar(80) DEFAULT NULL,
  `Women_question_2_2` varchar(80) DEFAULT NULL,
  `Women_question_2_3` varchar(80) DEFAULT NULL,
  `Women_question_2_3_1` varchar(80) DEFAULT NULL,
  `Women_question_2_3_2` varchar(80) DEFAULT NULL,
  `Women_question_2_4` varchar(80) DEFAULT NULL,
  `Women_question_2_5` varchar(80) DEFAULT NULL,
  `Women_question_2_6` varchar(80) DEFAULT NULL,
  `Women_question_3` varchar(80) DEFAULT NULL,
  `Women_question_3_1` varchar(80) DEFAULT NULL,
  `Women_question_4` varchar(80) DEFAULT NULL,
  `Women_question_4_1` varchar(80) DEFAULT NULL,
  `Women_question_5` varchar(80) DEFAULT NULL,
  `Women_question_6` varchar(80) DEFAULT NULL,
  `Women_question_7` varchar(80) DEFAULT NULL,
  `Women_question_8` varchar(80) DEFAULT NULL,
  `Women_question_9` varchar(80) DEFAULT NULL,
  `Women_question_9_1` varchar(80) DEFAULT NULL,
  `Women_question_9_2` varchar(80) DEFAULT NULL,
  `Women_question_10` varchar(80) DEFAULT NULL,
  `Women_question_10_1` varchar(80) DEFAULT NULL,
  `Women_question_11` varchar(80) DEFAULT NULL,
  `Women_question_11_1` varchar(80) DEFAULT NULL,
  `man_question_1` varchar(80) DEFAULT NULL,
  `man_question_1_2` varchar(80) DEFAULT NULL,
  `man_question_1_2_1` varchar(80) DEFAULT NULL,
  `man_question_1_3` varchar(80) DEFAULT NULL,
  `man_question_2` varchar(80) DEFAULT NULL,
  `man_question_3` varchar(80) DEFAULT NULL,
  `man_question_4` varchar(80) DEFAULT NULL,
  `man_question_5` varchar(80) DEFAULT NULL,
  `man_question_6` varchar(80) DEFAULT NULL,
  `man_question_8` varchar(80) DEFAULT NULL,
  `man_question_9` varchar(80) DEFAULT NULL,
  `man_question_9_1` varchar(80) DEFAULT NULL,
  `man_question_10` varchar(80) DEFAULT NULL,
  `man_question_11` varchar(80) DEFAULT NULL,
  `man_question_12` varchar(80) DEFAULT NULL,
  `man_question_13` varchar(80) DEFAULT NULL,
  `score` varchar(50) DEFAULT '0',
  `mail_sent` varchar(50) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `consultations`
--

INSERT INTO `consultations` (`id`, `name`, `age`, `email`, `source`, `sex`, `profession`, `Sales_position`, `question_1`, `question_1_1`, `question_1_2`, `question_1_3`, `question_1_4`, `question_1_5`, `question_1_6`, `question_1_7`, `question_2`, `startdate_2_1`, `endDate_2_2`, `question_3`, `question_3_1`, `question_3_2`, `question_3_3`, `question_4_1`, `question_4_2`, `question_4_3`, `question_4_4`, `question_4_5`, `question_4_6`, `question_4_7`, `question_4_8`, `question_4_9`, `question_4_10`, `question_4_11`, `question_4_12`, `question_5_1`, `question_5_2`, `question_5_3`, `question_5_4`, `question_5_5`, `question_6`, `Women_question_1_2`, `Women_question_1_3`, `Women_question_1_4`, `Women_question_2_1`, `Women_question_2_2`, `Women_question_2_3`, `Women_question_2_3_1`, `Women_question_2_3_2`, `Women_question_2_4`, `Women_question_2_5`, `Women_question_2_6`, `Women_question_3`, `Women_question_3_1`, `Women_question_4`, `Women_question_4_1`, `Women_question_5`, `Women_question_6`, `Women_question_7`, `Women_question_8`, `Women_question_9`, `Women_question_9_1`, `Women_question_9_2`, `Women_question_10`, `Women_question_10_1`, `Women_question_11`, `Women_question_11_1`, `man_question_1`, `man_question_1_2`, `man_question_1_2_1`, `man_question_1_3`, `man_question_2`, `man_question_3`, `man_question_4`, `man_question_5`, `man_question_6`, `man_question_8`, `man_question_9`, `man_question_9_1`, `man_question_10`, `man_question_11`, `man_question_12`, `man_question_13`, `score`, `mail_sent`, `created_at`) VALUES
(1, 'MD.ASIF RAHMAN', '40ã€œ49 æ­³', 'nakajima@cloudproduction.co.jp', 'Google æ¤œç´¢', 'å¥³æ€§', 'ã‚µãƒ¼ãƒ“ã‚¹è·', 'check', 'ã¯ã„', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'ç”·æ€§ä¸å¦Š', NULL, NULL, 'ã¯ã„', '3', '4', '4', 'æœ‰', 'ç•°å¸¸', 'ç„¡', 'ç•°å¸¸', 'æœ‰', 'æ­£å¸¸', 'ç„¡', 'ç•°å¸¸', 'ç„¡', 'ç•°å¸¸', 'ç„¡', 'ç•°å¸¸', '2', '3', '3', '2', '3', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'ã¯ã„', '7', 'ã¯ã„', '4', NULL, NULL, 'ã¯ã„', NULL, NULL, NULL, NULL, NULL, NULL, 'ã¯ã„', 'ãƒ“ã‚¿ãƒŸãƒ³D', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'ã¯ã„', 'æ¢…æ¯’', NULL, NULL, NULL, NULL, '1', NULL, '2019-04-03 04:49:30'),
(2, 'MD.ASIF RAHMAN', '30ã€œ39 æ­³', 'nakajima@cloudproduction.co.jp', 'Google æ¤œç´¢', 'å¥³æ€§', 'ä¿å®‰çš„è·ç¨®', 'check', 'ã¯ã„', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'ç”·æ€§ä¸å¦Š', NULL, NULL, 'ã¯ã„', '4', '4', '3', 'æœ‰', 'ç•°å¸¸', 'æœ‰', 'ç•°å¸¸', 'æœ‰', 'ç•°å¸¸', 'ç„¡', 'ç•°å¸¸', 'ç„¡', 'ç•°å¸¸', 'ç„¡', 'ç•°å¸¸', '2', '2', '2', '2', '2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'ã¯ã„', '4', 'ã¯ã„', '4', NULL, NULL, 'ã¯ã„', NULL, NULL, NULL, NULL, NULL, NULL, 'ã¯ã„', 'ãƒ“ã‚¿ãƒŸãƒ³D', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'ã¯ã„', 'ãƒ˜ãƒ«ãƒšã‚¹', NULL, NULL, NULL, NULL, '2', NULL, '2019-04-03 05:07:05'),
(3, 'MD.ASIF RAHMAN', '30ã€œ39 æ­³', 'nakajima@cloudproduction.co.jp', 'Google æ¤œç´¢', 'å¥³æ€§', 'æŠ€èƒ½ãƒ»åŠ´å‹™è·', 'check', 'ã¯ã„', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'ç”·æ€§ä¸å¦Š', NULL, NULL, 'ã¯ã„', '3', '4', '4', 'æœ‰', 'ç•°å¸¸', 'ç„¡', 'ç•°å¸¸', 'ç„¡', 'ç•°å¸¸', 'ç„¡', 'ç•°å¸¸', 'æœ‰', 'ç•°å¸¸', 'ç„¡', 'ç•°å¸¸', '2', '3', '3', '4', '2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'ã¯ã„', '5', 'ã¯ã„', '4', NULL, NULL, 'ã¯ã„', NULL, NULL, NULL, NULL, NULL, NULL, 'ã¯ã„', 'ãƒ“ã‚¿ãƒŸãƒ³D', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'ã¯ã„', 'æ·‹ç—…', NULL, NULL, NULL, NULL, '3', NULL, '2019-04-03 05:07:08'),
(4, 'MD.ASIF RAHMAN', '30ã€œ39 æ­³', 'nakajima@cloudproduction.co.jp', 'Google æ¤œç´¢', 'å¥³æ€§', 'ç®¡ç†è·', 'check', 'ã¯ã„', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'ç”·æ€§ä¸å¦Š', NULL, NULL, 'ã¯ã„', '3', '3', '3', 'æœ‰', 'ç•°å¸¸', 'æœ‰', 'ç•°å¸¸', 'æœ‰', 'ç•°å¸¸', 'ç„¡', 'ç•°å¸¸', 'æœ‰', 'ç•°å¸¸', 'ç„¡', 'ç•°å¸¸', '2', '2', '2', '3', '3', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'ã¯ã„', '4', 'ã¯ã„', '2', NULL, NULL, 'ã¯ã„', NULL, NULL, NULL, NULL, NULL, NULL, 'ã¯ã„', 'ãƒ“ã‚¿ãƒŸãƒ³D', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'ã¯ã„', 'ã‚«ãƒ³ã‚¸ãƒ€', NULL, NULL, NULL, NULL, '4', NULL, '2019-04-03 05:07:12'),
(5, 'MD.ASIF RAHMAN', '40ã€œ49 æ­³', ' sendinfo98@gmail.com ', 'Google æ¤œç´¢', 'å¥³æ€§', 'è¾²ãƒ»æž—ãƒ»æ¼æ¥­', 'check', 'ã¯ã„', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'ç”·æ€§ä¸å¦Š', NULL, NULL, 'ã¯ã„', '3', '4', '4', 'æœ‰', 'ç•°å¸¸', 'æœ‰', 'ç•°å¸¸', 'æœ‰', 'ç•°å¸¸', 'æœ‰', 'ç•°å¸¸', 'ç„¡', 'ç•°å¸¸', 'ç„¡', 'ç•°å¸¸', '3', '4', '3', '3', '2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'ã¯ã„', '4', 'ã¯ã„', '3', NULL, NULL, 'ã¯ã„', NULL, NULL, NULL, NULL, NULL, NULL, 'ã¯ã„', 'ãƒ“ã‚¿ãƒŸãƒ³D', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'ã¯ã„', 'ãƒ˜ãƒ«ãƒšã‚¹', NULL, NULL, NULL, NULL, '1', NULL, '2019-04-03 04:34:25'),
(6, 'MD.ASIF RAHMAN', '30ã€œ39 æ­³', 'asifrahman449@gmail.com', 'Google æ¤œç´¢', 'å¥³æ€§', 'è¾²ãƒ»æž—ãƒ»æ¼æ¥­', 'check', 'ã¯ã„', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'ç”·æ€§ä¸å¦Š', NULL, NULL, 'ã¯ã„', '3', '3', '4', 'æœ‰', 'ç•°å¸¸', 'ç„¡', 'ç•°å¸¸', 'æœ‰', 'ç•°å¸¸', 'ç„¡', 'ç•°å¸¸', 'ç„¡', 'ç•°å¸¸', 'æœ‰', 'ç•°å¸¸', '3', '3', '2', '2', '3', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'ã¯ã„', '3', 'ã¯ã„', '5', NULL, NULL, 'ã¯ã„', NULL, NULL, NULL, NULL, NULL, NULL, 'ã¯ã„', 'DHEA', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'ã¯ã„', 'ã‚«ãƒ³ã‚¸ãƒ€', NULL, NULL, NULL, NULL, '1', NULL, '2019-04-02 07:25:06'),
(7, 'MD.ASIF RAHMAN', '40ã€œ49 æ­³', 'asifrahman449@gmail.com', 'Google æ¤œç´¢', 'å¥³æ€§', 'å°‚é–€ãƒ»æŠ€è¡“è·', 'check', 'ã¯ã„', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'ç”·æ€§ä¸å¦Š', NULL, NULL, 'ã¯ã„', '3', '3', '4', 'æœ‰', 'ç•°å¸¸', 'æœ‰', 'ç•°å¸¸', 'æœ‰', 'ç•°å¸¸', 'ç„¡', 'ç•°å¸¸', 'ç„¡', 'ç•°å¸¸', 'ç„¡', 'ç•°å¸¸', '2', '3', '1', '2', '3', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'ã¯ã„', '5', 'ã¯ã„', '2', NULL, NULL, 'ã¯ã„', NULL, NULL, NULL, NULL, NULL, NULL, 'ã¯ã„', 'ãƒ“ã‚¿ãƒŸãƒ³D', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'ã¯ã„', 'ã‚«ãƒ³ã‚¸ãƒ€', NULL, NULL, NULL, NULL, '2', NULL, '2019-04-03 04:35:35'),
(8, 'MD.ASIF RAHMAN', '30ã€œ39 æ­³', 'asifrahman449@gmail.com', 'Google æ¤œç´¢', 'å¥³æ€§', 'æŠ€èƒ½ãƒ»åŠ´å‹™è·', 'check', 'ã¯ã„', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'ç”·æ€§ä¸å¦Š', NULL, NULL, 'ã¯ã„', '4', '4', '4', 'æœ‰', 'ç•°å¸¸', 'æœ‰', 'ç•°å¸¸', 'æœ‰', 'ç•°å¸¸', 'æœ‰', 'ç•°å¸¸', 'æœ‰', 'ç•°å¸¸', 'æœ‰', 'ç•°å¸¸', '3', '3', '2', '3', '4', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'ã¯ã„', '6', 'ã¯ã„', '5', NULL, NULL, 'ã¯ã„', NULL, NULL, NULL, NULL, NULL, NULL, 'ã¯ã„', 'DHEA', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'ã¯ã„', 'ãƒ˜ãƒ«ãƒšã‚¹', NULL, NULL, NULL, NULL, '1', NULL, '2019-04-02 07:33:17'),
(9, 'MD.ASIF RAHMAN', '30ã€œ39 æ­³', 'asifrahman449@gmail.com', 'Yahooæ¤œç´¢', 'å¥³æ€§', 'ä¿å®‰çš„è·ç¨®', 'check', 'ã¯ã„', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'ç”·æ€§ä¸å¦Š', NULL, NULL, 'ã¯ã„', '5', '2', '4', 'æœ‰', 'ç•°å¸¸', 'æœ‰', 'ç•°å¸¸', 'æœ‰', 'ç•°å¸¸', 'æœ‰', 'æ­£å¸¸', 'æœ‰', 'æ­£å¸¸', 'ç„¡', 'æ­£å¸¸', '2', '3', '3', '2', '2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'ã¯ã„', '3', 'ã¯ã„', '2', NULL, NULL, 'ã¯ã„', NULL, NULL, NULL, NULL, NULL, NULL, 'ã¯ã„', 'DHEA', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'ã¯ã„', 'ã‚«ãƒ³ã‚¸ãƒ€', NULL, NULL, NULL, NULL, '1', NULL, '2019-04-02 07:47:28'),
(10, 'Test ', '30 æ­³æœªæº€', 'jakareaparvez@gmail.com', 'ãƒ–ãƒ­ã‚°', 'ç”·æ€§', 'ã‚µãƒ¼ãƒ“ã‚¹è·', NULL, 'ã„ã„ãˆ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'ï¼’å¹´ä»¥ä¸Š ', NULL, NULL, 'ã¯ã„', '0', '0', '0', 'ç„¡', 'æ­£å¸¸', 'ç„¡', 'ç•°å¸¸', 'æœ‰', 'æ­£å¸¸', 'æœ‰', 'æ­£å¸¸', 'æœ‰', 'ç•°å¸¸', 'æœ‰', 'æ­£å¸¸', '4', '2', '1', '2', '2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'ã„ã„ãˆ', '', 'ã„ã„ãˆ', '', NULL, NULL, 'ã„ã„ãˆ', NULL, NULL, NULL, NULL, NULL, NULL, 'ã„ã„ãˆ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'ã„ã„ãˆ', 'ã‚¯ãƒ©ãƒŸã‚¸ã‚¢', NULL, NULL, NULL, NULL, '0', NULL, '2019-04-06 05:10:39');

-- --------------------------------------------------------

--
-- Table structure for table `contacts`
--

CREATE TABLE `contacts` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `sex` varchar(50) NOT NULL,
  `age` varchar(100) DEFAULT NULL,
  `question` varchar(500) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `contacts`
--

INSERT INTO `contacts` (`id`, `name`, `email`, `sex`, `age`, `question`) VALUES
(1, 'Jakarea', 'jakareaparvez@gmail.com', 'on', '30ã€œ39 æ­³', 'I am testing email'),
(2, 'Jakarea Parvez', 'jakareaparvez@gmail.com', 'ç”·æ€§', '40ã€œ49 æ­³', 'Hello Tomohiro Nakajima san,\r\n\r\nPlease inform if you get this message'),
(3, 'asif', 'asifrahman449@gmail.com', 'ç”·æ€§', '30ã€œ39 æ­³', 'hello tomohiro san'),
(4, 'ãƒˆãƒ¢', 'nakajima@cloudproduction.co.jp', 'ç”·æ€§', '30ã€œ39 æ­³', 'ãƒ†ã‚¹ãƒˆ1'),
(5, 'Jakarea Parvez', 'jakareaparvez@gmail.com', 'ç”·æ€§', '40ã€œ49 æ­³', 'Hello nakajima san\r\n\r\ndid you get this email?'),
(6, 'asif ', 'asifrahman449@gmail.com', 'ç”·æ€§', '40ã€œ49 æ­³', 'hello tomohiro san'),
(7, 'Jakarea Parvez', 'jakareaparvez@gmail.com', 'å¥³æ€§', '40ã€œ49 æ­³', 'Hello'),
(8, 'I am jakarea', 'jakareaparvez@gmail.com', 'å¥³æ€§', '30ä»£  ', 'Hello nahi anin'),
(9, 'Jakarea', 'jakareaparvez@gmail.com', 'ç”·æ€§', '80ä»£ä»¥ä¸Š ', 'Hello I am jakaarea');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `consultations`
--
ALTER TABLE `consultations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `contacts`
--
ALTER TABLE `contacts`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `consultations`
--
ALTER TABLE `consultations`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `contacts`
--
ALTER TABLE `contacts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
