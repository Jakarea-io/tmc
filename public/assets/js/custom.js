$(window).scroll(function() {
    if ($(this).scrollTop() > 50) {
        $('.main-navbar-wrapper').addClass('fixed-top-top');
        $('.opaque-navbar').addClass('opaque');
        $('.main-navbar').css("display", "block");
        $('li.fixed-logo-cover').css("display", "block");
        $(".bottom-menu-item li.nav-item.active").css('background-color', '#44494b');

        $(".bottom-menu-item li").addClass('hover-bg');

        $(".bottom-menu-item li.nav-item.active").css('color', '#fff');
        $('.bottom-menu-item .nav-item a').addClass('fixed-menu-nav-link');
        $(".bottom-menu-item li.nav-item.active .nav-link").css('color', '#fff !important');

    } else {
        $('.opaque-navbar').removeClass('opaque');
        $('.main-navbar-wrapper').removeClass('fixed-top-top');
        $('li.fixed-logo-cover').css("display", "none");
        $(".bottom-menu-item li.nav-item.active").css('background-color', '#fff');
        $('.bottom-menu-item .nav-item a').removeClass('fixed-menu-nav-link');
        $(".bottom-menu-item li").removeClass('hover-bg');
    }
});

$(document).ready(function() {
    $(window).scroll(function() {
        if ($(this).scrollTop() > 50) {
            $('#back-to-top').fadeIn();
        } else {
            $('#back-to-top').fadeOut();
        }
    });
    // scroll body to 0px on click
    $('#back-to-top').click(function() {
        $('#back-to-top').tooltip('hide');
        $('body,html').animate({
            scrollTop: 0
        }, 800);
        return false;
    });

    //$('#back-to-top').tooltip('show');

    $('#jobs').on('change', function() {
        this.form.submit();
    });

});

var btnInput = document.getElementById('btnInput')
var confirmBackBtn = document.getElementById('confirmBackBtn')
var confirmBtn = document.getElementById('confirmBtn')
var confirmStep = document.getElementById('confirmStep')
var successStep = document.getElementById('successStep')
var inputForm = document.getElementById('step-1')

btnInput.addEventListener("click", function() {
    if (checkValidation() == false)
        return false;

    localStorage.setItem('step', 2);
    inputForm.style.display = 'none'
    confirmStep.classList.add('activeStep')
    getInputData()
    document.getElementById('step-2')
    document.getElementById('step-2').style.display = 'block'
})


confirmBtn.addEventListener("click", function() {
    submitFormByAjax()

    document.getElementById('step-2').style.display = 'none'
    successStep.classList.add('activeStep')
    document.getElementById('step-3').style.display = 'block'
})

confirmBackBtn.addEventListener("click", function() {
    inputForm.style.display = 'block'
    confirmStep.classList.remove('activeStep')
    document.getElementById('step-2').style.display = 'none'
    document.getElementById('step-3').style.display = 'none'
})


function submitFormByAjax(){
    var local = [];
    data = {
        surname : localStorage.getItem("surname"),
        name: localStorage.getItem("name"),
        surname_2 : localStorage.getItem("surname_2"),
        name_2 : localStorage.getItem("name_2"),
        email : localStorage.getItem("email"),
        phone : localStorage.getItem("phone"),
        message : localStorage.getItem("message"),
        confirmEmail : localStorage.getItem('confirmEmail')

    }
    $.ajax({
        url : 'contact',
        type: 'post',
        dataType : 'json',
        data: data,
    }).done(function(response){ //
        console.log(response)
    });
}

function checkValidation() {
    var errors = 0;
    var surname = document.getElementById("surname").value;
    document.getElementById('fed_surname').innerHTML = ""
    if (surname.length == 0) {
        errors++
        document.getElementById('fed_surname').innerHTML = "姓 必須"
    }
    localStorage.setItem('surname', surname);

    var name = document.getElementById("name").value;
    document.getElementById('fed_name').innerHTML = ""
    if (name.length == 0) {
        errors++
        document.getElementById('fed_name').innerHTML = "名 必須"
    }
    localStorage.setItem('name', name);

    var surname_2 = document.getElementById("surname_2").value;
    document.getElementById('fed_surname_2').innerHTML = ""
    if (surname_2.length == 0) {
        errors++
        document.getElementById('fed_surname_2').innerHTML = "姓 必須"
    }
    localStorage.setItem('surname_2', surname_2);


    var name_2 = document.getElementById("name_2").value;
    document.getElementById('fed_name_2').innerHTML = ""
    if (name_2.length == 0) {
        errors++
        document.getElementById('fed_name_2').innerHTML = "名 必須"
    }
    localStorage.setItem('name_2', name_2);
    ////

    var email = document.getElementById("email").value;
    document.getElementById('fed_email').innerHTML = ""
    if (email.length == 0) {
        errors++
        document.getElementById('fed_email').innerHTML = "  メールアドレス 必須 "
    }
    var validateEmail = /\S+@\S+/;
    if(!validateEmail.test(email)){
        document.getElementById('fed_email').innerHTML = "メールアドレスを確認してください"
    }

    localStorage.setItem('email', email);

    var confirmEmail = document.getElementById("confirmEmail").value;
    document.getElementById('fed_confirmEmail').innerHTML = ""
    if (confirmEmail.length == 0) {
        errors++
        document.getElementById('fed_confirmEmail').innerHTML = "メールアドレス確認 必須"
    } else if (email != confirmEmail) {

        email != confirmEmail
        errors++
        document.getElementById('fed_confirmEmail').innerHTML = "メールアドレスを確認してください"
    }
    localStorage.setItem('confirmEmail', confirmEmail);


    var numbers = /^\d+$/;
    var phone = document.getElementById("phone").value;
    document.getElementById('fed_phone').innerHTML = ""
    if (phone.length == 0) {
        errors++
        document.getElementById('fed_phone').innerHTML = "必須"
    } else if ((phone.length < 9)) {
        errors++
        document.getElementById('fed_phone').innerHTML = "cant be less than 9"
    } else if (!phone.match(numbers)) {
        errors++
        document.getElementById('fed_phone').innerHTML = "ハイフンは抜いてください"
    }
    localStorage.setItem('phone', phone);

    var message = document.getElementById("message").value;
    document.getElementById('fed_message').innerHTML = ""
    if (message.length == 0) {
        errors++
        document.getElementById('fed_message').innerHTML = "お問い合わせ内容 必須"
    }
    localStorage.setItem('message', message);

    if (errors > 0)
        return false
    return true
}

function getInputData() {
    if (localStorage.getItem("surname") !== null) {
        document.getElementById('lsurname').innerHTML = localStorage.getItem("surname")
    }

    if (localStorage.getItem("name") !== null) {
        document.getElementById('lname').innerHTML = localStorage.getItem("name")
    }

    if (localStorage.getItem("surname_2") !== null) {
        document.getElementById('lsurname_2').innerHTML = localStorage.getItem("surname_2")
    }

    if (localStorage.getItem("name_2") !== null) {
        document.getElementById('lname_2').innerHTML = localStorage.getItem("name_2")
    }

    if (localStorage.getItem("email") !== null) {
        document.getElementById('lemail').innerHTML = localStorage.getItem("email")
    }

    if (localStorage.getItem("phone") !== null) {
        document.getElementById('lphone').innerHTML = localStorage.getItem("phone")
    }

    if (localStorage.getItem("message") !== null) {
        document.getElementById('lmessage').innerHTML = localStorage.getItem("message")
    }
}