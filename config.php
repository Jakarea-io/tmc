<?php 

return [
	'database' => [
		'name' 		=>	'tmclinic',
		'username'	=>	'root',
		'password'	=>	'',
		'connection'=>	'mysql:host=localhost',
		'options'	=>	[
			PDO::ATTR_ERRMODE	=>	PDO::ERRMODE_EXCEPTION
		]
	],
	'base_url' => 'http://localhost/tmc/',
	'project_folder' => 'tmc'
];