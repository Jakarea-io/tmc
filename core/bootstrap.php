<?php 
	use app\Core\App;
	$app = [];

	App::bind('config',require 'config.php');
	
	/**
	 *
     * This redirect function set header.
     * if $path null, It will automatically redircet back
     *
    */

    /*define("BASE_PATH", App::get('config')['base_url'].trim($path, '/'));*/
	function redirect($path = null){
		if (!isset($path) && isset($_SERVER["HTTP_REFERER"])) {
	        header("Location: " . $_SERVER["HTTP_REFERER"]); exit;
	    }
		header('Location: '.App::get('config')['base_url'].trim($path, '/'));
	}
	
	/**
	 *
     * This old function restore value and unset old value.
     *
    */

	function old($key){
		if(isset($_SESSION['old'][$key])){
			$value = $_SESSION['old'][$key];
			unset($_SESSION['old'][$key]);
			return $value;
		}
	}

	/**
	 *
     * This hasError function show error message.
     *
    */
    
	function hasError($key){
		if(isset($_SESSION['errors'][$key])){
			unset($_SESSION['errors'][$key]);
			return 1;
		}
	}

	function showError($key){
		if(isset($_SESSION['errors'][$key])){
			$value = $_SESSION['errors'][$key];
			unset($_SESSION['errors'][$key]);
			return $value;
		}
	}

	/**
	 *
     * view function render html file.
     *
    */
	function view($view,$data = []){
		extract($data);
		return require "app/views/{$view}.view.php";
	}
	
	/**
	 *
     * This style function generate link for css file.
     *
    */

	function style($css){
		$base_url = App::get('config')['base_url'];
		echo "<link rel='stylesheet' href='{$base_url}/public/assets/{$css}'>";
	}

	/**
	 *
     * This script function generate link for js file.
     *
    */

	function script($script){
		$base_url = App::get('config')['base_url'];
		echo "<script src='{$base_url}/public/assets/{$script}'></script>";
	}

	/**
	 *
     * This image function generate img tag and image link.
     *
    */
	function image($src,$class= null,$alt= null){
		$base_url = App::get('config')['base_url'];
		echo "<img src='{$base_url}/public/assets/{$src}' class='{$class}' alt='{$alt}'></img>";
	}

	/**
	 *
     * This url function generate Absolute url using base url.
     *
    */
	function url($url){
		echo App::get('config')['base_url'].$url;
	}

	/**
	 *
     * This pr function show data inside pre tag.
     *
    */
	function pr($data){
		echo '<pre>';
			print_r($data);
		echo '</pre>';
	}
	/**
	 *
     * This pre function show data inside pre tag and stop execution of file.
     *
    */
	function pre($data){
		echo '<pre>';
			print_r($data);
		echo '</pre>';
		exit;
	}

	/**
	 *
     * This prw function show data inside pre tag and stop execution of file.
     *
    */

	return new QueryBuilder(
		Connection::make(App::get('config')['database'])
	);
