<?php 

class QueryBuilder{

	protected $pdo;
	function __construct($pdo){
		$this->pdo = $pdo;
	}

	public function max($table,$column){
		$sql = $this->pdo->prepare("SELECT MAX($column) as max From {$table} WHERE assign_to = ".$_SESSION['user']->id);
		$sql->execute();
		return $sql->fetchAll(PDO::FETCH_OBJ);
	}
	public function countData($table){
		$sql = $this->pdo->prepare("SELECT count(*) as total From {$table} ");
		$sql->execute();
		return $sql->fetchAll(PDO::FETCH_OBJ);
	}

	public function min($table,$column){
		$sql = $this->pdo->prepare("SELECT min($column) as min From {$table} WHERE assign_to = ".$_SESSION['user']->id );

		$sql->execute();
		return $sql->fetchAll(PDO::FETCH_OBJ);
	}

	public function insertData($table,$data){
		$sql = sprintf(
			"insert into %s (%s) values (%s)",
			$table,
			implode(', ', array_keys($data)),
			':'.implode(', :', array_keys($data))
			);

		$sql = $this->pdo->prepare($sql);
		try {
			$sql->execute($data);
			return true;
		} catch (PDOException $e) {
			die("Woops Something went wrong". $e);
		}
	}

	public function getData($table, $data=[], $limit=null, $order=null){
		$limit = $limit?$limit:8;
		$page = isset($_GET['page']) && ($_GET['page'] > 1)?$_GET['page'] - 1:0;
		$offset = ($limit * $page);
		$condition='';
		if($data)
		foreach ($data as $key => $value) {
			$condition .=	$key." = '" . $value."' AND ";
		}
	
		if($order){
			$order = ' ORDER BY ' .$order[0] .' '.$order[1];
		}

		if($limit){
			$limit = ' LIMIT '. $limit;
		}
		if($offset > 0){
			$limit .=  ' OFFSET '.$offset;
		}
		
		$sql = " SELECT * FROM {$table} WHERE ". $condition . 1 . $order . $limit;
    
		$sql = $this->pdo->prepare($sql);
		$sql->execute();
		return $sql->fetchAll(PDO::FETCH_OBJ);
	}

	public function updateData($table,$data=[],$where=[]){
		if(empty($data || $condition)){
			return false;
		}

		$condition='';
		$update = '';
		foreach ($data as $key => $value) {
			$update .=	$key." = '" . $value."', ";
		}

		foreach ($where as $key => $value) {
			$condition .=	$key." = '" . $value."' AND ";
		}

		$sql = " UPDATE {$table} SET ". rtrim($update,' , ') ." WHERE ". $condition . 1;
		$sql = $this->pdo->prepare($sql);
		return $sql->execute();
	}


	public function getjobTitle(){
	
		$sql = " SELECT `title` FROM `jobs` ";
	
		$sql = $this->pdo->prepare($sql);
		$sql->execute();
		return $sql->fetchAll(PDO::FETCH_OBJ);
	}


}