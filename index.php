<?php 
session_start();
use app\core\App;
use app\core\Router;
use app\core\Request;

	require 'vendor/autoload.php';
	
	App::bind('db',require 'core/bootstrap.php');

	$route = new Router;

    $route->load('app/routes.php')->direct(Request::uri(),Request::method());

